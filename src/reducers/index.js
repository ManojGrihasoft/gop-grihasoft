import { combineReducers } from "redux";
import projects from "./projects";
import projectBatches from "./projectBatch";

export default combineReducers({
  projects,
  projectBatches,
});
