import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import helper from "../helpers/helpers";
import { Dropdown } from "react-bootstrap";
import face1 from "../assets/faces/face1.jpg";
import loginService from "../services/login.service";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      middleName: "",
      lastName: "",
      show: false,
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchUsername();
  }
  //#endregion

  //#region Fetching selected User details
  fetchUsername() {
    const user = helper.getUser();
    //Service Call
    loginService
      .getUsername(user)
      .then((response) => {
        this.setState({
          firstName: response.data.FirstName,
          middleName: response.data.MiddleName,
          lastName: response.data.LastName,
        });
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  closeMenu(e) {
    e.target.closest(".dropdown").classList.remove("show");
    e.target.closest(".dropdown .dropdown-menu").classList.remove("show");
  }

  toggleHeaderMenu(e) {
    e.preventDefault();
    document.querySelector("body").classList.toggle("az-header-menu-show");
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      document.querySelector("body").classList.remove("az-header-menu-show");
    }
  }

  render() {
    return (
      <div>
        <div className="az-header">
          <div className="container">
            <div className="az-header-left">
              <Link to="/Dashboard" className="az-logo">
                <span></span> GOP
              </Link>
              <a
                id="azMenuShow"
                onClick={(event) => this.toggleHeaderMenu(event)}
                className="az-header-menu-icon d-lg-none"
                href="#/"
              >
                <span></span>
              </a>
            </div>
            <div className="az-header-menu">
              <div className="az-header-menu-header">
                <Link to="/Dashboard" className="az-logo">
                  <span></span> GOP
                </Link>
                <a
                  href="#/"
                  onClick={(event) => this.toggleHeaderMenu(event)}
                  className="close"
                >
                  &times;
                </a>
              </div>
              <ul className="nav">
                <li
                  className={
                    this.isPathActive("/Dashboard")
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link to="/Dashboard" className="nav-link">
                    <i className="fas fa-chart-line mg-r-2"></i> Dashboard
                  </Link>
                </li>
                <li
                  className={
                    this.isPathActive("/Projects")
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link to="/Projects" className="nav-link">
                    <i className="fas fa-tasks mg-r-2"></i> Projects
                  </Link>
                </li>
                <li
                  className={
                    this.isPathActive("/Masters")
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link to="/Masters/Customers" className="nav-link">
                    <i className="fas fa-clipboard-list mg-r-2"></i> Masters
                  </Link>
                </li>
                <li
                  className={
                    this.isPathActive("/Allocation")
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link to="/Allocation" className="nav-link">
                    <i className="fas fa-users mg-r-2"></i>
                    Allocation
                  </Link>
                </li>
                <li
                  className={
                    this.isPathActive("/reports")
                      ? "nav-item active"
                      : "nav-item"
                  }
                >
                  <Link to="/reports" className="nav-link">
                    <i className="fas fa-chart-bar mg-r-2"></i> Reports
                  </Link>
                </li>
                <li
                  className={
                    this.isPathActive("/admin") ? "nav-item active" : "nav-item"
                  }
                >
                  <Link to="/admin" className="nav-link">
                    <i className="fas fa-user-cog mg-r-2"></i> Admin
                  </Link>
                </li>
              </ul>
            </div>
            <div className="az-header-right">
              <Dropdown
                className="az-profile-menu"
                show={this.state.show}
                onMouseEnter={() => this.setState({ show: true })}
                onMouseLeave={() => this.setState({ show: false })}
              >
                <Dropdown.Toggle as={"a"} className="az-img-user">
                  <img src={face1} alt=""></img>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <div className="az-dropdown-header d-sm-none">
                    <i className="icon ion-md-arrow-back"></i>
                  </div>
                  <div className="az-header-profile">
                    <div className="az-img-user">
                      <img src={face1} alt=""></img>
                    </div>
                    <h6>
                      {this.state.firstName} {this.state.middleName}{" "}
                      {this.state.lastName}
                    </h6>
                  </div>
                  <Link
                    to="/ChangePassword"
                    onClick={(event) => this.closeMenu(event)}
                    className="dropdown-item"
                  >
                    <i className="fas fa-key"></i> Change Password
                  </Link>
                  <Link to="/signout" className="dropdown-item">
                    <i className="fas fa-sign-out-alt"></i> Sign Out
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
      </div>
    );
  }

  isPathActive(path) {
    return this.props.location.pathname.startsWith(path);
  }
}

export default withRouter(Header);
