import { READ_DELIVERED_PROJECTS } from "./types";
import { READ_ONGOING_PROJECTS } from "./types";
import ProjectService from "../services/project.service";

export const readOnGoingProjectsList = (userID) => async (dispatch) => {
  try {
    const res = await ProjectService.readOnGoingProjectsList(userID);

    dispatch({
      type: READ_ONGOING_PROJECTS,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const readDeliveredProjectsList = (userID) => async (dispatch) => {
  try {
    const res = await ProjectService.readDeliveredProjectsList(userID);

    dispatch({
      type: READ_DELIVERED_PROJECTS,
      payload: res.data,
    });
    console.log(res.data);
  } catch (err) {
    console.log(err);
  }
};
