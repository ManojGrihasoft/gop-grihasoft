import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/Login/Login";
import Dashboard from "./components/Dashboard";
import Projects from "./components/Projects";
import Masters from "./components/Masters";
import Header from "./shared/Header";
import Admin from "./admin";
import Allocation from "./components/Allocation/Allocation";
import Signout from "./components/Login/Signout";
import ChangePassword from "./components/Login/ChangePassword";
import Reports from "./components/reports/reports";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import NetworkDetector from "./Hoc/NetworkDetector";
import SessionTimeout from "./helpers/sessionTimeout";
import "./App.scss";

class App extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route
              path="/Dashboard"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Dashboard {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            />
            <Route
              path="/Projects"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Projects {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route
              path="/Masters"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Masters {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route
              path="/Allocation"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Allocation {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route
              path="/reports"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Reports {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route
              path="/admin"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <Admin {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route
              path="/ChangePassword"
              render={(props) => (
                <div>
                  <Header data={props} />
                  <ChangePassword {...props} />
                  <SessionTimeout {...props} />
                </div>
              )}
            ></Route>
            <Route path="/signout" component={Signout}></Route>
          </Switch>
        </Router>
      </>
    );
  }
}

export default NetworkDetector(App);
