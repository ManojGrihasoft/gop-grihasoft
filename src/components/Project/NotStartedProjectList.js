import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import projectService from "../../services/project.service";
import Moment from "moment";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import accessControlService from "../../services/accessControl.service";
toast.configure();

class NotStartedProjectList extends Component {
  constructor(props) {
    super(props);

    this.divScrollRef = React.createRef();

    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.sortData = this.sortData.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.exportNotStartedProjectListToExcel =
      this.exportNotStartedProjectListToExcel.bind(this);

    this.state = {
      canAccessViewProject: false,
      canAccessProjectBatchList: false,
      loading: false,
      spinnerMessage: "",
      projects: [],
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: true,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
    };
  }

  //#region component mount
  componentDidMount() {
    this.canUserAccessPage("View Project");
    this.canUserAccessPage("Project Batch List");

    this.fetchNotStartedProjectList();
  }
  //#endregion

  //#region Fetch Not Started Project List
  async fetchNotStartedProjectList() {
    this.setState({
      spinnerMessage: "Please wait while fetching not started project List...",
      loading: true,
    });

    await projectService
      .readNotStartedProjectsList(helper.getUser())
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          ReceivedDate: Moment(obj.ReceivedDate).format("DD-MMM-yyyy"),
          PlannedDeliveryDate:
            obj.PlannedDeliveryDate !== null
              ? Moment(obj.PlannedDeliveryDate).format("DD-MMM-yyyy")
              : "",
        }));

        this.setState({ loading: false, projects: formattedArray });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Project page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "View Project") {
          this.setState({
            canAccessViewProject: response.data,
          });
        } else if (pageName === "Project Batch List") {
          this.setState({
            canAccessProjectBatchList: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Export Not Started Project List to Excel
  exportNotStartedProjectListToExcel() {
    this.setState({
      spinnerMessage:
        "Please wait while exporting not started project list to excel...",
      loading: true,
    });

    let fileName = "Not Started Projects List.xlsx";

    projectService
      .exportNotStartedProjectListToExcel()
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", fileName);
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.projects,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.projects[0]);
    let excludedColumns = [
      "ProjectID",
      "LocationCode",
      "TypeOfInput",
      "InputCountType",
      "ReceivedFormat",
      "OutputFormat",
      "PlannedStartDate",
      "DeliveryMode",
      "DeliveryPlanFileName",
      "IsResourceBased",
      "Remarks",
      "CustomerInputFileName",
      "ScopeFileName",
      "Guideline",
      "GuidelineFileName",
      "Checklist",
      "ChecklistFileName",
      "EmailDate",
      "EmailDescription",
      "Status",
      "canUserChangeProjectStatus",
      "HoldOnDate",
      "HoldOnReason",
      "DeliveredOn",
      "DeliveredCount",
      "Activities",
      "UserID",
    ];

    columns = columns.filter((item) => !excludedColumns.includes(item));

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "NoOfBatches", "InputCount"];
    let dateColumns = ["ReceivedDate", "PlannedDeliveryDate"];

    sortedArray = tableFunctions.sortData(
      this.state.projects,
      column,
      selectedSort,
      numberColumns,
      dateColumns
    );

    this.setState({ projects: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  render() {
    const data = this.state.projects.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);
    const canAccessViewProject = this.state.canAccessViewProject;
    const canAccessProjectBatchList = this.state.canAccessProjectBatchList;

    const projectListColumns = [
      {
        dataField: "SlNo",
        align: "center",
        style: {
          width: "65px",
        },
      },
      {
        dataField: "ProjectID",
        align: "center",
        hidden: true,
      },
      {
        dataField: "CustomerCode",
        align: "center",
        style: {
          width: "89px",
        },
      },
      {
        dataField: "ProjectCode",
        align: "center",
        style: {
          width: "89px",
        },
        classes: canAccessViewProject ? "demo-key-row1" : "",
        events: {
          onClick: (e, column, columnIndex, row, rowIndex) => {
            canAccessViewProject &&
              this.props.history.push({
                pathname: "/Projects/ViewProject",
                state: {
                  ProjectID: row.ProjectID,
                  activeTab: 3,
                }, // passing Project ID to View Project Page
              });
          },
        },
      },
      {
        dataField: "Scope",
        text: "Description",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
        },
        title: true,
      },
      {
        dataField: "ReceivedDate",
        align: "center",
        style: {
          width: "200px",
        },
      },
      {
        dataField: "PlannedDeliveryDate",
        align: "center",
        style: {
          width: "165px",
        },
      },
      {
        dataField: "NoOfBatches",
        align: "center",
        classes: canAccessProjectBatchList ? "demo-key-row1" : "",
        events: {
          onClick: (e, column, columnIndex, row, rowIndex) => {
            canAccessProjectBatchList &&
              this.props.history.push({
                pathname: "/Projects/ProjectBatchList",
                state: {
                  ProjectID: row.ProjectID,
                  CustomerCode: row.CustomerCode,
                  ProjectCode: row.ProjectCode,
                  Scope: row.Scope,
                  activeTab: 1,
                }, // passing Project details to Project Batch List Page
              });
          },
        },
        style: {
          width: "165px",
        },
      },
      {
        dataField: "ProjectType",
        align: "center",
        style: {
          width: "127px",
        },
      },
      {
        dataField: "InputCount",
        align: "center",
        style: {
          width: "99px",
        },
      },
    ];

    return (
      <>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div
              style={{
                backgroundColor: "white",
                paddingTop: "45px",
                width: "400px",
                height: "120px",
              }}
            >
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-body pd-lg-l-40 d-flex flex-column">
            <ToolkitProvider
              keyField="ProjectID"
              data={this.state.filterValue === "" ? data : filteredData}
              columns={projectListColumns}
            >
              {(props) => (
                <div className="mg-t-5">
                  <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                    <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                      <div className="row">
                        {this.state.isToShowSortingFields && (
                          <>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortColumn">Column:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedColumn}
                                    onChange={this.onChangeColumn}
                                  >
                                    <option value="">--Select--</option>
                                    {this.state.columns.map((col) => (
                                      <option key={col}>{col}</option>
                                    ))}
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortOrder">Order:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedSort}
                                    onChange={this.onChangeSortOrder}
                                  >
                                    <option value="">--Select--</option>
                                    <option value="ascending">Ascending</option>
                                    <option value="descending">
                                      Descending
                                    </option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-2">
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSort}
                                  title="Clear Sort Fields"
                                >
                                  <i className="far fa-window-close"></i>
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {this.state.isToShowFilteringField && (
                          <>
                            <div className="col-md-12">
                              <div
                                className="row"
                                style={{ flexWrap: "nowrap" }}
                              >
                                <div className="col-md-1 mg-t-7">
                                  <label htmlFor="search">Search:</label>
                                </div>
                                <div className="col-lg pd-r-10">
                                  <input
                                    type="text"
                                    className="form-control mg-l-10"
                                    maxLength="20"
                                    value={this.state.filterValue}
                                    onChange={this.onChangefilterValue}
                                  />
                                </div>
                                <div>
                                  <span
                                    className="btn btn-primary pd-b-5"
                                    onClick={this.clearSearch}
                                  >
                                    <i
                                      className="far fa-window-close"
                                      title="Clear Filter"
                                    ></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    {this.state.projects.length > 0 && (
                      <div
                        className="col-md-2"
                        style={{ textAlign: "end", marginTop: "10px" }}
                      >
                        <i
                          className="fas fa-exchange-alt fa-rotate-90 pointer"
                          title={
                            this.state.isToShowSortingFields
                              ? "Hide Sort"
                              : "Show Sort"
                          }
                          onClick={this.displaySortingFields}
                        ></i>
                        {!this.state.isToShowFilteringField ? (
                          <i
                            className="fas fa-filter pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Show Filter"
                          ></i>
                        ) : (
                          <i
                            className="fas fa-funnel-dollar pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Hide Filter"
                          ></i>
                        )}
                        <i
                          className="fas fa-file-excel mg-l-10 pointer"
                          style={{ color: "green" }}
                          onClick={this.exportNotStartedProjectListToExcel}
                          title="Export to Excel"
                        ></i>
                      </div>
                    )}
                  </div>
                  <div style={{ width: "98.3%" }}>
                    <div>
                      <table
                        style={{
                          width: "100%",
                        }}
                      >
                        <thead>
                          <tr>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "5%"
                                    : "5.05%",
                              }}
                            >
                              Sl No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "6.8%"
                                    : "6.8%",
                              }}
                            >
                              Cus Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "6.85%"
                                    : "6.8%",
                              }}
                            >
                              Proj Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "23.25%"
                                    : "22.35%",
                              }}
                            >
                              Description
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "15.35%"
                                    : "15.35%",
                              }}
                            >
                              Received Date
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "12.7%"
                                    : "12.7%",
                              }}
                            >
                              Planned Del Date
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "12.65%"
                                    : "12.65%",
                              }}
                            >
                              Batches
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "9.8%"
                                    : "9.75%",
                              }}
                            >
                              Project Type
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.projects.length < 12
                                    ? "13%"
                                    : "13%",
                              }}
                            >
                              Input Count
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div
                      style={
                        (this.state.filteredArray.length > 12 &&
                          this.state.filterValue !== "") ||
                        (this.state.projects.length > 12 &&
                          this.state.filterValue === "")
                          ? {
                              height: "350px",
                              overflowY: "scroll",
                              borderBottom: "1px solid #cdd4e0",
                            }
                          : {}
                      }
                      ref={this.divScrollRef}
                      className="scrollable-element"
                      onScroll={this.handleScroll}
                    >
                      <BootstrapTable
                        bootstrap4
                        {...props.baseProps}
                        striped
                        hover
                        condensed
                        headerClasses="header-class"
                      />
                      <div className="col-md-10">
                        {((this.state.index <= this.state.projects.length &&
                          this.state.filterValue === "") ||
                          this.state.index <=
                            this.state.filteredArray.length) && (
                          <p>loading more rows, please scroll...</p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
            {this.state.position > 600 && this.state.filterValue === "" && (
              <div style={{ textAlign: "end" }}>
                <button className="scroll-top" onClick={this.scrollToTop}>
                  <div className="arrow up"></div>
                </button>
              </div>
            )}
          </div>
        </LoadingOverlay>
      </>
    );
  }
}

export default NotStartedProjectList;
