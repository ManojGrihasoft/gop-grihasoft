import React, { Component } from "react";
import projectService from "../../services/project.service";
import inputOutputFormatService from "../../services/inputOutputFormat.service";
import projectActivityService from "../../services/projectActivity.service";
import helper from "../../helpers/helpers";
import { Button, Modal } from "react-bootstrap";
import ModernDatepicker from "react-modern-datepicker";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import Moment from "moment";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link } from "react-router-dom";
toast.configure();

class EditProject extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.fetchProjectActivities = this.fetchProjectActivities.bind(this);

    //#region Binding Project controls
    this.onChangeTypeOfInput = this.onChangeTypeOfInput.bind(this);
    this.onChangeInputCount = this.onChangeInputCount.bind(this);
    this.onChangeInputCountType = this.onChangeInputCountType.bind(this);
    this.clearPlannedStartDate = this.clearPlannedStartDate.bind(this);

    this.uploadCustomerInputFile = this.uploadCustomerInputFile.bind(this);
    this.downloadCustomerInputFile = this.downloadCustomerInputFile.bind(this);
    this.deleteCustomerInputFile = this.deleteCustomerInputFile.bind(this);
    this.onChangeRemarks = this.onChangeRemarks.bind(this);

    this.onChangePlannedStartDate = this.onChangePlannedStartDate.bind(this);
    this.onChangeIsResourceBased = this.onChangeIsResourceBased.bind(this);
    //#endregion

    //#region Binding Project Received Details
    this.onChangeReceivedDate = this.onChangeReceivedDate.bind(this);
    this.onChangeReceivedFormat = this.onChangeReceivedFormat.bind(this);
    this.clearReceivedDate = this.clearReceivedDate.bind(this);
    //#endregion

    //#region Binding Project Delivery Details
    this.onChangeOutputFormat = this.onChangeOutputFormat.bind(this);
    this.onChangeDeliveryMode = this.onChangeDeliveryMode.bind(this);
    this.onChangepPlannedDeliveryDate =
      this.onChangePlannedDeliveryDate.bind(this);
    this.clearPlannedDeliveryDate = this.clearPlannedDeliveryDate.bind(this);

    this.uploadDeliveryPlanFile = this.uploadDeliveryPlanFile.bind(this);
    this.downloadDeliveryPlanFile = this.downloadDeliveryPlanFile.bind(this);
    this.deleteDeliveryPlanFile = this.deleteDeliveryPlanFile.bind(this);
    //#endregion

    //#region Binding Project Scope
    this.onChangeScope = this.onChangeScope.bind(this);
    this.uploadScopeFile = this.uploadScopeFile.bind(this);
    this.downloadScopeFile = this.downloadScopeFile.bind(this);
    this.deleteScopeFile = this.deleteScopeFile.bind(this);
    //#endregion

    //#region Binding Project Guideline
    this.onChangeGuideline = this.onChangeGuideline.bind(this);
    this.uploadGuidelineFile = this.uploadGuidelineFile.bind(this);
    this.downloadGuidelineFile = this.downloadGuidelineFile.bind(this);
    this.deleteGuidelineFile = this.deleteGuidelineFile.bind(this);
    //#endregion

    //#region Binding Project Checklist
    this.onChangeChecklist = this.onChangeChecklist.bind(this);
    this.uploadChecklistFile = this.uploadChecklistFile.bind(this);
    this.downloadChecklistFile = this.downloadChecklistFile.bind(this);
    this.deleteChecklistFile = this.deleteChecklistFile.bind(this);
    //#endregion

    //#region Binding Project Client Details
    this.onChangeEmailDate = this.onChangeEmailDate.bind(this);
    this.onChangeEmailDescription = this.onChangeEmailDescription.bind(this);
    this.clearEmailDate = this.clearEmailDate.bind(this);
    //#endregion

    //#region Binding Project Activity Details
    this.handleYes = this.handleYes.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.onChangeProjectActivity = this.onChangeProjectActivity.bind(this);
    this.onChangeNoOfSKUs = this.onChangeNoOfSKUs.bind(this);
    this.onChangeProductionTarget = this.onChangeProductionTarget.bind(this);
    this.onChangeQCTarget = this.onChangeQCTarget.bind(this);
    this.onChangeQATarget = this.onChangeQATarget.bind(this);

    this.AddToProjectActivityList = this.AddToProjectActivityList.bind(this);
    this.deleteProjectActivityRow = this.deleteProjectActivityRow.bind(this);
    //#endregion

    this.closeEditProjectActivityTagetModal =
      this.closeEditProjectActivityTagetModal.bind(this);

    this.onEditNoOfSKUs = this.onEditNoOfSKUs.bind(this);
    this.onEditProductionTarget = this.onEditProductionTarget.bind(this);
    this.onEditQCTarget = this.onEditQCTarget.bind(this);
    this.onEditQATarget = this.onEditQATarget.bind(this);

    this.reset = this.reset.bind(this);

    //Component State
    this.state = {
      //#region Project
      loading: false,
      spinnerMessage: "",
      projectID: 0,
      customerCode: "",
      projectCode: "",
      projectType: "",
      location: "",
      selectedTypeOfInput: "",
      inputCount: 0,
      selectedInputCountType: "",
      plannedStartDate: "",
      remarks: "",
      isResourceBased: false,
      customerInputFile: "",
      customerInputFileUploadedName: "",
      messageForCustomerInputFile: false,
      customerInputFileKey: Date.now(),
      showCustomerInputFileLabel: true,
      //#endregion

      //#region Project Received Details
      receivedDate: "",
      InputOutputFormats: [],
      selectedReceivedFormat: "",
      //#endregion

      //#region Project Delivery Details
      selectedOutputFormat: "",
      selectedDeliveryMode: "",
      plannedDeliveryDate: "",
      deliveryPlanFileName: "",
      DeliveryPlanFileUploadedName: "",
      waitingMessageForPlannedDeliveryFile: false,
      deliveryPlanFileKey: Date.now(),
      showDeliverPlanFileLabel: true,
      //#endregion

      //#region Project Scope
      scope: "",
      scopeFileName: "",
      scopeFileUploadedName: "",
      messageForScopeFile: false,
      scopeFileKey: Date.now(),
      showScopeFileLabel: true,
      //#endregion

      //#region Project Guideline
      guideline: "",
      guidelineFileName: "",
      guidelineFileUploadedName: "",
      messageForGuidelineFile: false,
      guidelineFileKey: Date.now(),
      showGuidelineFileLabel: true,
      //#endregion

      //#region Project Checklist
      checklist: "",
      checklistFileName: "",
      checklistFileUploadedName: "",
      messageForChecklistFile: false,
      checklistFileKey: Date.now(),
      showChecklistFileLabel: true,
      //#endregion

      //#region Project Client Details
      emailDate: "",
      emailDescription: "",
      //#endregion

      //#region Add Project Activity Details
      ProjectActivities: [],
      selectedProjectActivity: "",
      NoOfSKUs: 0,
      productionTarget: 0,
      QCTarget: 0,
      QATarget: 0,
      projectActivityDetails: [],
      formErrors: {},
      projectActivityFormErrors: {},
      showModal: false,
      //#endregion

      //#region Edit Project Activity Details
      selectedActivityRow: [],
      editNoOfSKUs: 0,
      editDailyProductionTarget: 0,
      editDailyQCTarget: 0,
      editDailyQATarget: 0,
      AllocatedCount: 0,
      editableRowIndex: 0,
      showEditProjectActivityTagetModal: false,
      editProjectActivityFormErrors: {},
      //#endregion

      modalLoading: false,
    };

    this.initialState = this.state;
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    window.scrollTo(0, 0);
    this.fetchProjectDetails();
    this.fetchInputOutputFormats();
  }
  //#endregion

  //#region Component Will Unmount
  componentWillUnmount() {
    this.setState(this.initialState);
  }
  //#endregion

  //#region fetching project Details from Web API
  fetchProjectDetails() {
    const { state } = this.props.location; // Project ID passed from View Project Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Projects");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Project Details...",
      loading: true,
    });

    projectService
      .getProjectDetailsByID(state, helper.getUser())
      .then((response) => {
        if (response.data.ProjectType === "Regular")
          this.setState({ projectType: "R" });
        else this.setState({ projectType: "P" });

        if (response.data.TypeOfInput === "Single")
          this.setState({ selectedTypeOfInput: "S" });
        else this.setState({ selectedTypeOfInput: "R" });

        if (response.data.InputCountType === "Items / Lines")
          this.setState({ selectedInputCountType: "I" });
        else this.setState({ selectedInputCountType: "D" });

        if (response.data.DeliveryMode === "Single")
          this.setState({ selectedDeliveryMode: "S" });
        else this.setState({ selectedDeliveryMode: "M" });

        if (response.data.ReceivedDate)
          this.setState({
            receivedDate: Moment(response.data.ReceivedDate).format(
              "DD-MMM-yyyy"
            ),
          });
        else
          this.setState({
            receivedDate: "",
          });

        if (response.data.PlannedStartDate)
          this.setState({
            plannedStartDate: Moment(response.data.PlannedStartDate).format(
              "DD-MMM-yyyy"
            ),
          });
        else
          this.setState({
            plannedStartDate: "",
          });

        if (response.data.PlannedDeliveryDate)
          this.setState({
            plannedDeliveryDate: Moment(
              response.data.PlannedDeliveryDate
            ).format("DD-MMM-yyyy"),
          });
        else
          this.setState({
            plannedDeliveryDate: "",
          });

        if (response.data.EmailDate)
          this.setState({
            emailDate: Moment(response.data.EmailDate).format("DD-MMM-yyyy"),
          });
        else
          this.setState({
            emailDate: "",
          });

        this.setState({
          projectID: response.data.ProjectID,
          customerCode: response.data.CustomerCode,
          projectCode: response.data.ProjectCode,
          location: response.data.LocationCode,
          inputCount: response.data.InputCount,
          selectedReceivedFormat: response.data.ReceivedFormat,
          selectedOutputFormat: response.data.OutputFormat,
          deliveryPlanFileName: response.data.DeliveryPlanFileName,
          DeliveryPlanFileUploadedName: response.data.DeliveryPlanFileName,
          isResourceBased: response.data.IsResourceBased,
          remarks: response.data.Remarks,
          customerInputFile: response.data.CustomerInputFileName,
          customerInputFileUploadedName: response.data.CustomerInputFileName,
          scope: response.data.Scope,
          scopeFileName: response.data.ScopeFileName,
          scopeFileUploadedName: response.data.ScopeFileName,
          guideline: response.data.Guideline,
          guidelineFileName: response.data.GuidelineFileName,
          guidelineFileUploadedName: response.data.GuidelineFileName,
          checklist: response.data.Checklist,
          checklistFileName: response.data.ChecklistFileName,
          checklistFileUploadedName: response.data.ChecklistFileName,
          emailDescription: response.data.EmailDescription,
          projectActivityDetails: response.data.Activities,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Input Output Formats from Service
  fetchInputOutputFormats() {
    this.setState({
      spinnerMessage: "Please wait while loading input output formats...",
      loading: true,
    });

    inputOutputFormatService
      .getAllInputOutputFormats(helper.getUser(), true)
      .then((response) => {
        this.setState({
          InputOutputFormats: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Project Activities from Service
  fetchProjectActivities() {
    this.setState({
      spinnerMessage: "Please wait while loading project activities...",
      loading: true,
    });

    projectActivityService
      .getAllActivities(helper.getUser(), true)
      .then((response) => {
        this.setState({
          ProjectActivities: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Close Modal Pop Up
  handleNo() {
    this.setState({ showModal: false });
  }
  //#endregion

  //#region Display Modal Pop up
  handleYes() {
    this.setState({
      showModal: true,
      selectedProjectActivity: "",
      NoOfSKUs: "",
      productionTarget: "",
      QCTarget: "",
      QATarget: "",
      projectActivityFormErrors: "",
    });
  }
  //#endregion

  //#region Get Selected Type of Input
  onChangeTypeOfInput(e) {
    this.setState({
      selectedTypeOfInput: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, typeOfInputError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Input Count value
  onChangeInputCount(e) {
    this.setState({
      inputCount: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, inputCountError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Selected Input Count Type
  onChangeInputCountType(e) {
    this.setState({
      selectedInputCountType: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, inputCountTypeError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region  Get Selected Received Date
  onChangeReceivedDate(date) {
    this.setState({
      receivedDate: date,
    });

    if (date !== "" && date !== null) {
      const formErrors = { ...this.state.formErrors, receivedDateError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Selected Received Format
  onChangeReceivedFormat(e) {
    this.setState({
      selectedReceivedFormat: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, receivedFormatError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Selected Output Format
  onChangeOutputFormat(e) {
    this.setState({
      selectedOutputFormat: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, outputFormatError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region  Get Selected Planned Start Date
  onChangePlannedStartDate(date) {
    this.setState({
      plannedStartDate: date,
    });

    if (date !== "" && date !== null) {
      const formErrors = {
        ...this.state.formErrors,
        plannedStartDateError: "",
      };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Selected Delivery Mode
  onChangeDeliveryMode(e) {
    this.setState({
      selectedDeliveryMode: e.target.value,
    });

    if (e.target.value === "S") {
      this.setState({
        deliveryPlanFileName: "",
        deliveryPlanFileKey: Date.now(),
        DeliveryPlanFileUploadedName: "",
      });
    } else {
      this.setState({ plannedDeliveryDate: "" });
    }

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, deliveryModeError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region  Get Selected Planned Delivery Date
  onChangePlannedDeliveryDate(date) {
    this.setState({
      plannedDeliveryDate: date,
    });

    if (date !== "" && date !== null) {
      const formErrors = {
        ...this.state.formErrors,
        plannedDeliveryDateError: "",
      };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Uploading Delivery Plan File
  uploadDeliveryPlanFile(e) {
    this.setState({
      waitingMessageForPlannedDeliveryFile: true,
    });
    var files = e.target.files;

    let currentFile = files[0];
    let fileNameUploaded = files[0].name;
    this.setState({
      DeliveryPlanFileUploadedName: fileNameUploaded,
    });

    let formData = new FormData();
    formData.append("File", currentFile);

    this.setState({
      spinnerMessage: "Please wait while uploading delivery plan file...",
      loading: true,
    });

    //Service call
    projectService
      .saveFileupload(formData)
      .then((response) => {
        this.setState({
          waitingMessageForPlannedDeliveryFile: false,
          deliveryPlanFileName: response.data,
          showDeliverPlanFileLabel: false,
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          waitingMessageForPlannedDeliveryFile: false,
          deliveryPlanFileName: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region Downloading Delivery Plan File
  downloadDeliveryPlanFile(e) {
    this.setState({
      spinnerMessage: "Please wait while downloading delivery plan file...",
      loading: true,
    });

    projectService
      .downloadFile(this.state.deliveryPlanFileName, "deliveryPlanFile")
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");

        fileLink.href = fileURL;
        fileLink.setAttribute(
          "download",
          this.state.DeliveryPlanFileUploadedName
        );
        document.body.appendChild(fileLink);
        this.setState({
          loading: false,
        });
        fileLink.click();
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });

        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Deleting Delivery Plan File
  deleteDeliveryPlanFile() {
    this.setState({
      spinnerMessage: "Please wait while deleting delivery plan file...",
      loading: true,
    });

    projectService
      .deleteFile(this.state.deliveryPlanFileName)
      .then((response) => {
        this.setState({
          deliveryPlanFileKey: Date.now(),
          deliveryPlanFileName: "",
          DeliveryPlanFileUploadedName: "",
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          deliveryPlanFileName: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region get Is Resource Based value
  onChangeIsResourceBased(e) {
    this.setState({
      isResourceBased: e.target.checked,
    });
  }
  //#endregion

  //#region Uploading CustomerInput File
  uploadCustomerInputFile(e) {
    this.setState({
      messageForCustomerInputFile: true,
    });
    var files = e.target.files;

    let currentFile = files[0];
    let fileNameUploaded = files[0].name;
    this.setState({
      customerInputFileUploadedName: fileNameUploaded,
    });

    let formData = new FormData();
    formData.append("File", currentFile);

    this.setState({
      spinnerMessage: "Please wait while uploading customer file...",
      loading: true,
    });

    //Service call
    projectService
      .saveFileupload(formData)
      .then((response) => {
        this.setState({
          messageForCustomerInputFile: false,
          customerInputFile: response.data,
          showCustomerInputFileLabel: false,
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          messageForCustomerInputFile: false,
          customerInputFile: "",
          loading: false,
        });
      });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.formErrors,
        customerInputFileError: "",
      };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Downloading customer Input File
  downloadCustomerInputFile(e) {
    this.setState({
      spinnerMessage: "Please wait while downloading customer file...",
      loading: true,
    });

    projectService
      .downloadFile(this.state.customerInputFile, "customerInputFile")
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");

        fileLink.href = fileURL;
        fileLink.setAttribute(
          "download",
          this.state.customerInputFileUploadedName
        );
        document.body.appendChild(fileLink);

        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Delete Customer Input File
  deleteCustomerInputFile() {
    this.setState({
      spinnerMessage: "Please wait while deleting customer file...",
      loading: true,
    });

    projectService
      .deleteFile(this.state.customerInputFile)
      .then((response) => {
        this.setState({
          customerInputFileKey: Date.now(),
          customerInputFile: "",
          customerInputFileUploadedName: "",
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          customerInputFile: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region get Remarks value
  onChangeRemarks(e) {
    this.setState({
      remarks: e.target.value,
    });
  }
  //#endregion

  //#region getScope value
  onChangeScope(e) {
    this.setState({
      scope: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, scopeError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Uploading Scope File
  uploadScopeFile(e) {
    this.setState({
      messageForScopeFile: true,
    });
    var files = e.target.files;

    let currentFile = files[0];
    let fileNameUploaded = files[0].name;
    this.setState({
      scopeFileUploadedName: fileNameUploaded,
    });

    let formData = new FormData();
    formData.append("File", currentFile);

    this.setState({
      spinnerMessage: "Please wait while uploading scope file...",
      loading: true,
    });

    //Service call
    projectService
      .saveFileupload(formData)
      .then((response) => {
        this.setState({
          messageForScopeFile: false,
          scopeFileName: response.data,
          showScopeFileLabel: false,
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          messageForScopeFile: false,
          scopeFileName: "",
          loading: false,
        });
      });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, scopeError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Downloading Scope File
  downloadScopeFile(e) {
    this.setState({
      spinnerMessage: "Please wait while downloading scope file...",
      loading: true,
    });

    projectService
      .downloadFile(this.state.scopeFileName, "scope")
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");

        fileLink.href = fileURL;
        fileLink.setAttribute("download", this.state.scopeFileUploadedName);
        document.body.appendChild(fileLink);

        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Deleting Scope File
  deleteScopeFile() {
    this.setState({
      spinnerMessage: "Please wait while deleting scope file...",
      loading: true,
    });

    projectService
      .deleteFile(this.state.scopeFileName)
      .then((response) => {
        this.setState({
          scopeFileKey: Date.now(),
          scopeFileName: "",
          scopeFileUploadedName: "",
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          scopeFileName: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region get Guideline value
  onChangeGuideline(e) {
    this.setState({
      guideline: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, guidelineError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Uploading Guideline File
  uploadGuidelineFile(e) {
    this.setState({
      messageForGuidelineFile: true,
    });
    var files = e.target.files;

    let currentFile = files[0];
    let fileNameUploaded = files[0].name;
    this.setState({
      guidelineFileUploadedName: fileNameUploaded,
    });

    let formData = new FormData();
    formData.append("File", currentFile);

    this.setState({
      spinnerMessage: "Please wait while uploading guideline file...",
      loading: true,
    });

    //Service call
    projectService
      .saveFileupload(formData)
      .then((response) => {
        this.setState({
          messageForGuidelineFile: false,
          guidelineFileName: response.data,
          showGuidelineFileLabel: false,
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          messageForGuidelineFile: false,
          guidelineFileName: "",
          loading: false,
        });
      });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, guidelineError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Downloading Guideline File
  downloadGuidelineFile(e) {
    this.setState({
      spinnerMessage: "Please wait while downloading guideline file...",
      loading: true,
    });

    projectService
      .downloadFile(this.state.guidelineFileName, "guidelines")
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");

        fileLink.href = fileURL;
        fileLink.setAttribute("download", this.state.guidelineFileUploadedName);
        document.body.appendChild(fileLink);

        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });

        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Deleting guideline File
  deleteGuidelineFile() {
    this.setState({
      spinnerMessage: "Please wait while deleting guideline file...",
      loading: true,
    });

    projectService
      .deleteFile(this.state.guidelineFileName)
      .then((response) => {
        this.setState({
          guidelineFileKey: Date.now(),
          guidelineFileName: "",
          guidelineFileUploadedName: "",
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          guidelineFileName: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region get Checklist value
  onChangeChecklist(e) {
    this.setState({
      checklist: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, checklistError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Uploading Checlist File
  uploadChecklistFile(e) {
    this.setState({
      messageForChecklistFile: true,
    });
    var files = e.target.files;

    let currentFile = files[0];
    let fileNameUploaded = files[0].name;
    this.setState({
      checklistFileUploadedName: fileNameUploaded,
    });

    let formData = new FormData();
    formData.append("File", currentFile);

    this.setState({
      spinnerMessage: "Please wait while uploading checklist file...",
      loading: true,
    });

    //Service call
    projectService
      .saveFileupload(formData)
      .then((response) => {
        this.setState({
          messageForChecklistFile: false,
          checklistFileName: response.data,
          showChecklistFileLabel: false,
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          messageForChecklistFile: false,
          checklistFileName: "",
          loading: false,
        });
      });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = { ...this.state.formErrors, checklistError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Downloading Checklist File
  downloadChecklistFile(e) {
    this.setState({
      spinnerMessage: "Please wait while downloading checklist file...",
      loading: true,
    });

    projectService
      .downloadFile(this.state.checklistFileName, "checklist")
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");

        fileLink.href = fileURL;
        fileLink.setAttribute("download", this.state.checklistFileUploadedName);
        document.body.appendChild(fileLink);

        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });

        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Deleting Checklist File
  deleteChecklistFile() {
    this.setState({
      spinnerMessage: "Please wait while deleting checklist file...",
      loading: true,
    });

    projectService
      .deleteFile(this.state.checklistFileName)
      .then((response) => {
        this.setState({
          checklistFileKey: Date.now(),
          checklistFileName: "",
          checklistFileUploadedName: "",
          loading: false,
        });
      })
      .catch((error) => {
        toast.error(error.response.data.Message, { autoClose: false });
        this.setState({
          checklistFileName: "",
          loading: false,
        });
      });
  }
  //#endregion

  //#region Get Selected Email Date
  onChangeEmailDate(date) {
    this.setState({
      emailDate: date,
    });
  }
  //#endregion

  //#region Get Email Description value
  onChangeEmailDescription(e) {
    this.setState({
      emailDescription: e.target.value,
    });
  }
  //#endregion

  //#region Rendering Table body data
  renderTableData() {
    return this.state.projectActivityDetails.map((projectActivity, index) => {
      const {
        Activity,
        NoOfSKUs,
        ProductionTarget,
        QCTarget,
        QATarget,
        AllocatedCount,
      } = projectActivity;
      return (
        <tr key={index}>
          <td>{Activity}</td>
          <td align="center">{NoOfSKUs}</td>
          <td align="center">{ProductionTarget}</td>
          <td align="center">{QCTarget}</td>
          <td align="center">{QATarget}</td>
          <td align="center">{AllocatedCount}</td>
          <td align="center">
            <i
              className="fas fa-pencil-alt pointer"
              onClick={this.showModalToEditProjectActivityRow(index)}
            ></i>
          </td>
          <td align="center">
            {AllocatedCount === 0 && (
              <i
                className="fas fa-trash-alt pointer"
                onClick={this.deleteProjectActivityRow(index)}
              ></i>
            )}
          </td>
        </tr>
      );
    });
  }
  //#endregion

  //#region Rendering Table Header data
  renderTableHeader() {
    return (
      <tr>
        <th className="bg-white">Project Activity</th>
        <th className="text-center bg-white">No. of SKUs</th>
        <th className="text-center bg-white">Production Target</th>
        <th className="text-center bg-white">QC Target</th>
        <th className="text-center bg-white">QA Target</th>
        <th className="text-center bg-white">Allocated Count</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    );
  }
  //#endregion

  //#region Get Selected Project Activity
  onChangeProjectActivity(e) {
    this.setState({
      selectedProjectActivity: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.projectActivityFormErrors,
        projectActivityError: "",
        duplicateProjectActivityError: "",
      };
      this.setState({ projectActivityFormErrors: formErrors });
    }
  }
  //#endregion

  //#region Get No. of SKUs
  onChangeNoOfSKUs(e) {
    this.setState({
      NoOfSKUs: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.projectActivityFormErrors,
        NoOfSKUsError: "",
      };
      this.setState({ projectActivityFormErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Production Target value
  onChangeProductionTarget(e) {
    this.setState({
      productionTarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.projectActivityFormErrors,
        productionTargetError: "",
      };
      this.setState({ projectActivityFormErrors: formErrors });
    }
  }
  //#endregion

  //#region Get QC Target value
  onChangeQCTarget(e) {
    this.setState({
      QCTarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.projectActivityFormErrors,
        QCTargetError: "",
      };
      this.setState({ projectActivityFormErrors: formErrors });
    }
  }
  //#endregion

  //#region Get QA Target value
  onChangeQATarget(e) {
    this.setState({
      QATarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const formErrors = {
        ...this.state.projectActivityFormErrors,
        QATargetError: "",
      };
      this.setState({ projectActivityFormErrors: formErrors });
    }
  }
  //#endregion

  //#region Add to Project Activities List
  AddToProjectActivityList(e) {
    e.preventDefault();
    if (this.handleProjectActivityFormValidation()) {
      let projectActivityDetails = [...this.state.projectActivityDetails];

      projectActivityDetails.push({
        Activity: this.state.selectedProjectActivity,
        NoOfSKUs: parseInt(this.state.NoOfSKUs),
        ProductionTarget: parseInt(this.state.productionTarget),
        QCTarget: parseInt(this.state.QCTarget),
        QATarget: parseInt(this.state.QATarget),
        AllocatedCount: 0,
        IsActivityAllocated: 0,
      });

      const formErrors = {
        ...this.state.formErrors,
        projectActivityTargetError: "",
      };

      this.setState({
        projectActivityDetails,
        formErrors: formErrors,
        showModal: false,
        selectedProjectActivity: "",
      });
    }
  }
  //#endregion

  //#region Validating the project Activity Target data
  handleProjectActivityFormValidation() {
    const projectActivity = this.state.selectedProjectActivity.trim();
    const inputCount = this.state.inputCount;
    const NoOfSKUs = this.state.NoOfSKUs;
    const productionTarget = this.state.productionTarget;
    const QCTarget = this.state.QCTarget;
    const QATarget = this.state.QATarget;
    let formErrors = {};
    let isValidForm = true;

    //Project Activity
    if (!projectActivity) {
      isValidForm = false;
      formErrors["projectActivityError"] = "Project Activity is required";
    }

    //No. of SKUs
    if (!NoOfSKUs) {
      isValidForm = false;
      formErrors["NoOfSKUsError"] = "No. of SKUs is required";
    }

    if (parseInt(NoOfSKUs) > parseInt(inputCount)) {
      isValidForm = false;
      formErrors["NoOfSKUsError"] =
        "No. of SKUs cannot be more than input count";
    }

    //Production Target
    if (!productionTarget) {
      isValidForm = false;
      formErrors["productionTargetError"] = "Production Target is required";
    }

    //QC Target
    if (!QCTarget) {
      isValidForm = false;
      formErrors["QCTargetError"] = "QC Target is required";
    }

    //QA Target
    if (!QATarget) {
      isValidForm = false;
      formErrors["QATargetError"] = "QA Target is required";
    }

    // Find if the array contains an object by comparing the property value
    if (
      this.state.projectActivityDetails.some(
        (details) => details.Activity === projectActivity
      )
    ) {
      isValidForm = false;
      formErrors["duplicateProjectActivityError"] =
        "Project Activity already exists";
    }

    this.setState({ projectActivityFormErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Delete Specific Project Activity Row
  deleteProjectActivityRow = (index) => () => {
    const projectActivityDetails = [...this.state.projectActivityDetails];
    projectActivityDetails.splice(index, 1);
    this.setState({ projectActivityDetails });
  };
  //#endregion

  //#region Show Pop Up to View Project Activity Row
  showModalToEditProjectActivityRow = (index) => () => {
    const projectActivityDetails = [...this.state.projectActivityDetails];
    const selectedActivityRow = Object.values(projectActivityDetails[index]);
    this.setState({
      selectedActivityRow: selectedActivityRow,
      editNoOfSKUs: selectedActivityRow[1],
      editDailyProductionTarget: selectedActivityRow[2],
      editDailyQCTarget: selectedActivityRow[3],
      editDailyQATarget: selectedActivityRow[4],
      AllocatedCount: selectedActivityRow[5],
      editableRowIndex: index,
      showEditProjectActivityTagetModal: true,
      editProjectActivityFormErrors: {},
    });
  };
  //#endregion

  //#region On Edit No of SKUs
  onEditNoOfSKUs(e) {
    this.setState({
      editNoOfSKUs: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const editProjectActivityFormErrors = {
        ...this.state.editProjectActivityFormErrors,
        NoOfSKUsError: "",
      };
      this.setState({
        editProjectActivityFormErrors: editProjectActivityFormErrors,
      });
    }
  }
  //#endregion

  //#region On Edit Production Target
  onEditProductionTarget(e) {
    this.setState({
      editDailyProductionTarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const editProjectActivityFormErrors = {
        ...this.state.editProjectActivityFormErrors,
        productionTargetError: "",
      };
      this.setState({
        editProjectActivityFormErrors: editProjectActivityFormErrors,
      });
    }
  }
  //#endregion

  //#region On Edit QC Target
  onEditQCTarget(e) {
    this.setState({
      editDailyQCTarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const editProjectActivityFormErrors = {
        ...this.state.editProjectActivityFormErrors,
        QCTargetError: "",
      };
      this.setState({
        editProjectActivityFormErrors: editProjectActivityFormErrors,
      });
    }
  }
  //#endregion

  //#region On Edit QA Target
  onEditQATarget(e) {
    this.setState({
      editDailyQATarget: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null) {
      const editProjectActivityFormErrors = {
        ...this.state.editProjectActivityFormErrors,
        QATargetError: "",
      };
      this.setState({
        editProjectActivityFormErrors: editProjectActivityFormErrors,
      });
    }
  }
  //#endregion

  //#region Validating the Edit Project Activity Target data
  handleEditProjectActivityFormValidation() {
    const NoOfSKUs = this.state.editNoOfSKUs;
    const inputCount = this.state.inputCount;
    const productionTarget = this.state.editDailyProductionTarget;
    const QCTarget = this.state.editDailyQCTarget;
    const QATarget = this.state.editDailyQATarget;
    const AllocatedCount = this.state.AllocatedCount;
    let formErrors = {};
    let isValidForm = true;

    //No. of SKUs
    if (!NoOfSKUs) {
      isValidForm = false;
      formErrors["NoOfSKUsError"] = "No. of SKUs is required";
    }

    if (parseInt(NoOfSKUs) > parseInt(inputCount)) {
      isValidForm = false;
      formErrors["NoOfSKUsError"] =
        "No. of SKUs cannot be more than input count";
    }

    if (AllocatedCount > 0) {
      if (parseInt(NoOfSKUs) < parseInt(AllocatedCount)) {
        isValidForm = false;
        formErrors["NoOfSKUsError"] =
          "No. of SKUs cannot be less than allocated count";
      }
    }

    //Production Target
    if (!productionTarget) {
      isValidForm = false;
      formErrors["productionTargetError"] = "Production Target is required";
    }

    //QC Target
    if (!QCTarget) {
      isValidForm = false;
      formErrors["QCTargetError"] = "QC Target is required";
    }

    //QA Target
    if (!QATarget) {
      isValidForm = false;
      formErrors["QATargetError"] = "QA Target is required";
    }

    this.setState({ editProjectActivityFormErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Edit Project Activity Target Details
  EditProjectActivityTargetDetails = (e) => {
    e.preventDefault();

    if (this.handleEditProjectActivityFormValidation()) {
      let projectActivityDetails = this.state.projectActivityDetails;

      projectActivityDetails[this.state.editableRowIndex].NoOfSKUs =
        this.state.editNoOfSKUs;
      projectActivityDetails[this.state.editableRowIndex].ProductionTarget =
        this.state.editDailyProductionTarget;
      projectActivityDetails[this.state.editableRowIndex].QCTarget =
        this.state.editDailyQCTarget;
      projectActivityDetails[this.state.editableRowIndex].QATarget =
        this.state.editDailyQATarget;

      this.setState({
        projectActivityDetails: projectActivityDetails,
        showEditProjectActivityTagetModal: false,
      });
    }
  };
  //#endregion

  //#region Close Edit Project Activity Taget Modal
  closeEditProjectActivityTagetModal() {
    this.setState({
      showEditProjectActivityTagetModal: false,
    });
  }
  //#endregion

  //#region Reset the page
  reset() {
    this.setState(this.initialState);
    this.setState({
      deliveryPlanFileKey: Date.now(),
      customerInputFileKey: Date.now(),
      scopeFileKey: Date.now(),
      guidelineFileKey: Date.now(),
      checklistFileKey: Date.now(),
    });
    this.componentDidMount();
  }
  //#endregion

  //#region Clearing Dates
  clearEmailDate() {
    this.setState({ emailDate: "" });
  }

  clearPlannedDeliveryDate() {
    this.setState({ plannedDeliveryDate: "" });
  }

  clearPlannedStartDate() {
    this.setState({ plannedStartDate: "" });
  }

  clearReceivedDate() {
    this.setState({ receivedDate: "" });
  }
  //#endregion

  //#region Save Project
  saveProject = (e) => {
    e.preventDefault();

    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while Saving Project...",
        loading: true,
      });

      var data = {
        ProjectID: this.state.projectID,
        CustomerCode: this.state.customerCode,
        ProjectCode: this.state.projectCode,
        ProjectType: this.state.projectType,
        LocationCode: this.state.location,
        TypeOfInput: this.state.selectedTypeOfInput,
        InputCount: this.state.inputCount,
        InputCountType: this.state.selectedInputCountType,
        ReceivedDate: this.state.receivedDate,
        ReceivedFormat: this.state.selectedReceivedFormat,
        OutputFormat: this.state.selectedOutputFormat,
        PlannedStartDate: this.state.plannedStartDate,
        DeliveryMode: this.state.selectedDeliveryMode,
        PlannedDeliveryDate: this.state.plannedDeliveryDate,
        DeliveryPlanFileName: this.state.deliveryPlanFileName,
        IsResourceBased: this.state.isResourceBased,
        Remarks: this.state.remarks,
        CustomerInputFileName: this.state.customerInputFile,
        Scope: this.state.scope,
        ScopeFileName: this.state.scopeFileName,
        Guideline: this.state.guideline,
        GuidelineFileName: this.state.guidelineFileName,
        Checklist: this.state.checklist,
        ChecklistFileName: this.state.checklistFileName,
        EmailDate: this.state.emailDate,
        EmailDescription: this.state.emailDescription,
        Activities: this.state.projectActivityDetails,
        NoOfBatches: 0,
        Status: "In Progress",
        UserID: helper.getUser(),
      };

      //Service call
      projectService
        .updateProject(this.state.projectID, data)
        .then(() => {
          this.setState({
            loading: false,
          });
          toast.success("Project Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Projects",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  //#region Validating the input data
  handleFormValidation() {
    const typeOfInput = this.state.selectedTypeOfInput.trim();
    const inputCount = this.state.inputCount;
    const inputCountType = this.state.selectedInputCountType.trim();
    const receivedDate = this.state.receivedDate;
    const receivedFormat = this.state.selectedReceivedFormat.trim();
    const outputFormat = this.state.selectedOutputFormat.trim();
    const plannedStartDate = this.state.plannedStartDate;
    const deliveryMode = this.state.selectedDeliveryMode.trim();
    const plannedDeliveryDate = this.state.plannedDeliveryDate;
    const customerInputFile = this.state.customerInputFile;
    const scope = this.state.scope.trim();
    const scopeFile = this.state.scopeFileName.trim();
    const guideline = this.state.guideline.trim();
    const guidelineFile = this.state.guidelineFileName.trim();
    const checklist = this.state.checklist.trim();
    const checklistFile = this.state.checklistFileName.trim();
    const projectActivityTarget = this.state.projectActivityDetails;
    let formErrors = {};
    let isValidForm = true;

    //Type of Input
    if (!typeOfInput) {
      isValidForm = false;
      formErrors["typeOfInputError"] = "Type of Input is required";
    }

    //Input Count
    if (!inputCount) {
      isValidForm = false;
      formErrors["inputCountError"] = "Input Count is required";
    }

    //Input Count Type
    if (!inputCountType) {
      isValidForm = false;
      formErrors["inputCountTypeError"] = "Input Count Type is required";
    }

    //Received Date
    if (!receivedDate) {
      isValidForm = false;
      formErrors["receivedDateError"] = "Received Date is required";
    }

    //Received Format
    if (!receivedFormat) {
      isValidForm = false;
      formErrors["receivedFormatError"] = "Received Format is required";
    }

    //Output Format
    if (!outputFormat) {
      isValidForm = false;
      formErrors["outputFormatError"] = "Output Format is required";
    }

    //Planned Start Date
    if (!plannedStartDate) {
      isValidForm = false;
      formErrors["plannedStartDateError"] = "Planned Start Date is required";
    } else if (new Date(plannedStartDate) < new Date(receivedDate)) {
      isValidForm = false;
      formErrors["plannedStartDateError"] =
        "Planned Start Date can not be earlier than Received Date";
    }

    //Delivery Mode
    if (!deliveryMode) {
      isValidForm = false;
      formErrors["deliveryModeError"] = "Delivery Mode is required";
    }

    if (deliveryMode === "S") {
      //Planned Delivery Date
      if (!plannedDeliveryDate) {
        isValidForm = false;
        formErrors["plannedDeliveryDateError"] =
          "Planned Delivery Date is required";
      } else if (new Date(plannedDeliveryDate) < new Date(plannedStartDate)) {
        isValidForm = false;
        formErrors["plannedStartDateError"] =
          "Planned Delivery Date can not be earlier than Planned Start Date";
      }
    }

    //Customer Input File
    if (!customerInputFile) {
      isValidForm = false;
      formErrors["customerInputFileError"] = "Customer Input File is required";
    }

    //Scope
    if (!scope && !scopeFile) {
      isValidForm = false;
      formErrors["scopeError"] = "Either Scope or Scope File is required";
    }

    //Guideline
    if (!guideline && !guidelineFile) {
      isValidForm = false;
      formErrors["guidelineError"] =
        "Either Guideline or Guideline File is required";
    }

    //Checklist
    if (!checklist && !checklistFile) {
      isValidForm = false;
      formErrors["checklistError"] =
        "Either Checklist or Checklist File is required";
    }

    //Project Activity Target
    if (projectActivityTarget.length === 0) {
      isValidForm = false;
      formErrors["projectActivityTargetError"] =
        "At least one Project Activity is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  render() {
    const deliveryMode = this.state.selectedDeliveryMode;
    const waitingMessageForPlannedDeliveryFile =
      this.state.waitingMessageForPlannedDeliveryFile;
    const messageForCustomerInputFile = this.state.messageForCustomerInputFile;
    const deliveryPlanFileName = this.state.deliveryPlanFileName;
    const customerInputFile = this.state.customerInputFile;
    const scopeFileName = this.state.scopeFileName;
    const messageForScopeFile = this.state.messageForScopeFile;
    const guidelineFileName = this.state.guidelineFileName;
    const messageForGuidelineFile = this.state.messageForGuidelineFile;
    const checklistFileName = this.state.checklistFileName;
    const messageForChecklistFile = this.state.messageForChecklistFile;

    const showDeliverPlanFileLabel = this.state.showDeliverPlanFileLabel;
    const showCustomerInputFileLabel = this.state.showCustomerInputFileLabel;
    const showScopeFileLabel = this.state.showScopeFileLabel;
    const showGuidelineFileLabel = this.state.showGuidelineFileLabel;
    const showChecklistFileLabel = this.state.showChecklistFileLabel;

    let control;
    let DeliveryFileButtons;
    if (deliveryMode === "" || deliveryMode === "S") {
      control = (
        <>
          <label htmlFor="plannedDeliveryDate">
            Planned Delivery Date{" "}
            <span className="text-danger asterisk-size">*</span>
          </label>
          <div className="row mg-l-0">
            <div className="form-control date-field-width">
              <ModernDatepicker
                tabIndex="10"
                id="plannedDeliveryDate"
                name="plannedDeliveryDate"
                date={this.state.plannedDeliveryDate}
                format={"DD-MMM-YYYY"}
                onChange={(date) => this.onChangePlannedDeliveryDate(date)}
                placeholder={"Select a date"}
                className="color"
                minDate={new Date(1900, 1, 1)}
              />
            </div>
            <span
              className="btn btn-secondary"
              onClick={this.clearPlannedDeliveryDate}
            >
              <i
                className="far fa-window-close"
                title="Clear Planned Delivery Date"
              ></i>
            </span>
          </div>
          <div className="error-message">
            {this.state.formErrors["plannedDeliveryDateError"]}
          </div>
        </>
      );
      DeliveryFileButtons = <></>;
    } else {
      control = (
        <>
          <label htmlFor="DeliveryPlanFile">Delivery Plan File</label>
          <input
            type="file"
            className="form-control"
            tabIndex="10"
            id="DeliveryPlanFile"
            name="DeliveryPlanFile"
            key={this.state.deliveryPlanFileKey}
            onChange={this.uploadDeliveryPlanFile}
            accept=".xls, .xlsx,.doc,.docx,.pdf"
          />
          {showDeliverPlanFileLabel && (
            <label htmlFor="DeliveryPlanFile">
              {this.state.DeliveryPlanFileUploadedName}
            </label>
          )}
          {waitingMessageForPlannedDeliveryFile && <p>Please Wait...</p>}
        </>
      );
      DeliveryFileButtons = (
        <>
          {deliveryPlanFileName && (
            <>
              <span
                className="btn btn-secondary"
                onClick={this.downloadDeliveryPlanFile}
              >
                <i className="fas fa-download"></i>
              </span>
              <span
                className="btn btn-secondary mg-l-5"
                onClick={this.deleteDeliveryPlanFile}
              >
                <i className="fas fa-trash-alt"></i>
              </span>
            </>
          )}
        </>
      );
    }

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <>
        <LoadingOverlay
          active={this.state.loading}
          styles={{
            content: {
              marginTop: "1150px",
              marginLeft: "520px",
            },
          }}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div>
            <form onSubmit={this.saveProject}>
              <div className="container">
                <div className="az-content-breadcrumb mg-t-20">
                  <span>Project</span>
                  <span>Edit Project</span>
                </div>
                <h4>
                  Edit Project{" "}
                  <span className="icon-size">
                    <Link
                      className="far fa-arrow-alt-circle-left text-primary pointer"
                      tabIndex="1"
                      to="/Projects"
                      title="Back to List"
                    ></Link>
                  </span>
                </h4>
                <br />
                <div id="Add_Project">
                  <div className="row row-sm">
                    <div className="col-lg">
                      <div className="row row-sm">
                        <div className="col-md-5 text-nowrap">
                          <label htmlFor="CustomerCode">
                            <b>Customer Code</b>{" "}
                            <span className="text-danger asterisk-size">*</span>
                          </label>
                        </div>
                        <div className="col-md-6 mg-t-7">
                          <p id="CustomerCode" name="CustomerCode">
                            {this.state.customerCode}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <div className="row row-sm">
                        <div className="col-md-5">
                          <label htmlFor="ProjectCode">
                            <b>Project Code</b>{" "}
                            <span className="text-danger asterisk-size">*</span>
                          </label>
                        </div>
                        <div className="col-md-6 mg-t-7">
                          <p id="ProjectCode" name="ProjectCode">
                            {this.state.projectCode}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <div className="row row-sm">
                    <div className="col-lg">
                      <div className="row row-sm">
                        <div className="col-md-5">
                          <label htmlFor="ProjectType">
                            <b>Project Type</b>{" "}
                            <span className="text-danger asterisk-size">*</span>
                          </label>
                        </div>
                        <div className="col-md-6 mg-t-7">
                          <p id="ProjectType" name="ProjectType">
                            {this.state.projectType === "R"
                              ? "Regular"
                              : "Pilot"}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <div className="row row-sm">
                        <div className="col-md-5">
                          <label htmlFor="Location">
                            <b>Location</b>{" "}
                            <span className="text-danger asterisk-size">*</span>
                          </label>
                        </div>
                        <div className="col-md-6 mg-t-7">
                          <p id="Location" name="Location">
                            {this.state.location}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="TypeOfInput">
                        Type of Input{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <select
                        className="form-control"
                        tabIndex="1"
                        id="TypeOfInput"
                        name="TypeOfInput"
                        value={this.state.selectedTypeOfInput}
                        onChange={this.onChangeTypeOfInput}
                      >
                        <option value="">--Select--</option>
                        <option key="S" value="S">
                          Single
                        </option>
                        <option key="R" value="R">
                          Recurring
                        </option>
                      </select>
                      <div className="error-message">
                        {this.state.formErrors["typeOfInputError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="InputCount">
                        Input Count{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        tabIndex="2"
                        id="InputCount"
                        name="InputCount"
                        value={this.state.inputCount}
                        onChange={this.onChangeInputCount}
                        max="9999999"
                        min="1"
                      />
                      <div className="error-message">
                        {this.state.formErrors["inputCountError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="InputCountType">
                        Input Count Type{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <select
                        className="form-control"
                        tabIndex="3"
                        id="InputCountType"
                        name="InputCountType"
                        value={this.state.selectedInputCountType}
                        onChange={this.onChangeInputCountType}
                      >
                        <option value="">--Select--</option>
                        <option key="Items/Lines" value="I">
                          Items / Lines
                        </option>
                        <option key="Document" value="D">
                          Document
                        </option>
                      </select>
                      <div className="error-message">
                        {this.state.formErrors["inputCountTypeError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="ReceivedDate">
                        Received Date{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <div className="row mg-l-0">
                        <div className="form-control date-field-width">
                          <ModernDatepicker
                            tabIndex="4"
                            id="ReceivedDate"
                            name="ReceivedDate"
                            date={this.state.receivedDate}
                            format={"DD-MMM-YYYY"}
                            onChange={(date) => this.onChangeReceivedDate(date)}
                            placeholder={"Select a date"}
                            className="color"
                            minDate={new Date(1900, 1, 1)}
                          />
                        </div>
                        <span
                          className="btn btn-secondary"
                          onClick={this.clearReceivedDate}
                        >
                          <i
                            className="far fa-window-close"
                            title="Clear Received Date"
                          ></i>
                        </span>
                      </div>
                      <div className="error-message">
                        {this.state.formErrors["receivedDateError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="ReceivedFormat">
                        Received Format{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <select
                        className="form-control"
                        tabIndex="5"
                        id="ReceivedFormat"
                        name="ReceivedFormat"
                        value={this.state.selectedReceivedFormat}
                        onChange={this.onChangeReceivedFormat}
                      >
                        <option value="">--Select--</option>
                        {this.state.InputOutputFormats.map((formats) => (
                          <option key={formats.Format}>{formats.Format}</option>
                        ))}
                      </select>
                      <div className="error-message">
                        {this.state.formErrors["receivedFormatError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="OutputFormat">
                        Output Format{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <select
                        className="form-control"
                        tabIndex="6"
                        id="OutputFormat"
                        name="OutputFormat"
                        value={this.state.selectedOutputFormat}
                        onChange={this.onChangeOutputFormat}
                      >
                        <option value="">--Select--</option>
                        {this.state.InputOutputFormats.map((formats) => (
                          <option key={formats.Format}>{formats.Format}</option>
                        ))}
                      </select>
                      <div className="error-message">
                        {this.state.formErrors["outputFormatError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="PlannedStartDate">
                        Planned Start Date{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <div className="row mg-l-0">
                        <div className="form-control date-field-width">
                          <ModernDatepicker
                            tabIndex="7"
                            id="PlannedStartDate"
                            name="PlannedStartDate"
                            date={this.state.plannedStartDate}
                            format={"DD-MMM-YYYY"}
                            onChange={(date) =>
                              this.onChangePlannedStartDate(date)
                            }
                            placeholder={"Select a date"}
                            className="color"
                            minDate={new Date(1900, 1, 1)}
                          />
                        </div>
                        <span
                          className="btn btn-secondary"
                          onClick={this.clearPlannedStartDate}
                        >
                          <i
                            className="far fa-window-close"
                            title="Clear Planned Start Date"
                          ></i>
                        </span>
                      </div>
                      <div className="error-message">
                        {this.state.formErrors["plannedStartDateError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="DeliveryMode">
                        Delivery Mode{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <select
                        className="form-control"
                        tabIndex="8"
                        id="DeliveryMode"
                        name="DeliveryMode"
                        value={this.state.selectedDeliveryMode}
                        onChange={this.onChangeDeliveryMode}
                      >
                        <option value="">--Select--</option>
                        <option key="Single" value="S">
                          Single
                        </option>
                        <option key="Multiple" value="M">
                          Multiple
                        </option>
                      </select>
                      <div className="error-message">
                        {this.state.formErrors["deliveryModeError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="IsResourceBased">
                        Is Resource Based?
                      </label>
                      <div className="mg-t-10">
                        <label className="checkbox-inline mg-l-10">
                          <input
                            type="checkbox"
                            tabIndex="9"
                            id="IsResourceBased"
                            name="IsResourceBased"
                            value={this.state.isResourceBased}
                            onChange={this.onChangeIsResourceBased}
                            checked={this.state.isResourceBased}
                          />
                        </label>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">{control}</div>
                    <div className="col-lg mg-t-10 mg-lg-t-0 pd-t-25">
                      {DeliveryFileButtons}
                    </div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="Remarks">Remarks</label>
                      <textarea
                        className="form-control"
                        rows="2"
                        tabIndex="11"
                        id="Remarks"
                        name="Remarks"
                        maxLength="500"
                        value={this.state.remarks}
                        onChange={this.onChangeRemarks}
                      ></textarea>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="CustomerInputFile">
                        Customer Input File{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </label>
                      <input
                        type="file"
                        className="form-control"
                        tabIndex="12"
                        id="CustomerInputFile"
                        name="CustomerInputFile"
                        key={this.state.customerInputFileKey}
                        onChange={this.uploadCustomerInputFile}
                        accept=".xls, .xlsx,.doc,.docx,.pdf"
                      />
                      <div className="error-message">
                        {this.state.formErrors["customerInputFileError"]}
                      </div>
                      {showCustomerInputFileLabel && (
                        <label htmlFor="CustomerInputFile">
                          {this.state.customerInputFileUploadedName}
                        </label>
                      )}
                      {messageForCustomerInputFile && <p>Please Wait...</p>}
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0 d-flex align-items-center">
                      {customerInputFile && (
                        <div>
                          <span
                            className="btn btn-secondary"
                            onClick={this.downloadCustomerInputFile}
                          >
                            <i className="fas fa-download"></i>
                          </span>
                          <span
                            className="btn btn-secondary mg-l-5"
                            onClick={this.deleteCustomerInputFile}
                          >
                            <i className="fas fa-trash-alt"></i>
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="Scope">Scope</label>
                      <textarea
                        className="form-control"
                        rows="2"
                        tabIndex="13"
                        id="Scope"
                        maxLength="500"
                        name="Scope"
                        value={this.state.scope}
                        onChange={this.onChangeScope}
                      ></textarea>
                      <div className="error-message">
                        {this.state.formErrors["scopeError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="ScopeFile">Scope File</label>
                      <input
                        type="file"
                        className="form-control"
                        tabIndex="14"
                        id="ScopeFile"
                        name="ScopeFile"
                        key={this.state.scopeFileKey}
                        onChange={this.uploadScopeFile}
                        accept=".xls, .xlsx,.doc,.docx,.pdf"
                      />
                      {showScopeFileLabel && (
                        <label htmlFor="ScopeFile">
                          {this.state.scopeFileUploadedName}
                        </label>
                      )}
                      {messageForScopeFile && <p>Please Wait...</p>}
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0 d-flex align-items-center">
                      {scopeFileName && (
                        <div>
                          <span
                            className="btn btn-secondary"
                            onClick={this.downloadScopeFile}
                          >
                            <i className="fas fa-download"></i>
                          </span>
                          <span
                            className="btn btn-secondary mg-l-5"
                            onClick={this.deleteScopeFile}
                          >
                            <i className="fas fa-trash-alt"></i>
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="Guideline">Guideline</label>
                      <textarea
                        className="form-control"
                        rows="2"
                        tabIndex="15"
                        id="Guideline"
                        name="Guideline"
                        maxLength="500"
                        value={this.state.guideline}
                        onChange={this.onChangeGuideline}
                      ></textarea>
                      <div className="error-message">
                        {this.state.formErrors["guidelineError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="GuidelineFile">Guideline File</label>
                      <input
                        type="file"
                        className="form-control"
                        tabIndex="16"
                        id="GuidelineFile"
                        name="GuidelineFile"
                        key={this.state.guidelineFileKey}
                        onChange={this.uploadGuidelineFile}
                        accept=".xls, .xlsx,.doc,.docx,.pdf"
                      />
                      {showGuidelineFileLabel && (
                        <label htmlFor="GuidelineFile">
                          {this.state.guidelineFileUploadedName}
                        </label>
                      )}
                      {messageForGuidelineFile && <p>Please Wait...</p>}
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0 d-flex align-items-center">
                      {guidelineFileName && (
                        <>
                          <span
                            className="btn btn-secondary"
                            onClick={this.downloadGuidelineFile}
                          >
                            <i className="fas fa-download"></i>
                          </span>
                          <span
                            className="btn btn-secondary mg-l-5"
                            onClick={this.deleteGuidelineFile}
                          >
                            <i className="fas fa-trash-alt"></i>
                          </span>
                        </>
                      )}
                    </div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="Checklist">Checklist</label>
                      <textarea
                        className="form-control"
                        rows="2"
                        tabIndex="17"
                        id="Checklist"
                        name="Checklist"
                        maxLength="500"
                        value={this.state.checklist}
                        onChange={this.onChangeChecklist}
                      ></textarea>
                      <div className="error-message">
                        {this.state.formErrors["checklistError"]}
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="ChecklistFile">Checklist File</label>
                      <input
                        type="file"
                        className="form-control"
                        aria-describedby="basic-addon1"
                        tabIndex="18"
                        id="ChecklistFile"
                        name="ChecklistFile"
                        key={this.state.checklistFileKey}
                        onChange={this.uploadChecklistFile}
                        accept=".xls, .xlsx,.doc,.docx,.pdf"
                      />
                      {showChecklistFileLabel && (
                        <label htmlFor="ChecklistFile">
                          {this.state.checklistFileUploadedName}
                        </label>
                      )}
                      {messageForChecklistFile && <p>Please Wait...</p>}
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0 d-flex align-items-center">
                      {checklistFileName && (
                        <>
                          <span
                            className="btn btn-secondary"
                            onClick={this.downloadChecklistFile}
                          >
                            <i className="fas fa-download"></i>
                          </span>
                          <span
                            className="btn btn-secondary mg-l-5"
                            onClick={this.deleteChecklistFile}
                          >
                            <i className="fas fa-trash-alt"></i>
                          </span>
                        </>
                      )}
                    </div>
                  </div>
                  <br />
                  <div className="row row-sm">
                    <div className="col-lg">
                      <label htmlFor="EmailDate">Email Date</label>
                      <div className="row mg-l-0">
                        <div className="form-control date-field-width">
                          <ModernDatepicker
                            tabIndex="19"
                            name="EmailDate"
                            id="EmailDate"
                            date={this.state.emailDate}
                            format={"DD-MMM-YYYY"}
                            onChange={(date) => this.onChangeEmailDate(date)}
                            placeholder={"Select a date"}
                            className="color"
                            minDate={new Date(1900, 1, 1)}
                          />
                        </div>
                        <span
                          className="btn btn-secondary"
                          onClick={this.clearEmailDate}
                        >
                          <i
                            className="far fa-window-close"
                            title="Clear Email Date"
                          ></i>
                        </span>
                      </div>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0">
                      <label htmlFor="emailDescription">Description</label>
                      <textarea
                        className="form-control"
                        rows="3"
                        tabIndex="20"
                        id="emailDescription"
                        name="emailDescription"
                        maxLength="4000"
                        value={this.state.emailDescription}
                        onChange={this.onChangeEmailDescription}
                      ></textarea>
                    </div>
                    <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                  </div>
                  <br />
                  <h4>
                    Project Activity Targets{" "}
                    <span className="icon-size">
                      <i
                        className="fa fa-plus text-primary pointer"
                        onClick={this.handleYes}
                        title="Add New Project Activity"
                        tabIndex="21"
                      ></i>
                    </span>
                  </h4>
                  <div className="error-message">
                    {this.state.formErrors["projectActivityTargetError"]}
                  </div>
                  <div className="table-responsive col-md-9">
                    <table
                      id="projectActivityDetails"
                      className="table table-hover"
                      tabIndex="22"
                    >
                      <thead>{this.renderTableHeader()}</thead>
                      <tbody>{this.renderTableData()}</tbody>
                    </table>
                  </div>
                  <div className="row row-sm mg-t-30">
                    <div className="col-md-1"></div>
                    <div className="col-md-2 mg-t-10 mg-lg-t-0">
                      <input
                        type="submit"
                        id="Save"
                        className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                        tabIndex="31"
                        value="Save"
                      />
                    </div>
                    <div className="col-md-1"></div>
                    <div className="col-md-2  mg-t-10 mg-lg-t-0">
                      <span
                        className="btn btn-gray-700 btn-block"
                        tabIndex="32"
                        onClick={this.reset}
                        id="Reset"
                      >
                        Reset
                      </span>
                    </div>
                  </div>
                  <div className="mg-b-10"></div>
                </div>
              </div>
            </form>
            <Modal
              show={this.state.showModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.handleNo}
              backdrop="static"
              enforceFocus={false}
              centered
            >
              <form onSubmit={this.AddToProjectActivityList}>
                <Modal.Header>
                  <Modal.Title>Add New Project Activity</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div>
                    <div className="row row-sm">
                      <div className="col-lg">
                        <label htmlFor="ProjectActivity">
                          Project Activity{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <select
                          className="form-control"
                          tabIndex="25"
                          id="ProjectActivity"
                          name="ProjectActivity"
                          value={this.state.selectedProjectActivity}
                          onChange={this.onChangeProjectActivity}
                          onFocus={this.fetchProjectActivities}
                          placeholder="--Select--"
                        >
                          <option value="">--Select--</option>
                          {this.state.ProjectActivities.map(
                            (projectActivities) => (
                              <option key={projectActivities.Activity}>
                                {projectActivities.Activity}
                              </option>
                            )
                          )}
                        </select>
                        <div className="error-message">
                          {
                            this.state.projectActivityFormErrors[
                              "projectActivityError"
                            ]
                          }
                        </div>
                        <label>
                          No. of SKUs{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="27"
                          className="form-control"
                          value={this.state.NoOfSKUs}
                          onChange={this.onChangeNoOfSKUs}
                          min="1"
                          max="9999999"
                        />
                        <div className="error-message">
                          {
                            this.state.projectActivityFormErrors[
                              "NoOfSKUsError"
                            ]
                          }
                        </div>
                        <label htmlFor="ProductionTarget">
                          Daily Production Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="28"
                          className="form-control"
                          id="ProductionTarget"
                          name="ProductionTarget"
                          value={this.state.productionTarget}
                          onChange={this.onChangeProductionTarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.projectActivityFormErrors[
                              "productionTargetError"
                            ]
                          }
                        </div>
                        <label htmlFor="QCTarget">
                          Daily QC Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="29"
                          className="form-control"
                          id="QCTarget"
                          name="QCTarget"
                          maxLength="4"
                          value={this.state.QCTarget}
                          onChange={this.onChangeQCTarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.projectActivityFormErrors[
                              "QCTargetError"
                            ]
                          }
                        </div>
                        <label htmlFor="QATarget">
                          Daily QA Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          className="form-control"
                          maxLength="4"
                          tabIndex="30"
                          id="QATarget"
                          name="QATarget"
                          value={this.state.QATarget}
                          onChange={this.onChangeQATarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.projectActivityFormErrors[
                              "QATargetError"
                            ]
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </Modal.Body>
                <div className="error-message mg-l-25">
                  {
                    this.state.projectActivityFormErrors[
                      "duplicateProjectActivityError"
                    ]
                  }
                </div>
                <Modal.Footer>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Add To List"
                    tabIndex="31"
                  />
                  <Button
                    variant="secondary"
                    onClick={this.handleNo}
                    tabIndex="30"
                  >
                    Cancel
                  </Button>
                </Modal.Footer>
              </form>
            </Modal>
            <Modal
              show={this.state.showEditProjectActivityTagetModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.closeEditProjectActivityTagetModal}
              backdrop="static"
              enforceFocus={false}
              centered
            >
              <form onSubmit={this.EditProjectActivityTargetDetails}>
                <Modal.Header>
                  <Modal.Title>
                    Edit Project Activity Target Details
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <div>
                    <div className="row row-sm">
                      <div className="col-lg">
                        <label htmlFor="ProjectActivity">
                          <b>Project Activity</b>
                        </label>
                        <p id="ProjectActivity" name="ProjectActivity">
                          {this.state.selectedActivityRow[0]}
                        </p>
                        <label>
                          No. of SKUs{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="27"
                          className="form-control"
                          value={this.state.editNoOfSKUs}
                          onChange={this.onEditNoOfSKUs}
                          min="1"
                          max="9999999"
                        />
                        <div className="error-message">
                          {
                            this.state.editProjectActivityFormErrors[
                              "NoOfSKUsError"
                            ]
                          }
                        </div>
                        <label htmlFor="ProductionTarget">
                          Daily Production Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="28"
                          className="form-control"
                          id="ProductionTarget"
                          name="ProductionTarget"
                          value={this.state.editDailyProductionTarget}
                          onChange={this.onEditProductionTarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.editProjectActivityFormErrors[
                              "productionTargetError"
                            ]
                          }
                        </div>
                        <label htmlFor="QCTarget">
                          Daily QC Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          tabIndex="29"
                          className="form-control"
                          id="QCTarget"
                          name="QCTarget"
                          maxLength="4"
                          value={this.state.editDailyQCTarget}
                          onChange={this.onEditQCTarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.editProjectActivityFormErrors[
                              "QCTargetError"
                            ]
                          }
                        </div>
                        <label htmlFor="QATarget">
                          Daily QA Target{" "}
                          <span className="text-danger asterisk-size">*</span>
                        </label>
                        <input
                          type="number"
                          className="form-control"
                          maxLength="4"
                          tabIndex="30"
                          id="QATarget"
                          name="QATarget"
                          value={this.state.editDailyQATarget}
                          onChange={this.onEditQATarget}
                          min="0"
                          max="9999"
                        />
                        <div className="error-message">
                          {
                            this.state.editProjectActivityFormErrors[
                              "QATargetError"
                            ]
                          }
                        </div>
                        <label htmlFor="AllocatedCount">
                          <b>Allocated Count</b>
                        </label>
                        <p id="AllocatedCount" name="AllocatedCount">
                          {this.state.AllocatedCount}
                        </p>
                      </div>
                    </div>
                  </div>
                </Modal.Body>
                <Modal.Footer>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Save"
                  />
                  <Button
                    variant="secondary"
                    onClick={this.closeEditProjectActivityTagetModal}
                  >
                    Cancel
                  </Button>
                </Modal.Footer>
              </form>
            </Modal>
          </div>
        </LoadingOverlay>
      </>
    );
  }
}

export default EditProject;
