import React, { Component } from "react";
import Nav from "react-bootstrap/Nav";
import Tab from "react-bootstrap/Tab";
import helper from "../../helpers/helpers";
import DeliveredProjectList from "./DeliveredProjectList";
import OnGoingProjectList from "./OnGoingProjectList";
import NotStartedProjectList from "./NotStartedProjectList";
import accessControlService from "../../services/accessControl.service";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import "./projects.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class ProjectList extends Component {
  constructor(props) {
    super(props);

    this.moveToCreateProject = this.moveToCreateProject.bind(this);

    this.state = {
      canAccessCreateProject: false,
      activeTab: null,
      defaultActiveKey: "",
      loading: false,
      spinnerMessage: "Please wait while loading...",
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.setActiveTab();
    this.canUserAccessPage("Create Project");
  }
  //#endregion

  //#region Set active Tab
  setActiveTab() {
    const { state } = this.props.location;

    if (state) {
      if (state.activeTab === 2) {
        this.setState({
          activeTab: 2,
          defaultActiveKey: "delivered",
        });
      } else {
        this.setState({
          activeTab: 1,
          defaultActiveKey: "onGoing",
        });
      }
    } else {
      this.setState({
        activeTab: 1,
        defaultActiveKey: "onGoing",
      });
    }
  }
  //#endregion

  //#region fetching Project page access
  canUserAccessPage(pageName) {
    this.setState({
      spinnerMessage: "Please wait while loading...",
      loading: true,
    });

    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        this.setState({
          canAccessCreateProject: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Redirect to Create Project Page
  moveToCreateProject() {
    this.props.history.push("/Projects/CreateProject");
  }
  //#endregion

  //#region Change Between On Going and Delivered Tab
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  }
  //#endregion

  //#region UI
  render() {
    const props = this.props;
    const canAccessCreateProject = this.state.canAccessCreateProject;
    return this.state.activeTab === null ? (
      <LoadingOverlay
        active={true}
        spinner={
          <div className="spinner-background">
            <BarLoader
              css={helper.getcss()}
              color={"#38D643"}
              width={"350px"}
              height={"10px"}
              speedMultiplier={0.3}
            />
            <p style={{ color: "black", marginTop: "5px" }}>
              {this.state.spinnerMessage}
            </p>
          </div>
        }
      >
        <p style={{ height: "580px" }}></p>
      </LoadingOverlay>
    ) : (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb mg-l-50">
            <span>Project</span>
            <span>List</span>
          </div>
          <h4 className="mg-l-50">
            Projects List{" "}
            {canAccessCreateProject && (
              <span className="icon-size">
                <i
                  className="fa fa-plus text-primary pointer"
                  onClick={this.moveToCreateProject}
                  title="Add New Project"
                ></i>
              </span>
            )}
          </h4>
          <Tab.Container defaultActiveKey={this.state.defaultActiveKey}>
            <Nav variant="pills" className="mg-l-50 mg-b-20">
              <Nav.Item>
                <Nav.Link
                  eventKey="onGoing"
                  style={{ border: "1px solid #5E41FC" }}
                  onClick={() => {
                    this.toggle(1);
                  }}
                >
                  On Going
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="delivered"
                  style={{ border: "1px solid #5E41FC" }}
                  onClick={() => {
                    this.toggle(2);
                  }}
                >
                  Delivered
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="notStarted"
                  style={{ border: "1px solid #5E41FC" }}
                  onClick={() => {
                    this.toggle(3);
                  }}
                >
                  Not Started
                </Nav.Link>
              </Nav.Item>
            </Nav>

            <Tab.Content>
              <Tab.Pane eventKey="onGoing">
                {this.state.activeTab === 1 ? (
                  <OnGoingProjectList {...props} />
                ) : null}
              </Tab.Pane>
              <Tab.Pane eventKey="delivered">
                {this.state.activeTab === 2 ? (
                  <DeliveredProjectList {...props} />
                ) : null}
              </Tab.Pane>
              <Tab.Pane eventKey="notStarted">
                {this.state.activeTab === 3 ? (
                  <NotStartedProjectList {...props} />
                ) : null}
              </Tab.Pane>
            </Tab.Content>
          </Tab.Container>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default ProjectList;
