import React, { Component } from "react";
import DashboardDetails from "./Dashboard/dashboardDetails";
import ActiveProjects from "./Dashboard/activeProjects";
import ActiveResources from "./Dashboard/activeResources";
import ActiveTasks from "./Dashboard/activeTasks";
import Duration from "./Dashboard/duration";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import helper from "../helpers/helpers";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class Dashboard extends Component {
  constructor(props) {
    super(props); //reference to the parent constructor

    this.state = {};
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }
  }
  //#endregion

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route path="/Dashboard" exact component={DashboardDetails}></Route>
            <Route path="/Dashboard/Duration" component={Duration}></Route>
            <Route
              path="/Dashboard/ActiveResources"
              component={ActiveResources}
            ></Route>
            <Route
              path="/Dashboard/ActiveTasks"
              component={ActiveTasks}
            ></Route>
            <Route
              path="/Dashboard/ActiveProjects"
              component={ActiveProjects}
            ></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Dashboard;
