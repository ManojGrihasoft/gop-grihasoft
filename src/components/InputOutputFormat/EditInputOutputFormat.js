import React, { Component } from "react";
import InputOutputFormatService from "../../services/inputOutputFormat.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class EditInputOutputFormat extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.onChangeFormatName = this.onChangeFormatName.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.moveToInputOutputFormatList =
      this.moveToInputOutputFormatList.bind(this);
    this.saveInputOutputFormat = this.saveInputOutputFormat.bind(this);
    this.reset = this.reset.bind(this);

    //Component state
    this.state = {
      formatID: 0,
      formatName: "",
      isActive: true,
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchInputOutputFormat();
  }
  //#endregion

  //#region Fetching selected Input Output Format details
  fetchInputOutputFormat() {
    const { state } = this.props.location; // Format ID passed from View Input Output Format Page

    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/InputOutputFormats");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Input Output Format...",
      loading: true,
    });

    InputOutputFormatService.getInputOutputFormat(state, helper.getUser())
      .then((response) => {
        this.setState({
          formatID: response.data.FormatID,
          formatName: response.data.Format,
          isActive: response.data.IsActive,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region  Validating the input data
  handleFormValidation() {
    const formatName = this.state.formatName.trim();
    let formErrors = {};
    let isValidForm = true;

    //Format Name
    if (!formatName) {
      isValidForm = false;
      formErrors["formatNameError"] = "Format Name is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Bind control value to state variable
  onChangeFormatName(e) {
    this.setState({
      formatName: e.target.value,
    });

    if (e.target.value !== "" || e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(e) {
    this.setState({
      isActive: e.target.checked,
    });
  }
  //#endregion

  //#region Redirect to Input Output Format List Page
  moveToInputOutputFormatList = (e) => {
    this.props.history.push("/Masters/InputOutputFormats");
  };
  //#endregion

  //#region Reset the page
  reset() {
    this.fetchInputOutputFormat();
    this.setState({ formErrors: {} });
  }
  //#endregion

  //#region Save Input Output Format
  saveInputOutputFormat = (e) => {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while saving Input-Output Format...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        FormatID: this.state.formatID,
        Format: this.state.formatName.trim(),
        IsActive: this.state.isActive,
        UserID: helper.getUser(),
      };

      //Service call
      InputOutputFormatService.updateInputOutputFormat(
        this.state.formatID,
        data
      )
        .then((response) => {
          toast.success("Input-Output Format Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/InputOutputFormats",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  render() {
    const { formatNameError } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Input / Output Format</span>
          </div>
          <h4>
            Edit Input / Output Format{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={this.moveToInputOutputFormatList}
                title="Back to List"
                tabIndex="1"
              ></i>
            </span>
          </h4>
          <div>
            <div className="row row-sm">
              <div className="col-md-2">
                <label>
                  <b>Format ID </b>{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
              </div>
              <div className="col-md-5 mg-t-7">
                <p> {this.state.formatID}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2">
                <label>
                  <b>Format Name</b>{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
              </div>
              <div className="col-md-5">
                <input
                  type="text"
                  className="form-control"
                  tabIndex="2"
                  id="FormatName"
                  name="FormatName"
                  maxLength="50"
                  value={this.state.formatName}
                  onChange={this.onChangeFormatName}
                />
                {formatNameError && (
                  <div className="error-message">{formatNameError}</div>
                )}
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-md-2">
                <label>
                  <b>Is Active?</b>
                </label>
              </div>
              <div className="col-md-5 mg-t-5">
                <input
                  type="checkbox"
                  value={this.state.isActive}
                  onChange={this.onChangeIsActive}
                  checked={this.state.isActive}
                  tabIndex="3"
                  id="IsActive"
                />
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  id="Save"
                  onClick={this.saveInputOutputFormat}
                >
                  Save
                </button>
              </div>
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="5"
                  id="Reset"
                  onClick={this.reset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default EditInputOutputFormat;
