import React, { Component } from "react";
import { Link } from "react-router-dom";
import InputOutputFormatService from "../../services/inputOutputFormat.service";
import accessControlService from "../../services/accessControl.service";
import helper from "../../helpers/helpers";
import { Button, Modal } from "react-bootstrap";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class ViewInputOutputFormat extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.handleYes = this.handleYes.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.showPopUp = this.showPopUp.bind(this);

    //Component State
    this.state = {
      InputOutputFormats: [
        {
          FormatID: 0,
          Format: "",
          IsActive: true,
        },
      ],
      showModal: false,
      canAccessEditInputOutputFormat: false,
      canAccessDeleteInputOutputFormat: false,
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Edit Input-Output Format");
    this.canUserAccessPage("Delete Input-Output Format");
    this.fetchInputOutputFormat();
  }
  //#endregion

  //#region Fetching selected Input Output Format details
  fetchInputOutputFormat() {
    const { state } = this.props.location; // Format ID passed from Input Output Format List Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/InputOutputFormats");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Input-Output Format...",
      loading: true,
    });

    InputOutputFormatService.getInputOutputFormat(state, helper.getUser())
      .then((response) => {
        this.setState({
          InputOutputFormats: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Customer page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Edit Input-Output Format") {
          this.setState({
            canAccessEditInputOutputFormat: response.data,
          });
        } else if (pageName === "Delete Input-Output Format") {
          this.setState({
            canAccessDeleteInputOutputFormat: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region modal functions
  //#region show popup
  showPopUp() {
    this.setState({ showModal: true });
  }
  //#endregion

  //#region handle Yes
  handleYes() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while deleting Input-Output Format...",
      loading: true,
    });

    InputOutputFormatService.deleteInputOutputFormat(
      this.state.InputOutputFormats.FormatID,
      helper.getUser()
    )
      .then(() => {
        this.setState({ showModal: false, loading: false });
        toast.success("Input Output Format Deleted Successfully");
        this.props.history.push({
          pathname: "/Masters/InputOutputFormats",
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
        this.handleNo();
      });
  }
  //#endregion

  //#region handle No
  handleNo() {
    this.setState({ showModal: false });
  }
  //#endregion
  //#endregion

  //#region UI
  render() {
    const { FormatID, Format, IsActive } = this.state.InputOutputFormats;

    const canAccessEditInputOutputFormat =
      this.state.canAccessEditInputOutputFormat;
    const canAccessDeleteInputOutputFormat =
      this.state.canAccessDeleteInputOutputFormat;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Input / Output Format</span>
          </div>
          <h4>
            View Input / Output Format{" "}
            <span className="icon-size">
              <Link to="/Masters/InputOutputFormats" title="Back to List">
                <i className="far fa-arrow-alt-circle-left"></i>
              </Link>
            </span>
          </h4>
          <br />
          <div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-3">
                    <b>Format ID</b>
                  </div>
                  <div className="col-md-8">
                    <p>{FormatID}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-3">
                    <b>Format Name</b>
                  </div>
                  <div className="col-md-8">
                    <p>{Format}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Is Active?</b>
                  </div>
                  <div className="col-md-8">
                    {IsActive === false && <p>No</p>}
                    {IsActive === true && <p>Yes</p>}
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-0.5"></div>
              {canAccessEditInputOutputFormat && (
                <div className="col-md-2">
                  <Link
                    to={{
                      pathname: "/Masters/EditInputOutputFormat",
                      state: FormatID, // passing Format ID to Edit Input-Output Format Page
                    }}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Edit
                  </Link>
                </div>
              )}
              <div className="col-md-0.5"></div>
              {canAccessDeleteInputOutputFormat && (
                <div className="col-md-2">
                  <button
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                    onClick={this.showPopUp}
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>
            <Modal
              show={this.state.showModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.handleNo}
              backdrop="static"
              enforceFocus={false}
            >
              <Modal.Header>
                <Modal.Title>Delete Input-Output Format</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <p>Are you sure to delete this Input-Output Format?</p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="danger" onClick={this.handleYes}>
                  Yes
                </Button>
                <Button variant="primary" onClick={this.handleNo}>
                  No
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default ViewInputOutputFormat;
