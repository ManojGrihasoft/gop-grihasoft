import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import userService from "../../services/user.service";
import accessControlService from "../../services/accessControl.service";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class UserList extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.divScrollRef = React.createRef();

    this.moveToCreateUser = this.moveToCreateUser.bind(this);
    this.exportUserListToExcel = this.exportUserListToExcel.bind(this);
    this.onChangeDepartment = this.onChangeDepartment.bind(this);
    this.onChangeManager = this.onChangeManager.bind(this);
    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.sortData = this.sortData.bind(this);
    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.clearSearchField = this.clearSearchField.bind(this);
    this.clearSortFields = this.clearSortFields.bind(this);

    ///Component State
    this.state = {
      users: [],
      departments: [],
      selectedDepartment: "(All)",
      managers: [],
      selectedManager: "(All)",
      canAccessCreateUser: false,
      canAccessViewUser: false,
      loading: false,
      index: 20,
      position: 0,
      columns: [],
      selectedColumn: "",
      selectedSort: "",
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      filteredArray: [],
      filterValue: "",
      spinnerMessage: "",
    };
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Create User");
    this.canUserAccessPage("View User");
  }
  //#endregion

  //#region fetching Users List from Web API
  fetchUsersList() {
    this.setState({
      spinnerMessage: "Please wait while loading User List...",
      loading: true,
    });

    userService
      .getAllUsers(helper.getUser())
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          FullName:
            obj.FirstName +
            " " +
            (obj.MiddleName ? obj.MiddleName + " " : "") +
            obj.LastName,
          IsLockedOut: obj.IsLockedOut === true ? "Yes" : "No",
        }));

        formattedArray = formattedArray.map((obj) => {
          delete obj.Password;
          delete obj.User;
          delete obj.FirstName;
          delete obj.MiddleName;
          delete obj.LastName;

          return obj;
        });

        let departments = [
          ...new Set(formattedArray.map((obj) => obj.DepartmentName)),
        ];

        departments = departments.filter(function (el) {
          return el !== "";
        });

        let managers = [
          ...new Set(formattedArray.map((obj) => obj.ManagerName)),
        ];

        managers = managers.filter(function (el) {
          return el !== "";
        });

        this.setState({
          users: formattedArray,
          departments: departments,
          managers: managers,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Fetching Item Status page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Create User") {
          this.setState({
            canAccessCreateUser: response.data,
          });
        } else if (pageName === "View User") {
          this.setState({
            canAccessViewUser: response.data,
          });
        }
        this.fetchUsersList();
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Get Selected Department
  onChangeDepartment(e) {
    this.setState(
      {
        selectedDepartment: e.target.value,
        selectedManager: "(All)",
        filterValue: e.target.value,
      },
      () => this.fetchFilteredData(true)
    );
  }
  //#endregion

  //#region Get Selected Department
  onChangeManager(e) {
    this.setState(
      {
        selectedManager: e.target.value,
        filterValue: e.target.value,
      },
      () => this.fetchFilteredData(false)
    );
  }
  //#endregion

  //#region Fetch Filtered Data
  fetchFilteredData(isToLoadManager) {
    let filteredArray = [];

    if (this.state.selectedDepartment !== "(All)") {
      filteredArray = this.state.users.filter(
        (dept) => dept.DepartmentName === this.state.selectedDepartment
      );
    } else {
      filteredArray = this.state.users;
    }

    if (this.state.selectedManager !== "(All)") {
      filteredArray = filteredArray.filter(
        (manager) => manager.ManagerName === this.state.selectedManager
      );
    }

    this.setState({ filteredArray: filteredArray }, () => {
      if (isToLoadManager) {
        let managers = [
          ...new Set(this.state.filteredArray.map((obj) => obj.ManagerName)),
        ];

        managers = managers.filter(function (el) {
          return el !== "";
        });

        this.setState({
          managers: managers,
        });
      }
    });
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  //#region Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.users[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
      selectedDepartment: "(All)",
      selectedManager: "(All)",
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "UserID"];

    sortedArray = tableFunctions.sortData(
      this.state.users,
      column,
      selectedSort,
      numberColumns,
      []
    );

    this.setState({ users: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSortFields() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
      selectedDepartment: "(All)",
      selectedManager: "(All)",
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.users,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearchField() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Redirect to Create User Page
  moveToCreateUser() {
    this.props.history.push("/admin/CreateUser");
  }
  //#endregion

  //#region Export User List to Excel
  exportUserListToExcel() {
    this.setState({
      spinnerMessage: "Please wait while exporting user list to excel...",
      loading: true,
    });

    let fileName = "User List.xlsx";

    userService
      .exportUserListToExcel()
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", fileName);
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region UI
  render() {
    const canAccessCreateUser = this.state.canAccessCreateUser;
    const canAccessViewUser = this.state.canAccessViewUser;

    const data = this.state.users.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);

    const UsersListColumns = [
      {
        dataField: "SlNo",
        text: "Sl No",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "90px",
        },
        headerAlign: "center",
        align: "center",
        style: {
          width: "90px",
        },
      },
      {
        dataField: "UserID",
        text: "User ID",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "90px",
        },
        headerAlign: "center",
        align: "center",
        style: {
          width: "90px",
        },
      },
      {
        dataField: "FullName",
        text: "Full Name",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "230px",
        },
        headerAlign: "center",
        align: "left",
        style: {
          width: "230px",
        },
      },
      {
        dataField: "UserName",
        text: "User Name",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
        style: {
          width: "120px",
        },
        classes: canAccessViewUser ? "demo-key-row1" : "",
        events: {
          onClick: (e, column, columnIndex, row, rowIndex) => {
            canAccessViewUser &&
              this.props.history.push({
                pathname: "/admin/ViewUser",
                state: row.UserID,
              });
          },
        },
      },
      {
        dataField: "Email",
        text: "Email ID",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
          width: "190px",
        },
        title: true,
      },
      {
        dataField: "DepartmentName",
        text: "Department",
        align: "Center",
        style: {
          width: "120px",
        },
      },
      {
        dataField: "ManagerName",
        text: "Manager",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
          width: "150px",
        },
        title: true,
      },
      {
        dataField: "IsLockedOut",
        text: "Is Locked Out?",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
        style: {
          width: "130px",
        },
      },
    ];

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Admin</span>
            <span>Users-List</span>
          </div>
          <h4>
            Users List{" "}
            {canAccessCreateUser && (
              <span className="icon-size">
                <i
                  className="fa fa-plus text-primary pointer"
                  onClick={this.moveToCreateUser}
                  title="Add New User"
                ></i>
              </span>
            )}
          </h4>
          <ToolkitProvider
            keyField="UserID"
            data={this.state.filterValue === "" ? data : filteredData}
            columns={UsersListColumns}
          >
            {(props) => (
              <div>
                <div className="row mg-b-10">
                  {!this.state.isToShowSortingFields &&
                    !this.state.isToShowFilteringField && (
                      <div className="col-md-8 row row-sm mg-b-5 mg-t-5">
                        <div className="col-lg">
                          <div className="row">
                            <div className="col-md-4 mg-t-5">
                              <b>Department</b>
                            </div>
                            <div className="col-md-7">
                              <select
                                className="form-control"
                                id="department"
                                name="department"
                                placeholder="--Select--"
                                value={this.state.selectedDepartment}
                                onChange={this.onChangeDepartment}
                              >
                                <option value="(All)">(All)</option>
                                {this.state.departments.map((department) => (
                                  <option key={department}>{department}</option>
                                ))}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg mg-t-10 mg-lg-t-0">
                          <div className="row">
                            <div className="col-md-4 mg-t-5">
                              <b>Manager</b>
                            </div>
                            <div className="col-md-7">
                              <select
                                className="form-control"
                                id="manager"
                                name="manager"
                                placeholder="--Select--"
                                value={this.state.selectedManager}
                                onChange={this.onChangeManager}
                              >
                                <option value="(All)">(All)</option>
                                {this.state.managers.map((manager) => (
                                  <option key={manager}>{manager}</option>
                                ))}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}

                  <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                    <div className="row">
                      {this.state.isToShowSortingFields && (
                        <>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortColumn">Column:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedColumn}
                                  onChange={this.onChangeColumn}
                                >
                                  <option value="">--Select--</option>
                                  {this.state.columns.map((col) => (
                                    <option key={col}>{col}</option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortOrder">Order:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedSort}
                                  onChange={this.onChangeSortOrder}
                                >
                                  <option value="">--Select--</option>
                                  <option value="ascending">Ascending</option>
                                  <option value="descending">Descending</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-2">
                            <div>
                              <span
                                className="btn btn-primary pd-b-5"
                                onClick={this.clearSortFields}
                                title="Clear Sort Fields"
                              >
                                <i className="far fa-window-close"></i>
                              </span>
                            </div>
                          </div>
                        </>
                      )}
                      {this.state.isToShowFilteringField && (
                        <>
                          <div className="col-md-12">
                            <div className="row" style={{ flexWrap: "nowrap" }}>
                              <div className="col-md-1 mg-t-7">
                                <label htmlFor="search">Search:</label>
                              </div>
                              <div className="col-lg pd-r-10">
                                <input
                                  type="text"
                                  className="form-control mg-l-5"
                                  maxLength="20"
                                  value={this.state.filterValue}
                                  onChange={this.onChangefilterValue}
                                />
                              </div>
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSearchField}
                                >
                                  <i
                                    className="far fa-window-close"
                                    title="Clear Filter"
                                  ></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                  <div
                    className="col-md-2"
                    style={{ textAlign: "end", marginTop: "10px" }}
                  >
                    <i
                      className="fas fa-exchange-alt fa-rotate-90 pointer"
                      title={
                        this.state.isToShowSortingFields
                          ? "Hide Sort"
                          : "Show Sort"
                      }
                      onClick={this.displaySortingFields}
                    ></i>
                    {!this.state.isToShowFilteringField ? (
                      <i
                        className="fas fa-filter pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Show Filter"
                      ></i>
                    ) : (
                      <i
                        className="fas fa-funnel-dollar pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Hide Filter"
                      ></i>
                    )}
                    <i
                      className="fas fa-file-excel mg-l-10 pointer"
                      style={{ color: "green" }}
                      onClick={this.exportUserListToExcel}
                      title="Export to Excel"
                    ></i>
                  </div>
                </div>
                <table
                  style={{
                    width:
                      (this.state.filteredArray.length < 10 &&
                        this.state.filterValue !== "") ||
                      this.state.users.length < 10
                        ? "100%"
                        : "98.9%",
                  }}
                >
                  <thead>
                    <tr>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "8.05%"
                              : "8.05%",
                        }}
                      >
                        Sl No
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "8.05%"
                              : "8.05%",
                        }}
                      >
                        User ID
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "20.5%"
                              : "20.5%",
                        }}
                      >
                        Full Name
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "10.7%"
                              : "10.7%",
                        }}
                      >
                        User Name
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "17%"
                              : "17%",
                        }}
                      >
                        Email ID
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "10.7%"
                              : "10.7%",
                        }}
                      >
                        Department
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "13.4%"
                              : "13.4%",
                        }}
                      >
                        Manager
                      </td>
                      <td
                        className="custom-table-header"
                        style={{
                          width:
                            this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== ""
                              ? "13.9%"
                              : "13.9%",
                        }}
                      >
                        Is Locked Out?
                      </td>
                    </tr>
                  </thead>
                </table>
                <div
                  style={
                    (this.state.filteredArray.length > 12 &&
                      this.state.filterValue !== "") ||
                    (this.state.users.length > 12 &&
                      this.state.filterValue === "")
                      ? {
                          overflowY: "scroll",
                          borderBottom: "1px solid #cdd4e0",
                        }
                      : {}
                  }
                  ref={this.divScrollRef}
                  className="admin-table-height"
                  onScroll={this.handleScroll}
                >
                  <BootstrapTable
                    bootstrap4
                    {...props.baseProps}
                    striped
                    hover
                    condensed
                    headerClasses="header-class"
                  />
                </div>
              </div>
            )}
          </ToolkitProvider>
          {this.state.position > 600 && this.state.filterValue === "" && (
            <div style={{ textAlign: "end" }}>
              <button className="scroll-top" onClick={this.scrollToTop}>
                <div className="arrow up"></div>
              </button>
            </div>
          )}
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default UserList;
