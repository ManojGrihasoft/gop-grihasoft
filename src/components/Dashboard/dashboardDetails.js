import React, { Component } from "react";
import "../../home.scss";
import { Link } from "react-router-dom";
import helpers from "../../helpers/helpers";
import loginService from "../../services/login.service";
import accessControlService from "../../services/accessControl.service";
import dashboardService from "../../services/dashboard.service";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { Pie } from "react-chartjs-2";
import { Chart } from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

Chart.register(ChartDataLabels);

class DashboardDetails extends Component {
  constructor(props) {
    super(props); //reference to the parent constructor

    this.displayDurationDetails = this.displayDurationDetails.bind(this);
    this.displayActiveProjects = this.displayActiveProjects.bind(this);
    this.displayActiveResources = this.displayActiveResources.bind(this);
    this.displayActiveTasks = this.displayActiveTasks.bind(this);

    this.state = {
      firstName: "",
      canAccessCustomerList: false,
      canAccessProjectList: false,
      NoOfCustomers: null,
      NoOfProjects: null,
      NoOfOnGoingProjects: null,
      NoOfCompletedProjects: null,
      NoOfPendingProjects: null,
      NoOfCompletedProjectsPercentage: null,
      NoOfPendingProjectsPercentage: null,
      NoOfBatches: null,
      NoOfCompletedBatches: null,
      NoOfPendingBatches: null,
      NoOfCompletedBatchesPercentage: null,
      NoOfPendingBatchesPercentage: null,
      NoOfActiveTasks: null,
      loading: false,
      noOfProjects: [],
      status: [],
      spinnerMessage: "",
      activeTab: 1,
      defaultActiveKey: "activeProjects",
      NoOfActiveResources: 0,
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helpers.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Customer List");
    this.canUserAccessPage("Project List");
    this.fetchUsername();
    this.fetchProjectsCompletionStatus();
  }
  //#endregion

  //#region Fetching selected User details
  fetchUsername() {
    this.setState({
      spinnerMessage: "Please wait while fetching logged in user details...",
      loading: true,
    });

    const user = helpers.getUser();
    //Service Call
    loginService
      .getUsername(user)
      .then((response) => {
        this.setState(
          {
            firstName: response.data.FirstName,
            loading: false,
          },
          () => this.fetchDashboardDetails()
        );
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Customer page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helpers.getUser(), pageName)
      .then((response) => {
        if (pageName === "Customer List") {
          this.setState({
            canAccessCustomerList: response.data,
          });
        } else if (pageName === "Project List") {
          this.setState({
            canAccessProjectList: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching dashboard details from Web API
  fetchDashboardDetails() {
    this.setState({
      spinnerMessage: "Please wait while fetching dashboard details...",
      loading: true,
    });

    dashboardService
      .ReadDashboardDetails()
      .then((response) => {
        this.setState({
          NoOfCustomers: response.data.NoOfCustomers,
          NoOfProjects: response.data.NoOfProjects,
          NoOfOnGoingProjects: response.data.NoOfOnGoingProjects,
          NoOfCompletedProjects: response.data.NoOfCompletedProjects,
          NoOfPendingProjects: response.data.NoOfPendingProjects,
          NoOfCompletedProjectsPercentage:
            response.data.NoOfCompletedProjectsPercentage,
          NoOfPendingProjectsPercentage:
            response.data.NoOfPendingProjectsPercentage,
          NoOfBatches: response.data.NoOfBatches,
          NoOfCompletedBatches: response.data.NoOfCompletedBatches,
          NoOfPendingBatches: response.data.NoOfPendingBatches,
          NoOfCompletedBatchesPercentage:
            response.data.NoOfCompletedBatchesPercentage,
          NoOfPendingBatchesPercentage:
            response.data.NoOfPendingBatchesPercentage,
          NoOfActiveTasks: response.data.NoOfActiveTasks,
          NoOfActiveResources: response.data.NoOfActiveResources,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetch Projects Completion Status
  fetchProjectsCompletionStatus() {
    dashboardService
      .ReadProjectsCompletionStatus()
      .then((response) => {
        let status = response.data.map((a) => a.Status);
        let noOfProjects = response.data.map((a) => a.NoOfProjects);
        this.setState({
          status: status,
          noOfProjects: noOfProjects,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Display Duration Details
  displayDurationDetails() {
    this.props.history.push("/Dashboard/Duration");
  }
  //#endregion

  //#region Display Active Resources
  displayActiveResources() {
    this.props.history.push("/Dashboard/ActiveResources");
  }
  //#endregion

  //#region Display Active Tasks
  displayActiveTasks() {
    this.props.history.push("/Dashboard/ActiveTasks");
  }
  //#endregion

  //#region Display Active Projects
  displayActiveProjects() {
    this.props.history.push("/Projects");
  }
  //#endregion

  render() {
    const firstName = this.state.firstName;
    const canAccessCustomerList = this.state.canAccessCustomerList;
    const canAccessProjectList = this.state.canAccessProjectList;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    const data = {
      labels: this.state.status,

      datasets: [
        {
          data: this.state.noOfProjects,
          backgroundColor: [
            "rgba(97, 222, 62, 0.5)",
            "rgba(235, 93, 54, 0.2)",
            "rgba(62, 193, 222, 0.2)",
          ],
          borderColor: [
            "rgba(97, 222, 62, 1)",
            "rgba(235, 93, 54, 1)",
            "rgba(62, 193, 222, 1)",
          ],
          borderWidth: 1,
          datalabels: {
            anchor: "center",
          },
        },
      ],
    };

    //#region Chart Options
    const chartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: "top",
        },
        title: {
          display: true,
          text: "Projects Status",
          font: {
            size: 12,
          },
        },
        datalabels: {
          display: true,
          color: "black",
        },
      },
    };
    //#endregion

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          styles={{
            content: {
              marginTop: "250px",
              marginLeft: "470px",
            },
          }}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content az-content-dashboard">
            <div className="container">
              <div className="az-content-body">
                <div>
                  <h2 className="az-dashboard-title">
                    Hi {firstName}, welcome back!
                  </h2>
                </div>
                <div className="row row-sm mg-b-20">
                  <div className="col-lg-7 ht-lg-100p">
                    <div className="card card-dashboard-one">
                      <div className="card-header">
                        <div>
                          <h6 className="card-title">Projects Status</h6>
                        </div>
                      </div>
                      <div className="card-body" style={{ height: "150px" }}>
                        <div className="card-body-top">
                          <div>
                            <label className="mg-b-0">Projects</label>
                            <h2 className="text-center">
                              {this.state.NoOfProjects}
                            </h2>
                          </div>
                          <div>
                            <label className="mg-b-0">Completed</label>
                            <h2>
                              {this.state.NoOfCompletedProjectsPercentage !==
                                null && (
                                <>
                                  {this.state.NoOfCompletedProjectsPercentage}%
                                </>
                              )}
                            </h2>
                          </div>
                          <div>
                            <label className="mg-b-0">Pending</label>
                            <h2>
                              {this.state.NoOfPendingProjectsPercentage !==
                                null && (
                                <>{this.state.NoOfPendingProjectsPercentage}%</>
                              )}
                            </h2>
                          </div>
                        </div>
                        <div className="card-body-top mg-t-80">
                          <div>
                            <label className="mg-b-0">Batches</label>
                            <h2 className="text-center">
                              {this.state.NoOfBatches}
                            </h2>
                          </div>
                          <div>
                            <label className="mg-b-0">Completed</label>
                            <h2>
                              {this.state.NoOfCompletedBatchesPercentage !==
                                null && (
                                <>
                                  {this.state.NoOfCompletedBatchesPercentage}%
                                </>
                              )}
                            </h2>
                          </div>
                          <div>
                            <label className="mg-b-0">Pending</label>
                            <h2>
                              {this.state.NoOfPendingBatchesPercentage !==
                                null && (
                                <>{this.state.NoOfPendingBatchesPercentage}%</>
                              )}
                            </h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-5 mg-t-20 mg-lg-t-0">
                    <div className="row row-sm">
                      <div className="col-sm-6">
                        <Link to="/Projects">
                          <div
                            className="card card-dashboard-two pointer"
                            style={{ height: "210px" }}
                          >
                            <div className="card-header">
                              <h4 style={{ color: "black" }}>
                                Projects{" "}
                                {canAccessProjectList && (
                                  <i
                                    className="fas fa-list"
                                    style={{ color: "blue" }}
                                  ></i>
                                )}
                              </h4>
                              <h5>
                                <b style={{ color: "black" }}>
                                  {this.state.NoOfOnGoingProjects !== null && (
                                    <>
                                      {this.state.NoOfOnGoingProjects} /{" "}
                                      {this.state.NoOfProjects}
                                    </>
                                  )}
                                </b>
                              </h5>
                            </div>
                          </div>
                        </Link>
                      </div>
                      <div className="col-sm-6 mg-t-20 mg-sm-t-0">
                        <Link to="/Masters/Customers">
                          <div
                            className="card card-dashboard-two pointer"
                            style={{ height: "210px" }}
                          >
                            <div className="card-header">
                              <h4 style={{ color: "black" }}>
                                Customers{" "}
                                {canAccessCustomerList && (
                                  <i
                                    className="fas fa-list"
                                    style={{ color: "blue" }}
                                  ></i>
                                )}
                              </h4>
                              <h5>
                                <b style={{ color: "black" }}>
                                  {this.state.NoOfCustomers}
                                </b>
                              </h5>
                            </div>
                          </div>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row row-sm mg-b-20">
                  <div className="col-lg-7 ht-lg-50p">
                    <div
                      className="card card-dashboard-one pd-t-20"
                      style={{ height: "60px" }}
                    >
                      <div className="card-body" style={{ height: "150px" }}>
                        <div className="card-body-top" style={{ top: -2 }}>
                          <div>
                            <label
                              className="demo-key-row1"
                              onClick={this.displayActiveProjects}
                            >
                              <b>
                                Active Projects({this.state.NoOfOnGoingProjects}
                                )
                              </b>
                            </label>
                          </div>
                          <div style={{ marginLeft: "50px" }}>
                            <label
                              className="demo-key-row1"
                              onClick={this.displayActiveTasks}
                            >
                              <b>Active Tasks ({this.state.NoOfActiveTasks})</b>
                            </label>
                          </div>
                          <div style={{ marginLeft: "50px" }}>
                            <label
                              className="demo-key-row1"
                              onClick={this.displayActiveResources}
                            >
                              <b>
                                Active Resources (
                                {this.state.NoOfActiveResources})
                              </b>
                            </label>
                          </div>
                          <div style={{ marginLeft: "50px" }}>
                            <label
                              className="demo-key-row1"
                              onClick={this.displayDurationDetails}
                            >
                              <b>Duration</b>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-5 mg-t-20 mg-lg-t-0">
                    <div
                      className="card card-dashboard-two"
                      style={{
                        padding: "2px",
                      }}
                    >
                      <div>
                        <Pie data={data} options={chartOptions} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default DashboardDetails;
