import React, { Component } from "react";
import { Link } from "react-router-dom";
import helpers from "../../helpers/helpers";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, {
  textFilter,
  numberFilter,
} from "react-bootstrap-table2-filter";
import Moment from "moment";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import ModernDatepicker from "react-modern-datepicker";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import dashboardService from "../../services/dashboard.service";
toast.configure();

let customerCodeFilter;
let projectCodeFilter;
let scopeFilter;
let projectTypeFilter;
let inputCountFilter;
let receivedDateFilter;
let plannedDeliveryDateFilter;
let statusFilter;

class ActiveProjects extends Component {
  constructor(props) {
    super(props); //reference to the parent constructor

    this.exportActiveProjectListToExcel =
      this.exportActiveProjectListToExcel.bind(this);
    this.clearAllFilter = this.clearAllFilter.bind(this);
    this.fetchActiveProjects = this.fetchActiveProjects.bind(this);

    this.state = {
      spinnerMessage: "",
      loading: false,
      displayFilter: true,
      formErrors: {},
      fromDate: null,
      toDate: null,
      activeProjects: [],
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helpers.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    var toDate = new Date();

    toDate.setDate(toDate.getDate());

    this.loadActiveProjectsOnIsToValidate(
      "01-01-1900",
      Moment(toDate).format("DD-MMM-yyyy"),
      false
    );
  }
  //#endregion

  //#region Validating the From Date and To Date
  handleDateValidation() {
    var fromDate = this.state.fromDate;
    var toDate = this.state.toDate;

    let formErrors = {};
    let isValidForm = true;

    //From Date
    if (!fromDate) {
      isValidForm = false;
      formErrors["fromDateError"] = "From Date is required.";
    }

    //To Date
    if (!toDate) {
      isValidForm = false;
      formErrors["toDateError"] = "To Date is required.";
    }

    if (fromDate) {
      if (toDate) {
        if (new Date(fromDate) > new Date(toDate)) {
          isValidForm = false;
          formErrors["fromDateError"] = "From Date can't be later than To Date";
        }
      }
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Load Active Project Based on Component Mount or View Projects button
  loadActiveProjectsOnIsToValidate(fromDate, toDate, isToValidate) {
    if (isToValidate) {
      console.log("hi");
      if (this.handleDateValidation()) {
        this.fetchActiveProjects(fromDate, toDate);
      }
    } else {
      this.fetchActiveProjects(fromDate, toDate);
    }
  }
  //#endregion

  //#region fetch Active Projects
  fetchActiveProjects(fromDate, toDate) {
    this.setState({
      spinnerMessage: "Please wait while fetching Active Projects...",
      loading: true,
    });

    dashboardService
      .readActiveProjects(fromDate, toDate)
      .then((response) => {
        if (response.data.length === 0) {
          this.setState({
            displayFilter: false,
            activeProjects: [],
            loading: false,
          });
          toast.error("No Data Found!!");
        } else {
          this.setState({
            activeProjects: response.data,
            loading: false,
            displayFilter: true,
          });
        }
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region  Get Selected From Date
  onChangeFromDate(date) {
    this.setState({
      fromDate: date,
      activeProjects: [],
      displayFilter: false,
    });

    if (date !== "" && date !== null) {
      const formErrors = { ...this.state.formErrors, fromDateError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region  Get Selected To Date
  onChangeToDate(date) {
    this.setState({
      toDate: date,
      activeProjects: [],
      displayFilter: false,
    });

    if (date !== "" && date !== null) {
      const formErrors = { ...this.state.formErrors, toDateError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Show / Hide Advanced Filter
  clearAllFilter() {
    if (this.state.displayFilter) {
      customerCodeFilter("");
      projectCodeFilter("");
      scopeFilter("");
      projectTypeFilter("");
      inputCountFilter("");
      receivedDateFilter("");
      plannedDeliveryDateFilter("");
      statusFilter("");
    }
    this.setState((previousState) => ({
      displayFilter: !previousState.displayFilter,
    }));
  }
  //#endregion

  //#region Export Project List to Excel
  exportActiveProjectListToExcel() {
    this.setState({
      spinnerMessage:
        "Please wait while exporting active project list to excel...",
      loading: true,
    });

    var fromDate = this.state.fromDate;
    var toDate = this.state.toDate;

    let fileName = "Active Projects.xlsx";

    if (!fromDate && !toDate) {
      fromDate = "01-01-1900";

      toDate = new Date();

      toDate.setDate(toDate.getDate());
      toDate = Moment(toDate).format("DD-MMM-yyyy");
    }

    dashboardService
      .exportActiveProjectsToExcel(fromDate, toDate)
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", fileName);
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  render() {
    const customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total mg-l-10">
        Showing {from} to {to} of {size} Results
      </span>
    );

    const sizePerPageRenderer = ({
      options,
      currSizePerPage,
      onSizePerPageChange,
    }) => (
      <div className="btn-group" role="group">
        {options.map((option) => {
          const isSelect = currSizePerPage === `${option.page}`;
          return (
            <button
              key={option.text}
              type="button"
              onClick={() => onSizePerPageChange(option.page)}
              className={`btn ${isSelect ? "btn-primary" : "btn-light"}`}
            >
              {option.text}
            </button>
          );
        })}
      </div>
    );

    const options = {
      sizePerPageRenderer,
      paginationSize: 4,
      firstPageText: "First",
      prePageText: "Back",
      nextPageText: "Next",
      lastPageText: "Last",
      nextPageTitle: "First page",
      prePageTitle: "Pre page",
      firstPageTitle: "Next page",
      lastPageTitle: "Last page",
      showTotal: true,
      paginationTotalRenderer: customTotal,
      disablePageTitle: true,
      sizePerPageList: [
        {
          text: "10",
          value: 10,
        },
        {
          text: "50",
          value: 50,
        },
        {
          text: "All",
          value: this.state.activeProjects.length,
        },
      ],
    };

    const projectListColumns = [
      {
        dataField: "ProjectID",
        text: "Project ID",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        hidden: true,
      },
      {
        dataField: "CustomerCode",
        text: "Cus Code",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "90px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                customerCodeFilter = filter;
              },
            })
          : null,
      },
      {
        dataField: "ProjectCode",
        text: "Proj Code",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "90px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                projectCodeFilter = filter;
              },
            })
          : null,
      },
      {
        dataField: "Scope",
        text: "Scope",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
        },
        title: true,
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "200px",
        },
        headerAlign: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                scopeFilter = filter;
              },
            })
          : null,
      },
      {
        dataField: "ProjectType",
        text: "Project Type",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "120px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                projectTypeFilter = filter;
              },
            })
          : null,
      },
      {
        dataField: "InputCount",
        text: "Input Count",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "100px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? numberFilter({
              getFilter: (filter) => {
                inputCountFilter = filter;
              },
            })
          : null,
      },
      {
        dataField: "ReceivedDate",
        text: "Received Date",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "120px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                receivedDateFilter = filter;
              },
            })
          : null,
        formatter: (cell, row, rowIndex, extraData) =>
          `${Moment(row.ReceivedDate).format("DD-MMM-yyyy")}`,
        filterValue: (cell, row, rowIndex, extraData) =>
          `${Moment(row.ReceivedDate).format("DD-MMM-yyyy")}`,
      },
      {
        dataField: "PlannedDeliveryDate",
        text: "Planned Del Date",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "130px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                plannedDeliveryDateFilter = filter;
              },
            })
          : null,
        filterValue: (cell, row, rowIndex, extraData) =>
          `${Moment(row.PlannedDeliveryDate).format("DD-MMM-yyyy")}`,
        formatter: (cell, row, rowIndex, extraData) =>
          `${
            row.PlannedDeliveryDate
              ? Moment(row.PlannedDeliveryDate).format("DD-MMM-yyyy")
              : ""
          }`,
      },
      {
        dataField: "Status",
        text: "Status",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "90px",
        },
        headerAlign: "center",
        align: "center",
        sort: true,
        filter: this.state.displayFilter
          ? textFilter({
              getFilter: (filter) => {
                statusFilter = filter;
              },
            })
          : null,
      },
    ];

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-body pd-lg-l-40 d-flex flex-column mg-r-20 mg-t-20">
            <ToolkitProvider
              keyField="ProjectID"
              data={this.state.activeProjects}
              columns={projectListColumns}
            >
              {(props) => (
                <div>
                  <div style={{ marginBottom: "5px" }}>
                    <div className="row">
                      <h5 style={{ marginLeft: "15px", marginTop: "7px" }}>
                        <b>Active Projects List</b>
                      </h5>
                      <div className="col-md-8">
                        <div className="row row-sm">
                          <span className="icon-size">
                            <Link
                              to={{
                                pathname: "/Dashboard",
                              }}
                            >
                              <i
                                className="far fa-arrow-alt-circle-left"
                                title="Back to Dashboard"
                              ></i>
                            </Link>
                          </span>
                          <div className="col-lg mg-t-10 mg-lg-t-0"></div>
                        </div>
                      </div>
                      <div
                        className="col-md-2"
                        style={{ textAlign: "end", marginTop: "10px" }}
                      >
                        <i
                          className="fas fa-filter pointer"
                          onClick={this.clearAllFilter}
                          title="Advanced Filter"
                        ></i>
                        {this.state.activeProjects.length > 0 && (
                          <i
                            className="fas fa-file-excel mg-l-5 pointer"
                            style={{ color: "green" }}
                            onClick={this.exportActiveProjectListToExcel}
                            title="Export to Excel"
                          ></i>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row" style={{ marginLeft: "230px" }}>
                    <div className="col-md-4">
                      <div className="row row-sm">
                        <div className="col-md-4 mg-t-10">
                          <b>From Date</b>
                        </div>
                        <div className="col-md-7">
                          <div className="form-control">
                            <ModernDatepicker
                              date={this.state.fromDate}
                              format={"DD-MMM-YYYY"}
                              onChange={(date) => this.onChangeFromDate(date)}
                              placeholder={"Select a date"}
                              className="color"
                              minDate={new Date(1900, 1, 1)}
                            />
                          </div>
                          <div className="error-message">
                            {this.state.formErrors["fromDateError"]}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 mg-t-10 mg-lg-t-0">
                      <div className="row row-sm">
                        <div className="col-md-4 mg-t-10">
                          <b>To Date</b>
                        </div>
                        <div className="col-md-7">
                          <div className="form-control">
                            <ModernDatepicker
                              date={this.state.toDate}
                              format={"DD-MMM-YYYY"}
                              onChange={(date) => this.onChangeToDate(date)}
                              placeholder={"Select a date"}
                              className="color"
                              minDate={new Date(1900, 1, 1)}
                            />
                          </div>
                          <div className="error-message">
                            {this.state.formErrors["toDateError"]}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-2 mg-t-10 mg-lg-t-0">
                      <button
                        onClick={() =>
                          this.loadActiveProjectsOnIsToValidate(
                            this.state.fromDate,
                            this.state.toDate,
                            true
                          )
                        }
                        className="btn btn-gray-700 btn-block"
                      >
                        View Projects
                      </button>
                    </div>
                  </div>
                  <BootstrapTable
                    {...props.baseProps}
                    pagination={paginationFactory(options)}
                    filter={filterFactory()}
                    striped
                    hover
                    condensed
                  />
                </div>
              )}
            </ToolkitProvider>
          </div>
        </LoadingOverlay>
      </>
    );
  }
}

export default ActiveProjects;
