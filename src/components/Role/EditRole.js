import React, { Component } from "react";
import roleService from "../../services/role.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class EditRole extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.onChangeRoleName = this.onChangeRoleName.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.reset = this.reset.bind(this);

    //Component State
    this.state = {
      roleID: 0,
      roleName: "",
      description: "",
      isActive: true,
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchRole();
  }
  //#endregion

  //#region Fetching selected Role details
  fetchRole() {
    const { state } = this.props.location; // Role ID passed from View Role Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/admin/Roles");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Role Details...",
      loading: true,
    });

    roleService
      .getRole(state, helper.getUser())
      .then((response) => {
        this.setState({
          roleID: response.data.RoleID,
          roleName: response.data.RoleName,
          description: response.data.Description,
          isActive: response.data.IsActive,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Get Role Name Value
  onChangeRoleName(e) {
    this.setState({
      roleName: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region Get Description Value
  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(e) {
    this.setState({
      isActive: e.target.checked,
    });
  }
  //#endregion

  //#region Validating the input data
  handleFormValidation() {
    const roleName = this.state.roleName.trim();
    let formErrors = {};
    let isValidForm = true;

    //Role Name
    if (!roleName) {
      isValidForm = false;
      formErrors["roleError"] = "Role Name is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Save Role
  saveRole = () => {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while saving Role...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        RoleID: this.state.roleID,
        RoleName: this.state.roleName.trim(),
        Description: this.state.description.trim(),
        IsActive: this.state.isActive,
        UserID: helper.getUser(),
      };

      //Service call
      roleService
        .updateRole(this.state.roleID, data)
        .then(() => {
          toast.success("Role Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/admin/Roles",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  //#region Reset the page
  reset() {
    this.fetchRole();
    this.setState({ formErrors: {} });
  }
  //#endregion

  //#region UI
  render() {
    const { roleError } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Admin</span>
            <span>Roles</span>
          </div>
          <h4>
            Edit Role{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={() => this.props.history.goBack()}
                title="Back to List"
              ></i>
            </span>
          </h4>
          <div id="Edit_Role">
            <div className="row row-sm">
              <div className="col-lg">
                <label htmlFor="roleName">
                  Role Name <span className="text-danger asterisk-size">*</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  maxLength="50"
                  id="roleName"
                  name="roleName"
                  tabIndex="1"
                  value={this.state.roleName}
                  onChange={this.onChangeRoleName}
                />
                {roleError && <div className="error-message">{roleError}</div>}
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-lg">
                <label htmlFor="description">Description</label>
                <textarea
                  className="form-control"
                  rows="2"
                  tabIndex="2"
                  id="description"
                  name="description"
                  maxLength="500"
                  value={this.state.description}
                  onChange={this.onChangeDescription}
                ></textarea>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-lg">
                <label htmlFor="IsActive">Is Active?</label>
                <input
                  type="checkbox"
                  className="mg-l-20"
                  tabIndex="3"
                  value={this.state.isActive}
                  onChange={this.onChangeIsActive}
                  checked={this.state.isActive}
                  id="IsActive"
                  name="IsActive"
                />
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2 mg-t-10 mg-lg-t-0">
                <button
                  id="Save"
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  onClick={this.saveRole}
                >
                  Save
                </button>
              </div>
              <div className="col-md-0.5"></div>
              <div className="col-md-2  mg-t-10 mg-lg-t-0">
                <button
                  className="btn btn-gray-700 btn-block"
                  tabIndex="5"
                  onClick={this.reset}
                  id="Reset"
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default EditRole;
