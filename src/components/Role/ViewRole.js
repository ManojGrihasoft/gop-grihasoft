import React, { Component } from "react";
import { Link } from "react-router-dom";
import roleService from "../../services/role.service";
import accessControlService from "../../services/accessControl.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { Button, Modal } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class ViewRole extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.handleYes = this.handleYes.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.showPopUp = this.showPopUp.bind(this);

    ///Component State
    this.state = {
      Role: [
        {
          RoleID: null,
          RoleName: null,
          Description: null,
          IsActive: null,
        },
      ],
      showModal: false,
      canAccessEditRole: false,
      canAccessDeleteRole: false,
      loading: false,
      spinnerMessage: "",
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Edit Role");
    this.canUserAccessPage("Delete Role");
    this.fetchRole();
  }
  //#endregion

  //#region Fetching selected Role details
  fetchRole() {
    const { state } = this.props.location; // Role ID passed from Roles List Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/admin/Roles");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Role Details...",
      loading: true,
    });

    roleService
      .getRole(state, helper.getUser())
      .then((response) => {
        this.setState({
          Role: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Role page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Edit Role") {
          this.setState({
            canAccessEditRole: response.data,
          });
        } else if (pageName === "Delete Role") {
          this.setState({
            canAccessDeleteRole: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region modal functions
  //#region show popup
  showPopUp() {
    this.setState({ showModal: true });
  }
  //#endregion

  //#region handle Yes
  handleYes() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while deleting Role...",
      loading: true,
    });

    roleService
      .deleteRole(this.state.Role.RoleID, helper.getUser())
      .then(() => {
        this.setState({ showModal: false });
        toast.success("Role Deleted Successfully");
        this.props.history.push({
          pathname: "/admin/Roles",
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
        this.handleNo();
      });
  }
  //#endregion

  //#region hanle No
  handleNo() {
    this.setState({ showModal: false });
  }
  //#endregion
  //#endregion

  //#region UI
  render() {
    const { RoleID, RoleName, Description, IsActive } = this.state.Role;

    const canAccessEditRole = this.state.canAccessEditRole;
    const canAccessDeleteRole = this.state.canAccessDeleteRole;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Admin</span>
            <span>Roles</span>
          </div>
          <h4>
            View Role{" "}
            <span className="icon-size">
              <Link to="/admin/Roles" title="Back to List">
                <i className="far fa-arrow-alt-circle-left"></i>
              </Link>
            </span>
          </h4>
          <br />
          <div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-3">
                    <b>Roles ID</b>
                  </div>
                  <div className="col-md-8">
                    <p>{RoleID}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-3">
                    <b>Role Name</b>
                  </div>
                  <div className="col-md-8">
                    <p>{RoleName}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Description</b>
                  </div>
                  <div className="col-md-8">{Description}</div>
                </div>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Is Active?</b>
                  </div>
                  <div className="col-md-8">
                    {IsActive === true ? "Yes" : "No"}
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-0.5"></div>
              {canAccessEditRole && (
                <div className="col-md-2">
                  <Link
                    to={{
                      pathname: "/admin/EditRole",
                      state: RoleID, // passing Role ID to Edit Role Page
                    }}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Edit
                  </Link>
                </div>
              )}
              <div className="col-md-0.5"></div>
              {canAccessDeleteRole && (
                <div className="col-md-2">
                  <button
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                    onClick={this.showPopUp}
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>
            <Modal
              show={this.state.showModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.handleNo}
              backdrop="static"
              enforceFocus={false}
            >
              <Modal.Header>
                <Modal.Title>Delete Role</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <p>Are you sure to delete this Role?</p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="danger" onClick={this.handleYes}>
                  Yes
                </Button>
                <Button variant="primary" onClick={this.handleNo}>
                  No
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default ViewRole;
