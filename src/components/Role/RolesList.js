import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import roleService from "../../services/role.service";
import accessControlService from "../../services/accessControl.service";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class RolesList extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.divScrollRef = React.createRef();

    this.moveToCreateRole = this.moveToCreateRole.bind(this);

    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.sortData = this.sortData.bind(this);
    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.exportRolesListToExcel = this.exportRolesListToExcel.bind(this);

    ///Component State
    this.state = {
      roles: [],
      canAccessCreateRole: false,
      canAccessViewRole: false,
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
    };
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Create Role");
    this.canUserAccessPage("View Role");
  }
  //#endregion

  //#region fetching Roles List from Web API
  fetchRolesList() {
    this.setState({
      spinnerMessage: "Please wait while loading Role List...",
      loading: true,
    });

    roleService
      .getAllRoles(helper.getUser(), false)
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          IsActive: obj.IsActive === true ? "Yes" : "No",
        }));

        formattedArray = formattedArray.map((obj) => {
          delete obj.UserID;
          return obj;
        });

        this.setState({ roles: formattedArray, loading: false });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Roles List page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Create Role") {
          this.setState({
            canAccessCreateRole: response.data,
          });
        } else if (pageName === "View Role") {
          this.setState({
            canAccessViewRole: response.data,
          });
        }
        this.fetchRolesList();
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Redirect to Create Role Page
  moveToCreateRole() {
    this.props.history.push("/admin/CreateRole");
  }
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.roles[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "RoleID"];

    sortedArray = tableFunctions.sortData(
      this.state.roles,
      column,
      selectedSort,
      numberColumns,
      []
    );

    this.setState({ roles: sortedArray });
  }
  //#endregion
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.roles,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region Export Role List to Excel
  exportRolesListToExcel() {
    this.setState({
      spinnerMessage: "Please wait while exporting role list to excel...",
      loading: true,
    });

    let fileName = "Role List.xlsx";

    roleService
      .exportRoleListToExcel()
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", fileName);
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  //#region UI
  render() {
    const data = this.state.roles.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);

    const canAccessCreateRole = this.state.canAccessCreateRole;
    const canAccessViewRole = this.state.canAccessViewRole;

    const RolesListColumns = [
      {
        dataField: "SlNo",
        text: "Sl No.",
        headerStyle: {
          backgroundColor: "#f2f8fb",
          width: "80px",
        },
        headerAlign: "center",
        align: "center",
      },
      {
        dataField: "RoleID",
        text: "Role ID",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
      },
      {
        dataField: "RoleName",
        text: "Role Name",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "left",
        classes: canAccessViewRole ? "demo-key-row1" : "",
        events: {
          onClick: (e, column, columnIndex, row, rowIndex) => {
            canAccessViewRole &&
              this.props.history.push({
                pathname: "/admin/ViewRole",
                state: row.RoleID,
              });
          },
        },
      },
      {
        dataField: "Description",
        text: "Description",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "left",
      },
      {
        dataField: "IsActive",
        text: "Is Active?",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
      },
    ];

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Admin</span>
            <span>Roles-List</span>
          </div>
          <h4>
            Roles List{" "}
            {canAccessCreateRole && (
              <span className="icon-size">
                <i
                  className="fa fa-plus text-primary pointer"
                  onClick={this.moveToCreateRole}
                  title="Add New Role"
                ></i>
              </span>
            )}
          </h4>
          <ToolkitProvider
            keyField="RoleID"
            data={this.state.filterValue === "" ? data : filteredData}
            columns={RolesListColumns}
          >
            {(props) => (
              <div className="mg-t-10">
                <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                  <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                    <div className="row">
                      {this.state.isToShowSortingFields && (
                        <>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortColumn">Column:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedColumn}
                                  onChange={this.onChangeColumn}
                                >
                                  <option value="">--Select--</option>
                                  {this.state.columns.map((col) => (
                                    <option key={col}>{col}</option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortOrder">Order:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedSort}
                                  onChange={this.onChangeSortOrder}
                                >
                                  <option value="">--Select--</option>
                                  <option value="ascending">Ascending</option>
                                  <option value="descending">Descending</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-2">
                            <div>
                              <span
                                className="btn btn-primary pd-b-5"
                                onClick={this.clearSort}
                                title="Clear Sort Fields"
                              >
                                <i className="far fa-window-close"></i>
                              </span>
                            </div>
                          </div>
                        </>
                      )}
                      {this.state.isToShowFilteringField && (
                        <>
                          <div className="col-md-12">
                            <div className="row" style={{ flexWrap: "nowrap" }}>
                              <div className="col-md-1 mg-t-7">
                                <label htmlFor="search">Search:</label>
                              </div>
                              <div className="col-lg pd-r-10">
                                <input
                                  type="text"
                                  className="form-control mg-l-10"
                                  maxLength="20"
                                  value={this.state.filterValue}
                                  onChange={this.onChangefilterValue}
                                />
                              </div>
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSearch}
                                >
                                  <i
                                    className="far fa-window-close"
                                    title="Clear Filter"
                                  ></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                  <div
                    className="col-md-2"
                    style={{ textAlign: "end", marginTop: "10px" }}
                  >
                    <i
                      className="fas fa-exchange-alt fa-rotate-90 pointer"
                      title={
                        this.state.isToShowSortingFields
                          ? "Hide Sort"
                          : "Show Sort"
                      }
                      onClick={this.displaySortingFields}
                    ></i>
                    {!this.state.isToShowFilteringField ? (
                      <i
                        className="fas fa-filter pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Show Filter"
                      ></i>
                    ) : (
                      <i
                        className="fas fa-funnel-dollar pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Hide Filter"
                      ></i>
                    )}
                    <i
                      className="fas fa-file-excel mg-l-10 pointer"
                      style={{ color: "green" }}
                      onClick={this.exportRolesListToExcel}
                      title="Export to Excel"
                    ></i>
                  </div>
                </div>
                <div>
                  <table
                    style={{
                      width:
                        (this.state.filteredArray.length < 12 &&
                          this.state.filterValue !== "") ||
                        this.state.roles.length < 12
                          ? "100%"
                          : "98.8%",
                    }}
                  >
                    <thead>
                      <tr>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.roles.length < 12
                                ? "19.4%"
                                : "19.6%",
                          }}
                        >
                          Sl No
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.roles.length < 12
                                ? "19.4%"
                                : "19.6%",
                          }}
                        >
                          Role ID
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.roles.length < 12
                                ? "19.4%"
                                : "19.6%",
                          }}
                        >
                          Role Name
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.roles.length < 12
                                ? "19.4%"
                                : "19.6%",
                          }}
                        >
                          Description
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.roles.length < 12
                                ? "19.4%"
                                : "19.7%",
                          }}
                        >
                          Is Active?
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div
                  style={
                    (this.state.filteredArray.length > 12 &&
                      this.state.filterValue !== "") ||
                    (this.state.roles.length > 12 &&
                      this.state.filterValue === "")
                      ? {
                          height: "380px",
                          overflowY: "scroll",
                          borderBottom: "1px solid #cdd4e0",
                        }
                      : {}
                  }
                  ref={this.divScrollRef}
                  className="scrollable-element"
                  onScroll={this.handleScroll}
                >
                  <BootstrapTable
                    bootstrap4
                    {...props.baseProps}
                    striped
                    hover
                    condensed
                    headerClasses="header-class"
                  />
                  <div className="col-md-10">
                    {((this.state.index <= this.state.roles.length &&
                      this.state.filterValue === "") ||
                      this.state.index <= this.state.filteredArray.length) && (
                      <p>loading more rows, please scroll...</p>
                    )}
                  </div>
                </div>
              </div>
            )}
          </ToolkitProvider>
          {this.state.position > 600 && (
            <div style={{ textAlign: "end" }}>
              <button className="scroll-top" onClick={this.scrollToTop}>
                <div className="arrow up"></div>
              </button>
            </div>
          )}
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default RolesList;
