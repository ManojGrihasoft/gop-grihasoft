import React, { Component } from "react";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import customerService from "../../services/customer.service";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import accessControlService from "../../services/accessControl.service";
import { Link } from "react-router-dom";
toast.configure();

class CustomerList extends Component {
  constructor(props) {
    super(props);

    this.divScrollRef = React.createRef();

    this.moveToAddCustomer = this.moveToAddCustomer.bind(this);

    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.sortData = this.sortData.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.exportCustomerListToExcel = this.exportCustomerListToExcel.bind(this);

    this.state = {
      customers: [],
      canAccessCreateCustomer: false,
      canAccessViewCustomer: false,
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
    };
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Create Customer");
    this.canUserAccessPage("View Customer");
  }
  //#endregion

  //#region fetching customers from Web API
  fetchCustomers() {
    this.setState({
      spinnerMessage: "Please wait while loading Customer List...",
      loading: true,
    });

    customerService
      .getAllCustomers(helper.getUser())
      .then((response) => {
        let formattedArray = response.data;

        formattedArray = formattedArray.map((obj) => {
          delete obj.UserID;
          return obj;
        });

        this.setState({ customers: formattedArray, loading: false });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Customer page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Create Customer") {
          this.setState({
            canAccessCreateCustomer: response.data,
          });
        } else if (pageName === "View Customer") {
          this.setState({
            canAccessViewCustomer: response.data,
          });
        }
        this.fetchCustomers();
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region  Redirect to Add Customer Page
  moveToAddCustomer() {
    this.props.history.push("/Masters/AddCustomer");
  }
  //#endregion

  //#region Export Customers List to Excel
  exportCustomerListToExcel() {
    this.setState({
      spinnerMessage: "Please wait while exporting Customer List to excel...",
      loading: true,
    });

    let fileName = "Customers List.xlsx";

    customerService
      .exportCustomersListToExcel()
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", fileName);
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.customers[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "CustomerID"];

    sortedArray = tableFunctions.sortData(
      this.state.customers,
      column,
      selectedSort,
      numberColumns,
      []
    );

    this.setState({ customers: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.customers,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion

  //#endregion

  //#region UI
  render() {
    const data = this.state.customers.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);

    const canAccessCreateCustomer = this.state.canAccessCreateCustomer;
    const canAccessViewCustomer = this.state.canAccessViewCustomer;

    const customerColumns = [
      {
        dataField: "SlNo",
        text: "Sl No.",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
      },
      {
        dataField: "CustomerID",
        text: "Customer ID",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
      },
      {
        dataField: "CustomerCode",
        text: "Customer Code",
        headerStyle: {
          backgroundColor: "#f2f8fb",
        },
        headerAlign: "center",
        align: "center",
        formatter: (cell, row, rowIndex, extraData) => (
          <div>
            {canAccessViewCustomer ? (
              <Link
                to={{
                  pathname: "/Masters/ViewCustomer",
                  state: row.CustomerID, // passing Customer ID to View Customer Page
                }}
              >
                {row.CustomerCode}
              </Link>
            ) : (
              <div>{row.CustomerCode}</div>
            )}
          </div>
        ),
      },
    ];

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Customers</span>
          </div>
          <h4>
            Customers List{" "}
            {canAccessCreateCustomer && (
              <span className="icon-size">
                <i
                  className="fa fa-plus text-primary pointer"
                  onClick={this.moveToAddCustomer}
                  title="Add New Customer"
                ></i>
              </span>
            )}
          </h4>
          <ToolkitProvider
            keyField="CustomerID"
            data={this.state.filterValue === "" ? data : filteredData}
            columns={customerColumns}
          >
            {(props) => (
              <div className="mg-t-10">
                <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                  <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                    <div className="row">
                      {this.state.isToShowSortingFields && (
                        <>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortColumn">Column:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedColumn}
                                  onChange={this.onChangeColumn}
                                >
                                  <option value="">--Select--</option>
                                  {this.state.columns.map((col) => (
                                    <option key={col}>{col}</option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-4">
                            <div className="row">
                              <div className="col-md-3 mg-t-7">
                                <label htmlFor="sortOrder">Order:</label>
                              </div>
                              <div className="col-lg">
                                <select
                                  className="form-control mg-l-5"
                                  value={this.state.selectedSort}
                                  onChange={this.onChangeSortOrder}
                                >
                                  <option value="">--Select--</option>
                                  <option value="ascending">Ascending</option>
                                  <option value="descending">Descending</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-2">
                            <div>
                              <span
                                className="btn btn-primary pd-b-5"
                                onClick={this.clearSort}
                                title="Clear Sort Fields"
                              >
                                <i className="far fa-window-close"></i>
                              </span>
                            </div>
                          </div>
                        </>
                      )}
                      {this.state.isToShowFilteringField && (
                        <>
                          <div className="col-md-12">
                            <div className="row" style={{ flexWrap: "nowrap" }}>
                              <div className="col-md-1 mg-t-7">
                                <label htmlFor="search">Search:</label>
                              </div>
                              <div className="col-lg pd-r-10">
                                <input
                                  type="text"
                                  className="form-control mg-l-10"
                                  maxLength="20"
                                  value={this.state.filterValue}
                                  onChange={this.onChangefilterValue}
                                />
                              </div>
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSearch}
                                >
                                  <i
                                    className="far fa-window-close"
                                    title="Clear Filter"
                                  ></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                  <div
                    className="col-md-2"
                    style={{ textAlign: "end", marginTop: "10px" }}
                  >
                    <i
                      className="fas fa-exchange-alt fa-rotate-90 pointer"
                      title={
                        this.state.isToShowSortingFields
                          ? "Hide Sort"
                          : "Show Sort"
                      }
                      onClick={this.displaySortingFields}
                    ></i>
                    {!this.state.isToShowFilteringField ? (
                      <i
                        className="fas fa-filter pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Show Filter"
                      ></i>
                    ) : (
                      <i
                        className="fas fa-funnel-dollar pointer mg-l-10"
                        onClick={this.displayFilteringField}
                        title="Hide Filter"
                      ></i>
                    )}
                    <i
                      className="fas fa-file-excel mg-l-10 pointer"
                      style={{ color: "green" }}
                      onClick={this.exportCustomerListToExcel}
                      title="Export to Excel"
                    ></i>
                  </div>
                </div>
                <div>
                  <table
                    style={{
                      width:
                        (this.state.filteredArray.length < 12 &&
                          this.state.filterValue !== "") ||
                        this.state.customers.length < 12
                          ? "100%"
                          : "98.8%",
                    }}
                  >
                    <thead>
                      <tr>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.customers.length < 12
                                ? "19.4%"
                                : "19.4%",
                          }}
                        >
                          Sl No
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.customers.length < 12
                                ? "19.4%"
                                : "19.3%",
                          }}
                        >
                          Customer ID
                        </td>
                        <td
                          className="custom-table-header"
                          style={{
                            width:
                              (this.state.filteredArray.length < 12 &&
                                this.state.filterValue !== "") ||
                              this.state.customers.length < 12
                                ? "19.4%"
                                : "19.5%",
                          }}
                        >
                          Customer Code
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div
                  style={
                    (this.state.filteredArray.length > 12 &&
                      this.state.filterValue !== "") ||
                    (this.state.customers.length > 12 &&
                      this.state.filterValue === "")
                      ? {
                          height: "375px",
                          overflowY: "scroll",
                          borderBottom: "1px solid #cdd4e0",
                        }
                      : {}
                  }
                  ref={this.divScrollRef}
                  className="scrollable-element"
                  onScroll={this.handleScroll}
                >
                  <BootstrapTable
                    bootstrap4
                    {...props.baseProps}
                    striped
                    hover
                    condensed
                    headerClasses="header-class"
                  />
                  <div className="col-md-10">
                    {((this.state.index <= this.state.customers.length &&
                      this.state.filterValue === "") ||
                      this.state.index <= this.state.filteredArray.length) && (
                      <p>loading more rows, please scroll...</p>
                    )}
                  </div>
                </div>
              </div>
            )}
          </ToolkitProvider>
          {this.state.position > 600 && (
            <div style={{ textAlign: "end" }}>
              <button className="scroll-top" onClick={this.scrollToTop}>
                <div className="arrow up"></div>
              </button>
            </div>
          )}
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default CustomerList;
