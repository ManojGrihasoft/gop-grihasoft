import React, { Component } from "react";
import customerService from "../../services/customer.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class AddCustomer extends Component {
  constructor(props) {
    super(props);
    this.onChangeCustomer = this.onChangeCustomer.bind(this);
    this.reset = this.reset.bind(this);
    this.state = {
      CustomerID: 0,
      CustomerCode: "",
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }
  }
  //#endregion

  //#region  Validating the input data
  handleFormValidation() {
    const custCode = this.state.CustomerCode;
    let formErrors = {};
    let isValidForm = true;

    //Customer code
    if (!custCode) {
      isValidForm = false;
      formErrors["custCodeErr"] = "Customer Code is required.";
    } else if (custCode.length !== 3) {
      isValidForm = false;
      formErrors["custCodeErr"] = "Customer Code must be minimum 3 letters.";
    }
    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Bind control value to state variable
  onChangeCustomer(e) {
    const re = /^[A-Za-z]+$/;
    if (e.target.value === "" || re.test(e.target.value))
      this.setState({
        CustomerCode: e.target.value,
      });

    if (e.target.value !== "" || e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region Reset the page
  reset() {
    this.setState(this.initialState);
  }
  //#endregion

  //#region Save Customer Code
  saveCustomer = (e) => {
    e.preventDefault();

    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while adding Customer...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        CustomerID: this.state.CustomerID,
        CustomerCode: this.state.CustomerCode,
        UserID: helper.getUser(),
      };

      //Service call
      customerService
        .createCustomer(data)
        .then(() => {
          toast.success("Customer Added Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/Customers",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  render() {
    const { custCodeErr } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Customers</span>
          </div>
          <h4>
            Create Customer{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={() => this.props.history.goBack()}
                title="Back to List"
              ></i>
            </span>
          </h4>
          <div>
            <div className="row row-sm">
              <div className="col-lg">
                <label>
                  Customer Code{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="CustomerCode"
                  value={this.state.CustomerCode}
                  onChange={this.onChangeCustomer}
                  name="CustomerCode"
                  maxLength="3"
                  tabIndex="2"
                />
                {custCodeErr && (
                  <div className="error-message">{custCodeErr}</div>
                )}
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2 mg-t-10 mg-lg-t-0">
                <button
                  id="Save"
                  onClick={this.saveCustomer}
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="3"
                >
                  Save
                </button>
              </div>
              <div className="col-md-0.5"></div>
              <div className="col-md-2  mg-t-10 mg-lg-t-0">
                <button
                  className="btn btn-gray-700 btn-block"
                  tabIndex="4"
                  id="Reset"
                  onClick={this.reset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default AddCustomer;
