import React, { Component } from "react";
import { Link } from "react-router-dom";
import customerService from "../../services/customer.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Button, Modal } from "react-bootstrap";
import accessControlService from "../../services/accessControl.service";
toast.configure();

class ViewCustomer extends Component {
  constructor(props) {
    super(props);
    this.showPopUp = this.showPopUp.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.handleYes = this.handleYes.bind(this);

    this.state = {
      customers: [
        {
          CustomerID: 0,
          CustomerCode: "",
        },
      ],
      showModal: false,
      canAccessEditCustomer: false,
      canAccessDeleteCustomer: false,
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  handleNo() {
    this.setState({ showModal: false });
  }

  showPopUp() {
    this.setState({ showModal: true });
  }

  //Page Load
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Edit Customer");
    this.canUserAccessPage("Delete Customer");
    this.fetchCustomers();
  }

  //#region Fetching selected customer details
  fetchCustomers() {
    const { state } = this.props.location; // Customer ID passed from Customer List Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/Customers");
      return;
    }
    this.setState({
      spinnerMessage: "Please wait while loading Customer...",
      loading: true,
    });

    customerService
      .getCustomer(state, helper.getUser())
      .then((response) => {
        this.setState({
          customers: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Customer page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Edit Customer") {
          this.setState({
            canAccessEditCustomer: response.data,
          });
        } else if (pageName === "Delete Customer") {
          this.setState({
            canAccessDeleteCustomer: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Delete Customer
  handleYes() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while deleting Customer...",
      loading: true,
    });

    customerService
      .deleteCustomer(this.state.customers.CustomerID, helper.getUser())
      .then((response) => {
        this.setState({ showModal: false, loading: false });
        toast.success("Customer Deleted Successfully");
        this.props.history.push({
          pathname: "/Masters/Customers",
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
        this.handleNo();
      });
  }
  //#endregion

  //#region UI
  render() {
    const { CustomerID, CustomerCode } = this.state.customers;
    const canAccessEditCustomer = this.state.canAccessEditCustomer;
    const canAccessDeleteCustomer = this.state.canAccessDeleteCustomer;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Customers</span>
          </div>
          <h4>
            View Customer{" "}
            <span className="icon-size">
              {" "}
              <Link to="/Masters/Customers" title="Back to List">
                <i className="far fa-arrow-alt-circle-left"></i>
              </Link>
            </span>
          </h4>
          <br />
          <div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Customer ID</b>
                  </div>
                  <div className="col-md-2">
                    <p>{CustomerID}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Customer Code</b>
                  </div>
                  <div className="col-md-2">
                    <p>{CustomerCode}</p>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-0.5"></div>
              {canAccessEditCustomer && (
                <div className="col-md-2">
                  <Link
                    to={{
                      pathname: "/Masters/EditCustomer",
                      state: CustomerID, // passing customer ID to Edit Customer Page
                    }}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Edit
                  </Link>
                </div>
              )}
              <div className="col-md-0.5"></div>
              {canAccessDeleteCustomer && (
                <div className="col-md-2">
                  <button
                    onClick={this.showPopUp}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>
          </div>
        </LoadingOverlay>
        <Modal
          show={this.state.showModal}
          aria-labelledby="contained-modal-title-vcenter"
          onHide={this.handleNo}
          backdrop="static"
          enforceFocus={false}
        >
          <Modal.Header>
            <Modal.Title>Delete Customer</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <p>Are you sure to delete this Customer?</p>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleYes}>
              Yes
            </Button>
            <Button variant="primary" onClick={this.handleNo}>
              No
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
  //#endregion
}

export default ViewCustomer;
