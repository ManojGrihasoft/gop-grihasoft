import React, { Component } from "react";
import customerService from "../../services/customer.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class EditCustomer extends Component {
  constructor(props) {
    super(props);
    this.onChangeCustomer = this.onChangeCustomer.bind(this);
    this.reset = this.reset.bind(this);
    this.state = {
      CustomerID: 0,
      CustomerCode: "",
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };
  }

  //#region Page Load
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchCustomers();
  }
  //#endregion

  //#region Fetching selected customer details
  fetchCustomers() {
    const { state } = this.props.location; // Customer ID passed from View Customer Page

    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Customer...",
      loading: true,
    });

    customerService
      .getCustomer(state, helper.getUser())
      .then((response) => {
        this.setState({
          CustomerID: response.data.CustomerID,
          CustomerCode: response.data.CustomerCode,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Bind control value to state variable
  onChangeCustomer(e) {
    const re = /^[A-Za-z]+$/;
    if (e.target.value === "" || re.test(e.target.value))
      this.setState({
        CustomerCode: e.target.value,
      });

    if (e.target.value !== "" || e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region Redirect to Customer List Page
  moveToCustomerList = (e) => {
    this.props.history.push("/Masters/Customers");
  };
  //#endregion

  //#region Reset the page
  reset() {
    this.setState({
      formErrors: {},
    });
    this.fetchCustomers();
  }
  //#endregion

  //#region Validations
  handleFormValidation() {
    const custCode = this.state.CustomerCode;
    let formErrors = {};
    let isValidForm = true;

    //Customer code
    if (!custCode) {
      isValidForm = false;
      formErrors["custCodeErr"] = "Customer Code is required.";
    } else if (custCode.length !== 3) {
      isValidForm = false;
      formErrors["custCodeErr"] = "Customer Code must be minimum 3 letters.";
    }
    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Save Customer Code
  saveCustomer = (e) => {
    e.preventDefault();

    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while editing the Customer...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        CustomerID: this.state.CustomerID,
        CustomerCode: this.state.CustomerCode,
        UserID: helper.getUser(),
      };

      //Service call
      customerService
        .updateCustomer(this.state.CustomerID, data)
        .then((response) => {
          toast.success("Customer Code Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/Customers",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  render() {
    const { custCodeErr } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    //#region UI
    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Customers</span>
          </div>
          <h4>
            Edit Customer{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={this.moveToCustomerList}
                title="Back to List"
              ></i>
            </span>
          </h4>
          <div id="Add_form">
            <div className="row row-sm">
              <div className="col-lg">
                <div className="row">
                  <div className="col-md-8">
                    <div className="row row-sm">
                      <div className="col-md-6">
                        <b>Customer ID</b>{" "}
                        <span className="text-danger asterisk-size">*</span>
                      </div>
                      <div className="col-md-2 mg-t-10 mg-l-10">
                        <p> {this.state.CustomerID}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <div className="row">
              <div className="col-md-2">
                <b>Customer Code </b>
                <span className="text-danger asterisk-size">*</span>
              </div>
              <div className="col-md-5">
                <input
                  type="text"
                  className="form-control"
                  tabIndex="2"
                  id="CustomerCode"
                  value={this.state.CustomerCode}
                  onChange={this.onChangeCustomer}
                  name="CustomerCode"
                  maxLength="3"
                />
                {custCodeErr && (
                  <div className="error-message">{custCodeErr}</div>
                )}
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  tabIndex="3"
                  onClick={this.saveCustomer}
                  id="Save"
                >
                  Save
                </button>
              </div>
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  id="Reset"
                  onClick={this.reset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
    //#endregion
  }
}

export default EditCustomer;
