import React, { Component } from "react";
import helper from "../../helpers/helpers";
import userService from "../../services/user.service";
import userRoleService from "../../services/userRole.service";
import tableFunctions from "../../helpers/tableFunctions";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class createUserRole extends Component {
  constructor(props) {
    super(props); //reference to the parent constructor

    this.divScrollRef = React.createRef();

    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.sortData = this.sortData.bind(this);
    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.clearSearchField = this.clearSearchField.bind(this);
    this.clearSortFields = this.clearSortFields.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      userRoles: [],
      users: [],
      formErrors: {},
      username: "",
      grantAll: false,
      revokeAll: false,
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      selectedColumn: "",
      selectedSort: "",
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      filteredArray: [],
      filterValue: "",
    };

    this.initialState = this.state;
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchUsersList();
  }
  //#endregion

  //#region fetching Users from Web API
  fetchUsersList() {
    this.setState({
      spinnerMessage: "Please wait while loading User List...",
      loading: true,
    });

    userService
      .getAllUsers(helper.getUser())
      .then((response) => {
        this.setState({
          users: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Fetching Rolename based on Username
  onChangeUsername(e) {
    this.setState(
      {
        username: e.target.value,
      },
      () => {
        const username = this.state.username.split("-");

        userRoleService
          .ReadUserRolesByUserName(username[0].trim(), helper.getUser())
          .then((response) => {
            this.setState({
              userRoles: response.data,
              isToShowSortingFields: false,
              isToShowFilteringField: false,
            });
          })
          .catch((e) => {
            toast.error(e.response.data.Message, { autoClose: false });
          });
      }
    );

    if (e.target.value !== "" || e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(RoleName, IsActive) {
    var index = this.state.userRoles.findIndex((x) => x.RoleName === RoleName);

    const userRole = [...this.state.userRoles];
    userRole[index] = { ...userRole[index], IsActive: IsActive };

    this.setState({
      userRoles: userRole,
      grantAll: false,
      revokeAll: false,
    });
  }
  //#endregion

  //#region  Validating the input data
  handleFormValidation() {
    const userName = this.state.username.trim();
    let formErrors = {};
    let isValidForm = true;

    //validating user name
    if (!userName) {
      isValidForm = false;
      formErrors["UsernameError"] = "Username is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Save User Role
  saveUserRole = (e) => {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while saving User Role...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        UserRoleID: 0,
        UserRolesList: this.state.userRoles,
        UserID: helper.getUser(),
      };

      //Service call
      userRoleService
        .createUserRole(data)
        .then(() => {
          toast.success("User Role Added Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/admin/UserRolesList",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  //#region Reset the page
  reset() {
    this.setState(this.initialState);
    this.componentDidMount();
  }
  //#endregion

  //#region Grant All or Revoke all
  grantAllOrRevokeAll(value) {
    if (value) {
      this.setState({
        grantAll: true,
        revokeAll: false,
      });
    } else {
      this.setState({
        grantAll: false,
        revokeAll: true,
      });
    }

    const tmpUserRoles = [...this.state.userRoles];

    const newUserRoles = tmpUserRoles.map((obj) => {
      return { ...obj, IsActive: value };
    });

    this.setState({
      userRoles: newUserRoles,
    });
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  //#region Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.userRoles[0]);
    let indexOfUserName = columns.indexOf("UserName");

    columns.splice(indexOfUserName, 1);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "IsActive"];

    sortedArray = tableFunctions.sortData(
      this.state.userRoles,
      column,
      selectedSort,
      numberColumns,
      []
    );

    this.setState({ userRoles: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSortFields() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.userRoles,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearchField() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  render() {
    const data = this.state.userRoles.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);

    const userRoleColumns = [
      {
        dataField: "SlNo",
        align: "center",
      },
      {
        dataField: "UserName",
        align: "center",
        hidden: true,
      },
      {
        dataField: "RoleName",
      },
      {
        dataField: "IsActive",
        align: "center",
        formatter: (cell, row, rowIndex, extraData) => (
          <div>
            <input
              type="checkbox"
              value={row.IsActive}
              onChange={() =>
                this.onChangeIsActive(row.RoleName, !row.IsActive)
              }
              checked={row.IsActive}
            />
          </div>
        ),
      },
    ];

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Users</span>
            <span>User Roles</span>
          </div>
          <h4>
            Create User Role(s){" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={() => this.props.history.goBack()}
                title="Back to List"
              ></i>
            </span>
          </h4>
          <div id="Add_form">
            <div className="row">
              <div className="col-md-2 text-nowrap">
                <b>Username</b>{" "}
                <span className="text-danger asterisk-size">*</span>
              </div>
              <div className="col-md-4">
                <select
                  className="form-control"
                  tabIndex="1"
                  id="username"
                  name="username"
                  placeholder="--Select--"
                  value={this.state.username}
                  onChange={this.onChangeUsername}
                >
                  <option value="">--Select--</option>
                  {this.state.users.map((user) => (
                    <option key={user.UserID}>
                      {user.UserName +
                        " - " +
                        user.FirstName +
                        (user.MiddleName ? " " + user.MiddleName : "") +
                        (user.LastName ? " " + user.LastName : "")}
                    </option>
                  ))}
                </select>
                <div className="error-message">
                  {this.state.formErrors["UsernameError"]}
                </div>
              </div>
            </div>
            <ToolkitProvider
              keyField="SlNo"
              data={this.state.filterValue === "" ? data : filteredData}
              columns={userRoleColumns}
              search
            >
              {(props) => (
                <div>
                  <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                    <div className="col-md-9" style={{ whiteSpace: "nowrap" }}>
                      <div className="row">
                        {this.state.isToShowSortingFields && (
                          <>
                            <div className="col-md-5">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortColumn">Column:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedColumn}
                                    onChange={this.onChangeColumn}
                                  >
                                    <option value="">--Select--</option>
                                    {this.state.columns.map((col) => (
                                      <option key={col}>{col}</option>
                                    ))}
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-5">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortOrder">Order:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedSort}
                                    onChange={this.onChangeSortOrder}
                                  >
                                    <option value="">--Select--</option>
                                    <option value="ascending">Ascending</option>
                                    <option value="descending">
                                      Descending
                                    </option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-1">
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSortFields}
                                  title="Clear Sort Fields"
                                >
                                  <i className="far fa-window-close"></i>
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {this.state.isToShowFilteringField && (
                          <>
                            <div className="col-md-1 mg-t-7">
                              <label htmlFor="search">Search:</label>
                            </div>
                            <div className="col-md-9">
                              <input
                                type="text"
                                className="form-control mg-l-5"
                                maxLength="20"
                                value={this.state.filterValue}
                                onChange={this.onChangefilterValue}
                              />
                            </div>
                            <div>
                              <span
                                className="btn btn-primary pd-b-5"
                                onClick={this.clearSearchField}
                              >
                                <i
                                  className="far fa-window-close"
                                  title="Clear Filter"
                                ></i>
                              </span>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    <div
                      className="col-md-3"
                      style={{ textAlign: "end", marginTop: "10px" }}
                    >
                      <div className="row text-nowrap">
                        {this.state.userRoles.length > 0 && (
                          <>
                            <div>
                              <input
                                type="checkbox"
                                id="grantAll"
                                value={this.state.grantAll}
                                onChange={() => this.grantAllOrRevokeAll(true)}
                                checked={this.state.grantAll}
                              />
                              <label htmlFor="grantAll" className="mg-l-5">
                                Grant All
                              </label>
                              <input
                                type="checkbox"
                                className="mg-l-20"
                                id="revokeAll"
                                value={this.state.revokeAll}
                                onChange={() => this.grantAllOrRevokeAll(false)}
                                checked={this.state.revokeAll}
                              />
                              <label htmlFor="revokeAll" className="mg-l-5">
                                Revoke All
                              </label>
                            </div>
                            <div className="mg-l-5">
                              <i
                                className="fas fa-exchange-alt fa-rotate-90 pointer"
                                title={
                                  this.state.isToShowSortingFields
                                    ? "Hide Sort"
                                    : "Show Sort"
                                }
                                onClick={this.displaySortingFields}
                              ></i>
                              {!this.state.isToShowFilteringField ? (
                                <i
                                  className="fas fa-filter pointer mg-l-10"
                                  onClick={this.displayFilteringField}
                                  title="Show Filter"
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-funnel-dollar pointer mg-l-10"
                                  onClick={this.displayFilteringField}
                                  title="Hide Filter"
                                ></i>
                              )}
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                  <div>
                    <table
                      style={{
                        width:
                          (this.state.filteredArray.length < 12 &&
                            this.state.filterValue !== "") ||
                          this.state.userRoles.length < 12
                            ? "100%"
                            : "98.8%",
                      }}
                    >
                      <thead>
                        <tr>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 12 &&
                                  this.state.filterValue !== "") ||
                                this.state.userRoles.length < 12
                                  ? "18.2%"
                                  : "17.7%",
                            }}
                          >
                            Sl No
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 12 &&
                                  this.state.filterValue !== "") ||
                                this.state.userRoles.length < 12
                                  ? "18.2%"
                                  : "17.7%",
                            }}
                          >
                            Role Name
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 12 &&
                                  this.state.filterValue !== "") ||
                                this.state.userRoles.length < 12
                                  ? "18.2%"
                                  : "17.8%",
                            }}
                          >
                            Is Active?
                          </td>
                        </tr>
                      </thead>
                    </table>
                  </div>
                  <div
                    style={
                      (this.state.filteredArray.length > 12 &&
                        this.state.filterValue !== "") ||
                      (this.state.userRoles.length > 12 &&
                        this.state.filterValue === "")
                        ? {
                            height: "325px",
                            overflowY: "scroll",
                            borderBottom: "1px solid #cdd4e0",
                          }
                        : {}
                    }
                    ref={this.divScrollRef}
                    className="scrollable-element"
                    onScroll={this.handleScroll}
                  >
                    <BootstrapTable
                      bootstrap4
                      {...props.baseProps}
                      striped
                      hover
                      condensed
                      headerClasses="header-class"
                    />
                    <div className="col-md-10">
                      {((this.state.index <= this.state.userRoles.length &&
                        this.state.filterValue === "") ||
                        this.state.index <=
                          this.state.filteredArray.length) && (
                        <p>loading more rows, please scroll...</p>
                      )}
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
            <div className="row">
              <div className="row row-sm col-md-11 mg-t-10">
                <div className="col-md-4"></div>
                <div className="col-md-2 mg-t-10 mg-lg-t-0">
                  <button
                    id="Save"
                    onClick={this.saveUserRole}
                    className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                    tabIndex="3"
                  >
                    Save
                  </button>
                </div>
                <div className="col-md-1"></div>
                <div className="col-md-2  mg-t-10 mg-lg-t-0">
                  <button
                    className="btn btn-gray-700 btn-block"
                    tabIndex="4"
                    id="Reset"
                    onClick={this.reset}
                  >
                    Reset
                  </button>
                </div>
              </div>
              {this.state.position > 600 && this.state.filterValue === "" && (
                <div style={{ textAlign: "end" }}>
                  <button className="scroll-top" onClick={this.scrollToTop}>
                    <div className="arrow up"></div>
                  </button>
                </div>
              )}
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default createUserRole;
