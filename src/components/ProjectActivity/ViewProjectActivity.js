import React, { Component } from "react";
import { Link } from "react-router-dom";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import projectActivityService from "../../services/projectActivity.service";
import helper from "../../helpers/helpers";
import { Button, Modal } from "react-bootstrap";
import accessControlService from "../../services/accessControl.service";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class ViewProjectActivity extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.handleYes = this.handleYes.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.showPopUp = this.showPopUp.bind(this);

    this.state = {
      ProjectActivities: [
        {
          ProjectActivityID: 0,
          Activity: "",
          IsActive: true,
        },
      ],
      showModal: false,
      canAccessEditProjectActivity: false,
      canAccessDeleteProjectActivity: false,
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region modal functions
  //#region show popup
  showPopUp() {
    this.setState({ showModal: true });
  }
  //#endregion

  //#region handle Yes
  handleYes() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    projectActivityService
      .deleteProjectActivity(
        this.state.ProjectActivities.ProjectActivityID,
        helper.getUser()
      )
      .then(() => {
        this.setState({ showModal: false });
        toast.success("Project Activity Deleted Successfully");
        this.props.history.push({
          pathname: "/Masters/ProjectActivities",
        });
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
        this.handleNo();
        console.log(e);
      });
  }
  //#endregion

  //#region handle No
  handleNo() {
    this.setState({ showModal: false });
  }
  //#endregion
  //#endregion

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Edit Project Activity");
    this.canUserAccessPage("Delete Project Activity");
    this.fetchProjectActivity();
  }
  //#endregion

  //#region Fetching selected Project Activity details
  fetchProjectActivity() {
    const { state } = this.props.location; // Project Activity ID passed from Project Activity List Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/ProjectActivities");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Project Activities...",
      loading: true,
    });

    projectActivityService
      .getProjectActivity(state, helper.getUser())
      .then((response) => {
        this.setState({
          ProjectActivities: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Customer page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Edit Project Activity") {
          this.setState({
            canAccessEditProjectActivity: response.data,
          });
        } else if (pageName === "Delete Project Activity") {
          this.setState({
            canAccessDeleteProjectActivity: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region UI
  render() {
    const { ProjectActivityID, Activity, IsActive } =
      this.state.ProjectActivities;

    const canAccessEditProjectActivity =
      this.state.canAccessEditProjectActivity;
    const canAccessDeleteProjectActivity =
      this.state.canAccessDeleteProjectActivity;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Project-Activities</span>
          </div>
          <h4>
            View Project Activity{" "}
            <span className="icon-size">
              {" "}
              <Link to="/Masters/ProjectActivities" title="Back to List">
                <i className="far fa-arrow-alt-circle-left"></i>
              </Link>
            </span>
          </h4>
          <br />
          <div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-3">
                    <b>Project Activity ID</b>
                  </div>
                  <div className="col-md-6">
                    <p>{ProjectActivityID}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Activity</b>
                  </div>
                  <div className="col-md-6">
                    <p>{Activity}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-3">
                    <b>Is Active?</b>
                  </div>
                  <div className="col-md-6">
                    {IsActive === false && <p>No</p>}
                    {IsActive === true && <p>Yes</p>}
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-0.5"></div>
              {canAccessEditProjectActivity && (
                <div className="col-md-2">
                  <Link
                    to={{
                      pathname: "/Masters/EditProjectActivity",
                      state: ProjectActivityID, // passing Project Activity to Edit Project Activity Page
                    }}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Edit
                  </Link>
                </div>
              )}
              <div className="col-md-0.5"></div>
              {canAccessDeleteProjectActivity && (
                <div className="col-md-2">
                  <button
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                    onClick={this.showPopUp}
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>
            <Modal
              show={this.state.showModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.handleNo}
              backdrop="static"
              enforceFocus={false}
            >
              <Modal.Header>
                <Modal.Title>Delete Project Activity</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <p>Are you sure to delete this Project Activity?</p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="danger" onClick={this.handleYes}>
                  Yes
                </Button>
                <Button variant="primary" onClick={this.handleNo}>
                  No
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default ViewProjectActivity;
