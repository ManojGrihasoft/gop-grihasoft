import React, { Component } from "react";
import projectSubActivityService from "../../services/projectSubActivity.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class EditProjectSubActivity extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.onChangeSubActivity = this.onChangeSubActivity.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.moveToSubActivityList = this.moveToSubActivityList.bind(this);
    this.saveProjectSubActivity = this.saveProjectSubActivity.bind(this);
    this.reset = this.reset.bind(this);

    //state
    this.state = {
      subActivityID: 0,
      subActivity: "",
      isActive: true,
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchProjectSubActivity();
  }
  //#endregion

  //#region Fetching selected Project Sub Activity details
  fetchProjectSubActivity() {
    const { state } = this.props.location; // Project Sub Activity ID passed from View Project Sub Activity Page

    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/ProjectSubActivities");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Project Sub Activities...",
      loading: true,
    });

    projectSubActivityService
      .getProjectSubActivity(state, helper.getUser())
      .then((response) => {
        this.setState({
          subActivityID: response.data.ProjectSubActivityID,
          subActivity: response.data.SubActivity,
          isActive: response.data.IsActive,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region  Validating the input data
  handleFormValidation() {
    const subActivity = this.state.subActivity.trim();
    let formErrors = {};
    let isValidForm = true;

    //Sub-activity
    if (!subActivity) {
      isValidForm = false;
      formErrors["subActivityError"] = "Sub Activity is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Bind control value to state variable
  onChangeSubActivity(e) {
    this.setState({
      subActivity: e.target.value,
    });

    if (e.target.value !== "" || e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(e) {
    this.setState({
      isActive: e.target.checked,
    });
  }
  //#endregion

  //#region Redirect to Project Sub Activity List Page
  moveToSubActivityList = (e) => {
    this.props.history.push("/Masters/ProjectSubActivities");
  };
  //#endregion

  //#region Reset the page
  reset() {
    this.fetchProjectSubActivity();
    this.setState({ formErrors: {} });
  }
  //#endregion

  //#region Save Project Sub Activity
  saveProjectSubActivity = (e) => {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while editing the Project Sub-Activity...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        ProjectSubActivityID: this.state.subActivityID,
        SubActivity: this.state.subActivity.trim(),
        IsActive: this.state.isActive,
        UserID: helper.getUser(),
      };

      //Service call
      projectSubActivityService
        .updateProjectSubActivity(this.state.subActivityID, data)
        .then(() => {
          toast.success("Sub Activity Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/ProjectSubActivities",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  render() {
    const { subActivityError } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Sub-Activities</span>
          </div>
          <h4>
            Edit Project Sub-Activity{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={this.moveToSubActivityList}
                title="Back to List"
                tabIndex="1"
              ></i>
            </span>
          </h4>
          <div>
            <div className="row row-sm">
              <div className="col-md-3">
                <label style={{ display: "inline-block" }}>
                  <b>Project Sub-Activity ID </b>{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
              </div>
              <div className="col-md-5 mg-t-7 mg-l-10">
                <p> {this.state.subActivityID}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-3">
                <b>Sub-Activity</b>{" "}
                <span className="text-danger asterisk-size">*</span>
              </div>
              <div className="col-md-5">
                <input
                  type="text"
                  className="form-control"
                  tabIndex="2"
                  id="SubActivity"
                  name="SubActivity"
                  maxLength="50"
                  value={this.state.subActivity}
                  onChange={this.onChangeSubActivity}
                />
                {subActivityError && (
                  <div className="error-message">{subActivityError}</div>
                )}
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-md-3">
                <label>
                  <b>Is Active?</b>
                </label>
              </div>
              <div className="col-md-5 mg-t-5">
                <input
                  type="checkbox"
                  value={this.state.isActive}
                  onChange={this.onChangeIsActive}
                  checked={this.state.isActive}
                  tabIndex="3"
                  id="IsActive"
                />
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  id="Save"
                  onClick={this.saveProjectSubActivity}
                >
                  Save
                </button>
              </div>
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="5"
                  id="Reset"
                  onClick={this.reset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default EditProjectSubActivity;
