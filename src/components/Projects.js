import React, { Component } from "react";
import ProjectList from "./Project/ProjectList";
import CreateProject from "./Project/CreateProject";
import EditProject from "./Project/EditProject";
import ViewProject from "./Project/ViewProject";
import ProjectBatchList from "./ProjectBatch/ProjectBatchList";
import CreateProjectBatch from "./ProjectBatch/CreateProjectBatch";
import ViewProjectBatch from "./ProjectBatch/ViewProjectBatch";
import EditProjectBatch from "./ProjectBatch/EditProjectBatch";
import CompletedProjectList from "./Project/DeliveredProjectList";
import PostProjectBatchDetails from "./PostProjectDetails/postProjectBatchDetails";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import DeliveredProjectBatchList from "./ProjectBatch/DeliveredProjectBatchList";
import accessControlService from "../services/accessControl.service";
import helper from "../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class Projects extends Component {
  constructor(props) {
    super(props); //reference to the parent constructor

    this.fetchUserRoleAccess = this.fetchUserRoleAccess.bind(this);

    this.state = {
      accessControl: [],
      projectPageAccess: false,
      loading: false,
      spinnerMessage: "Please wait while loading...",
    };

    this.initialState = this.state;
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchUserRoleAccess();
  }
  //#endregion

  //#region Fetching Logged In User Access
  fetchUserRoleAccess() {
    this.setState({
      spinnerMessage: "Please wait while loading...",
      loading: true,
    });

    accessControlService
      .ReadUserMenuAccessList(helper.getUser(), "Projects")
      .then((response) => {
        this.setState(
          {
            accessControl: response.data,
          },
          () => {
            let projectPageAccess = this.state.accessControl.find(
              (a) => a.PageName === "Project List"
            );

            this.setState({
              projectPageAccess: projectPageAccess.canAccess,
              loading: false,
            });
          }
        );
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  render() {
    const projectPageAccess = this.state.projectPageAccess;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <Router>
            <Switch>
              {projectPageAccess && (
                <Route path="/Projects" exact component={ProjectList}></Route>
              )}
              <Route
                path="/Projects/CreateProject"
                component={CreateProject}
              ></Route>
              <Route
                path="/Projects/EditProject"
                component={EditProject}
              ></Route>
              <Route
                path="/Projects/ViewProject"
                component={ViewProject}
              ></Route>
              <Route
                path="/Projects/ProjectBatchList"
                component={ProjectBatchList}
              ></Route>
              <Route
                path="/Projects/CreateProjectBatch"
                component={CreateProjectBatch}
              ></Route>
              <Route
                path="/Projects/ViewProjectBatch"
                component={ViewProjectBatch}
              ></Route>
              <Route
                path="/Projects/EditProjectBatch"
                component={EditProjectBatch}
              ></Route>
              <Route
                path="/Projects/Completed"
                component={CompletedProjectList}
              ></Route>
              <Route
                path="/Projects/DeliveredProjectBatch"
                component={DeliveredProjectBatchList}
              ></Route>
              <Route
                path="/Projects/PostProjectBatchDetails"
                component={PostProjectBatchDetails}
              ></Route>
            </Switch>
          </Router>
        </LoadingOverlay>
      </div>
    );
  }
}

export default Projects;
