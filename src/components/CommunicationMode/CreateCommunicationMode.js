import React, { Component } from "react";
import communicationModeService from "../../services/communicationMode.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class CreateCommunicationMode extends Component {
  constructor(props) {
    super(props);

    this.onChangeCommunicationMode = this.onChangeCommunicationMode.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      communicationModeID: 0,
      communicationMode: "",
      isActive: true,
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }
  }
  //#endregion

  //#region Bind control value to state variable
  onChangeCommunicationMode(e) {
    this.setState({
      communicationMode: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(e) {
    this.setState({
      isActive: e.target.checked,
    });
  }
  //#endregion

  //#region Reset the page
  reset() {
    this.setState(this.initialState);
  }
  //#endregion

  //#region Save Communication Mode
  saveCommunicationMode = (e) => {
    e.preventDefault();

    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while adding Communication Mode...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        communicationModeID: this.state.communicationModeID,
        communicationMode: this.state.communicationMode.trim(),
        isActive: this.state.isActive,
        UserID: helper.getUser(),
      };

      //Service call
      communicationModeService
        .createCommunicationMode(data)
        .then(() => {
          toast.success("Communication Mode Added Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/CommunicationModeList",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  //#region Validating the input data
  handleFormValidation() {
    const communicationMode = this.state.communicationMode.trim();
    let formErrors = {};
    let isValidForm = true;

    //Communication Mode
    if (!communicationMode) {
      isValidForm = false;
      formErrors["communicationModeError"] = "Communication Mode is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  render() {
    const { communicationModeError } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    //#region UI
    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Communication Mode</span>
          </div>
          <h4>
            Create Communication Mode{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={() => this.props.history.goBack()}
                title="Back to List"
              ></i>
            </span>
          </h4>
          <div>
            <div className="row row-sm">
              <div className="col-lg">
                <label>
                  Communication Mode{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  maxLength="50"
                  id="CommunicationMode"
                  name="CommunicationMode"
                  tabIndex="2"
                  value={this.state.communicationMode}
                  onChange={this.onChangeCommunicationMode}
                />
                {communicationModeError && (
                  <div className="error-message">{communicationModeError}</div>
                )}
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-lg">
                <label>Is Active?</label>
                <input
                  type="checkbox"
                  className="mg-l-20"
                  tabIndex="3"
                  value={this.state.isActive}
                  onChange={this.onChangeIsActive}
                  checked={this.state.isActive}
                />
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2 mg-t-10 mg-lg-t-0">
                <button
                  id="Save"
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  onClick={this.saveCommunicationMode}
                >
                  Save
                </button>
              </div>
              <div className="col-md-0.5"></div>
              <div className="col-md-2  mg-t-10 mg-lg-t-0">
                <button
                  className="btn btn-gray-700 btn-block"
                  tabIndex="5"
                  onClick={this.reset}
                  id="Reset"
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
    //#endregion
  }
}

export default CreateCommunicationMode;
