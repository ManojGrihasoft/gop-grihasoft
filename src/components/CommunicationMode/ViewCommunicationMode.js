import React, { Component } from "react";
import { Link } from "react-router-dom";
import communicationModeService from "../../services/communicationMode.service";
import accessControlService from "../../services/accessControl.service";
import helper from "../../helpers/helpers";
import { Button, Modal } from "react-bootstrap";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class ViewCommunicationMode extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.

    this.handleYes = this.handleYes.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.showPopUp = this.showPopUp.bind(this);

    this.state = {
      CommunicationModes: [
        {
          CommunicationModeID: 0,
          CommunicationMode: "",
          IsActive: true,
        },
      ],
      showModal: false,
      canAccessEditCommunicationMode: false,
      canAccessDeleteCommunicationMode: false,
      loading: false,
      spinnerMessage: "",
    };

    this.initialState = this.state;
  }

  //#region component mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.canUserAccessPage("Edit Communication Mode");
    this.canUserAccessPage("Delete Communication Mode");
    this.fetchCommunicationMode();
  }
  //#endregion

  //#region modal functions
  //#region show popup
  showPopUp() {
    this.setState({ showModal: true });
  }
  //#endregion

  //#region handle Yes
  handleYes() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while deleting Communication Mode...",
      loading: true,
    });

    communicationModeService
      .deleteCommunicationMode(
        this.state.CommunicationModes.CommunicationModeID,
        helper.getUser()
      )
      .then(() => {
        this.setState({ showModal: false });
        toast.success("Communication Mode Deleted Successfully");
        this.props.history.push({
          pathname: "/Masters/CommunicationModeList",
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
        this.handleNo();
      });
  }
  //#endregion

  //#region handle No
  handleNo() {
    this.setState({ showModal: false });
  }
  //#endregion
  //#endregion

  //#region Fetching selected Communication Mode details
  fetchCommunicationMode() {
    const { state } = this.props.location; // Communication Mode ID passed from Communication Mode List Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/CommunicationModeList");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Communication Mode...",
      loading: true,
    });

    communicationModeService
      .getCommunicationMode(state, helper.getUser())
      .then((response) => {
        this.setState({
          CommunicationModes: response.data,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region fetching Communication Mode page access
  canUserAccessPage(pageName) {
    accessControlService
      .CanUserAccessPage(helper.getUser(), pageName)
      .then((response) => {
        if (pageName === "Edit Communication Mode") {
          this.setState({
            canAccessEditCommunicationMode: response.data,
          });
        } else if (pageName === "Delete Communication Mode") {
          this.setState({
            canAccessDeleteCommunicationMode: response.data,
          });
        }
      })
      .catch((e) => {
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region UI
  render() {
    const { CommunicationModeID, CommunicationMode, IsActive } =
      this.state.CommunicationModes;

    const canAccessEditCommunicationMode =
      this.state.canAccessEditCommunicationMode;
    const canAccessDeleteCommunicationMode =
      this.state.canAccessDeleteCommunicationMode;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Communication Modes</span>
          </div>
          <h4>
            View Communication Mode{" "}
            <span className="icon-size">
              {" "}
              <Link to="/Masters/CommunicationModeList" title="Back to List">
                <i className="far fa-arrow-alt-circle-left"></i>
              </Link>
            </span>
          </h4>
          <br />
          <div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm mg-b-5">
                  <div className="col-md-4">
                    <b>Communication Mode ID</b>
                  </div>
                  <div className="col-md-6">
                    <p>{CommunicationModeID}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-4">
                    <b>Communication Mode</b>
                  </div>
                  <div className="col-md-6">
                    <p>{CommunicationMode}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-8">
                <div className="row row-sm">
                  <div className="col-md-4">
                    <b>Is Active?</b>
                  </div>
                  <div className="col-md-6">
                    {IsActive === false && <p>No</p>}
                    {IsActive === true && <p>Yes</p>}
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-0.5"></div>
              {canAccessEditCommunicationMode && (
                <div className="col-md-2">
                  <Link
                    to={{
                      pathname: "/Masters/EditCommunicationMode",
                      state: CommunicationModeID, // passing Communication Mode ID to Edit Communication Mode Page
                    }}
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                  >
                    Edit
                  </Link>
                </div>
              )}
              <div className="col-md-0.5"></div>
              {canAccessDeleteCommunicationMode && (
                <div className="col-md-2">
                  <button
                    className="mg-t-10 mg-md-t-0 btn  btn-gray-700 btn-block"
                    onClick={this.showPopUp}
                  >
                    Delete
                  </button>
                </div>
              )}
            </div>
            <Modal
              show={this.state.showModal}
              aria-labelledby="contained-modal-title-vcenter"
              onHide={this.handleNo}
              backdrop="static"
              enforceFocus={false}
            >
              <Modal.Header>
                <Modal.Title>Delete Communication Mode</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <p>Are you sure to delete this Communication Mode?</p>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="danger" onClick={this.handleYes}>
                  Yes
                </Button>
                <Button variant="primary" onClick={this.handleNo}>
                  No
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
  //#endregion
}

export default ViewCommunicationMode;
