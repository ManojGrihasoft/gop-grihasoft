import React, { Component } from "react";
import communicationModeService from "../../services/communicationMode.service";
import helper from "../../helpers/helpers";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { css } from "@emotion/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class EditCommunicationMode extends Component {
  constructor(props) {
    super(props); //reference to the parents constructor() function.
    //add super(props) every single time we define a constructor() function inside a class-based component
    this.onChangeCommunicationMode = this.onChangeCommunicationMode.bind(this);
    this.onChangeIsActive = this.onChangeIsActive.bind(this);
    this.moveToCommunicationModeList =
      this.moveToCommunicationModeList.bind(this);
    this.saveCommunicationMode = this.saveCommunicationMode.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      communicationModeID: 0,
      communicationMode: "",
      isActive: true,
      formErrors: {},
      loading: false,
      spinnerMessage: "",
    };
  }

  //#region Component Mount
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchCommunicationMode();
  }
  //#endregion

  //#region Fetching selected Communication Mode details
  fetchCommunicationMode() {
    const { state } = this.props.location; // Communication Mode ID passed from View Communication Mode Page
    if (state === 0 || state === null || state === undefined) {
      this.props.history.push("/Masters/CommunicationModeList");
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while loading Communication Mode...",
      loading: true,
    });

    communicationModeService
      .getCommunicationMode(state, helper.getUser())
      .then((response) => {
        this.setState({
          communicationModeID: response.data.CommunicationModeID,
          communicationMode: response.data.CommunicationMode,
          isActive: response.data.IsActive,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({ loading: false });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region bind control value to state variable
  onChangeCommunicationMode(e) {
    this.setState({
      communicationMode: e.target.value,
    });

    if (e.target.value !== "" && e.target.value !== null)
      this.setState({ formErrors: {} });
  }
  //#endregion

  //#region get IsActive value
  onChangeIsActive(e) {
    this.setState({
      isActive: e.target.checked,
    });
  }
  //#endregion

  //#region Redirect to Communication Mode List Page
  moveToCommunicationModeList = (e) => {
    this.props.history.push("/Masters/CommunicationModeList");
  };
  //#endregion

  //#region Save Communication Mode
  saveCommunicationMode = (e) => {
    e.preventDefault(); //cancels the event if it is cancelable,
    //meaning that the default action that belongs to the event will not occur.

    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    if (this.handleFormValidation()) {
      this.setState({
        spinnerMessage: "Please wait while saving Communication Mode...",
        loading: true,
      });

      //Bind state data to object
      var data = {
        CommunicationModeID: this.state.communicationModeID,
        CommunicationMode: this.state.communicationMode.trim(),
        isActive: this.state.isActive,
        UserID: helper.getUser(),
      };

      //Service call
      communicationModeService
        .updateCommunicationMode(this.state.communicationModeID, data)
        .then((response) => {
          toast.success("Communication Mode Updated Successfully");
          this.setState(this.initialState);
          this.props.history.push({
            pathname: "/Masters/CommunicationModeList",
          });
        })
        .catch((error) => {
          this.setState({
            loading: false,
          });
          toast.error(error.response.data.Message, { autoClose: false });
        });
    }
  };
  //#endregion

  //#region Validating the input data
  handleFormValidation() {
    const communicationMode = this.state.communicationMode.trim();
    let formErrors = {};
    let isValidForm = true;

    //communication Mode
    if (!communicationMode) {
      isValidForm = false;
      formErrors["communicationModeError"] = "Communication Mode is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Reset the page
  reset() {
    this.fetchCommunicationMode();
    this.setState({
      formErrors: {},
    });
  }
  //#endregion

  render() {
    const { communicationModeError } = this.state.formErrors;

    const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
      border: none;
    `;

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={override}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="az-content-breadcrumb">
            <span>Master</span>
            <span>Communication Mode List</span>
          </div>
          <h4>
            Edit Communication Mode{" "}
            <span className="icon-size">
              <i
                className="far fa-arrow-alt-circle-left text-primary pointer"
                onClick={this.moveToCommunicationModeList}
                title="Back to List"
                tabIndex="1"
              ></i>
            </span>
          </h4>
          <div>
            <div className="row row-sm">
              <div className="col-md-4">
                <label>
                  <b>Communication Mode ID</b>{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
              </div>
              <div className="col-md-5 mg-t-7 mg-l-10">
                <p> {this.state.communicationModeID}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-md-4">
                <label>
                  <b>Communication Mode</b>{" "}
                  <span className="text-danger asterisk-size">*</span>
                </label>
              </div>
              <div className="col-md-5">
                <input
                  type="text"
                  className="form-control"
                  tabIndex="2"
                  id="CommunicationMode"
                  name="CommunicationMode"
                  maxLength="50"
                  value={this.state.communicationMode}
                  onChange={this.onChangeCommunicationMode}
                />
                {communicationModeError && (
                  <div className="error-message">{communicationModeError}</div>
                )}
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col-md-4">
                <label>
                  <b>Is Active?</b>
                </label>
              </div>
              <div className="col-md-5 mg-t-5">
                <input
                  type="checkbox"
                  value={this.state.isActive}
                  onChange={this.onChangeIsActive}
                  checked={this.state.isActive}
                  tabIndex="3"
                />
              </div>
            </div>
            <br />
            <div className="row row-sm">
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="4"
                  id="Save"
                  onClick={this.saveCommunicationMode}
                >
                  Save
                </button>
              </div>
              <div className="col-md-1"></div>
              <div className="col-md-2">
                <button
                  className="mg-t-10 mg-md-t-0 btn btn-gray-700 btn-block"
                  tabIndex="5"
                  id="Reset"
                  onClick={this.reset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default EditCommunicationMode;
