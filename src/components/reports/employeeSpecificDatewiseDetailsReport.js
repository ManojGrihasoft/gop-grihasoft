import React, { Component } from "react";
import employeeSpecificReportService from "../../services/employeeSpecificReport.service";
import Moment from "moment";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class employeeSpecificDatewiseDetailsReport extends Component {
  constructor(props) {
    super(props);

    this.divScrollRef = React.createRef();

    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.sortData = this.sortData.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.exportEmployeeSpecificDetailsReportToExcel =
      this.exportEmployeeSpecificDetailsReportToExcel.bind(this);

    //#region State Variables
    this.state = {
      employeeSpecificDetailsReport: [],
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
    };
    //#endregion
  }

  //#region page load
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchEmployeeSpecificDetailsReport();
  }
  //#endregion

  //#region fetching Employee Specific Details Report from Web API
  fetchEmployeeSpecificDetailsReport() {
    this.setState({
      spinnerMessage:
        "Please wait while fetching Employee Specific Details Report...",
      loading: true,
    });

    employeeSpecificReportService
      .readEmployeeSpecificDetailsReport(
        this.props.employee,
        this.props.fromDate,
        this.props.toDate
      )
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          ProductionOrQCCompletedOn: Moment(
            obj.ProductionOrQCCompletedOn
          ).format("DD-MMM-yyyy"),
        }));

        if (response.data.length === 0) {
          toast.error("No Data Found!!");
        }
        this.setState({
          employeeSpecificDetailsReport: formattedArray,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Export Employee Specific Details Report to Excel
  exportEmployeeSpecificDetailsReportToExcel() {
    this.setState({
      spinnerMessage:
        "Please wait while exporting Employee Specific Report to Excel...",
      loading: true,
    });

    employeeSpecificReportService
      .exportEmployeeSpecificDetailsReportToExcel(
        this.props.employee,
        this.props.fromDate,
        this.props.toDate
      )
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute(
          "download",
          "Employee Specific Details Report.xlsx"
        );
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.employeeSpecificDetailsReport,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.employeeSpecificDetailsReport[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = [
      "SlNo",
      "ProductionCompletedCount",
      "QCCompletedCount",
      "ProductionTarget",
      "ProductionProductiveHours",
      "QCTarget",
      "QCProductiveHours",
    ];

    let dateColumns = ["ProductionOrQCCompletedOn"];

    sortedArray = tableFunctions.sortData(
      this.state.employeeSpecificDetailsReport,
      column,
      selectedSort,
      numberColumns,
      dateColumns
    );

    this.setState({ employeeSpecificDetailsReport: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  render() {
    const data = this.state.employeeSpecificDetailsReport.slice(
      0,
      this.state.index
    );
    const filteredData = this.state.filteredArray.slice(0, this.state.index);
    const employeeSpecificDetailsReportLength =
      this.state.employeeSpecificDetailsReport.length;

    //#region Employee Specifc Details Report Columns
    const employeeSpecificDetailsReportColumns = [
      {
        dataField: "SlNo",
        align: "center",
        style: {
          width: "5.5%",
        },
      },
      {
        dataField: "CustomerCode",
        align: "center",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "ProjectCode",
        align: "center",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "BatchNo",
        align: "center",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "Activity",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
          width: "19%",
        },
        title: true,
      },
      {
        dataField: "ProductionOrQCCompletedOn",
        align: "center",
        style: {
          width: "15%",
        },
      },
      {
        dataField: "ProductionCompletedCount",
        align: "center",
        style: {
          width: "13%",
        },
      },
      {
        dataField: "ProductionTarget",
        align: "center",
        style: {
          width: "9%",
        },
      },
      {
        dataField: "ProductionProductiveHours",
        align: "center",
        style: {
          width: "14%",
        },
      },
      {
        dataField: "QCCompletedCount",
        align: "center",
        style: {
          width: "13%",
        },
      },
      {
        dataField: "QCTarget",
        align: "center",
        style: {
          width: "11%",
        },
      },
      {
        dataField: "QCProductiveHours",
        align: "center",
        style: {
          width: "14%",
        },
      },
    ];
    //#endregion

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="mg-l-50">
            <ToolkitProvider
              keyField="SlNo"
              data={this.state.filterValue === "" ? data : filteredData}
              columns={employeeSpecificDetailsReportColumns}
            >
              {(props) => (
                <>
                  <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                    <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                      <div className="row">
                        {this.state.isToShowSortingFields && (
                          <>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortColumn">Column:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedColumn}
                                    onChange={this.onChangeColumn}
                                  >
                                    <option value="">--Select--</option>
                                    {this.state.columns.map((col) => (
                                      <option key={col}>{col}</option>
                                    ))}
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortOrder">Order:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedSort}
                                    onChange={this.onChangeSortOrder}
                                  >
                                    <option value="">--Select--</option>
                                    <option value="ascending">Ascending</option>
                                    <option value="descending">
                                      Descending
                                    </option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-2">
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSort}
                                  title="Clear Sort Fields"
                                >
                                  <i className="far fa-window-close"></i>
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {this.state.isToShowFilteringField && (
                          <>
                            <div className="col-md-12">
                              <div
                                className="row"
                                style={{ flexWrap: "nowrap" }}
                              >
                                <div className="col-md-1 mg-t-7">
                                  <label htmlFor="search">Search:</label>
                                </div>
                                <div className="col-lg pd-r-10">
                                  <input
                                    type="text"
                                    className="form-control mg-l-10"
                                    maxLength="20"
                                    value={this.state.filterValue}
                                    onChange={this.onChangefilterValue}
                                  />
                                </div>
                                <div>
                                  <span
                                    className="btn btn-primary pd-b-5"
                                    onClick={this.clearSearch}
                                  >
                                    <i
                                      className="far fa-window-close"
                                      title="Clear Filter"
                                    ></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    {employeeSpecificDetailsReportLength > 0 && (
                      <div
                        className="col-md-2"
                        style={{ textAlign: "end", marginTop: "10px" }}
                      >
                        <i
                          className="fas fa-exchange-alt fa-rotate-90 pointer"
                          title={
                            this.state.isToShowSortingFields
                              ? "Hide Sort"
                              : "Show Sort"
                          }
                          onClick={this.displaySortingFields}
                        ></i>
                        {!this.state.isToShowFilteringField ? (
                          <i
                            className="fas fa-filter pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Show Filter"
                          ></i>
                        ) : (
                          <i
                            className="fas fa-funnel-dollar pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Hide Filter"
                          ></i>
                        )}
                        <i
                          className="fas fa-file-excel mg-l-10 pointer"
                          style={{ color: "green" }}
                          onClick={
                            this.exportEmployeeSpecificDetailsReportToExcel
                          }
                          title="Export to Excel"
                        ></i>
                      </div>
                    )}
                  </div>
                  <div
                    className="borderTable"
                    style={{ overflowX: "auto", width: "98%" }}
                  >
                    <div>
                      <table
                        style={{
                          width:
                            (this.state.filteredArray.length < 10 &&
                              this.state.filterValue !== "") ||
                            this.state.employeeSpecificDetailsReport.length < 10
                              ? "140%"
                              : "139.1%",
                        }}
                      >
                        <thead>
                          <tr>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "4%"
                                    : "4%",
                              }}
                            >
                              Sl No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "5.85%"
                                    : "5.8%",
                              }}
                            >
                              Cus Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "5.8%"
                                    : "5.8%",
                              }}
                            >
                              Proj Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "5.85%"
                                    : "5.8%",
                              }}
                            >
                              Batch No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "13.8%"
                                    : "13.85%",
                              }}
                            >
                              Activity
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "10.9%"
                                    : "10.9%",
                              }}
                            >
                              Prod. or QC Completed on
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "9.45%"
                                    : "9.5%",
                              }}
                            >
                              Prod. Completed Count
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "6.55%"
                                    : "6.55%",
                              }}
                            >
                              Prod. Target
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "10.2%"
                                    : "10.15%",
                              }}
                            >
                              Prod. Productive Hours
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "9.45%"
                                    : "9.45%",
                              }}
                            >
                              QC Completed Count
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "7.95%"
                                    : "8%",
                              }}
                            >
                              QC Target
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificDetailsReport
                                    .length < 10
                                    ? "12.6%"
                                    : "12.6%",
                              }}
                            >
                              QC Productive Hours
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div
                      style={
                        (this.state.filteredArray.length > 10 &&
                          this.state.filterValue !== "") ||
                        (this.state.employeeSpecificDetailsReport.length > 10 &&
                          this.state.filterValue === "")
                          ? {
                              width: "140%",
                              height: "300px",
                              overflowY: "scroll",
                              borderBottom: "1px solid #cdd4e0",
                            }
                          : { width: "140%" }
                      }
                      ref={this.divScrollRef}
                      className="scrollable-element"
                      onScroll={this.handleScroll}
                    >
                      <BootstrapTable
                        bootstrap4
                        {...props.baseProps}
                        striped
                        hover
                        condensed
                        headerClasses="header-class"
                      />
                      <div className="col-md-10">
                        {((this.state.index <=
                          this.state.employeeSpecificDetailsReport.length &&
                          this.state.filterValue === "") ||
                          this.state.index <=
                            this.state.filteredArray.length) && (
                          <p>loading more rows, please scroll...</p>
                        )}
                      </div>
                    </div>
                  </div>
                </>
              )}
            </ToolkitProvider>
            {this.state.position > 600 && this.state.filterValue === "" && (
              <div style={{ textAlign: "end" }}>
                <button className="scroll-top" onClick={this.scrollToTop}>
                  <div className="arrow up"></div>
                </button>
              </div>
            )}
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default employeeSpecificDatewiseDetailsReport;
