import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import Moment from "moment";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import ModernDatepicker from "react-modern-datepicker";
import employeesTaskReportService from "../../services/employeesTaskReport.service";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class employeesTaskReportReport extends Component {
  constructor(props) {
    super(props);

    this.divScrollRef = React.createRef();

    this.onChangeCustomerCode = this.onChangeCustomerCode.bind(this);
    this.onChangeProjectCode = this.onChangeProjectCode.bind(this);
    this.onChangeBatchNo = this.onChangeBatchNo.bind(this);
    this.onChangeDepartment = this.onChangeDepartment.bind(this);
    this.onChangeEmployee = this.onChangeEmployee.bind(this);
    this.onChangeActivity = this.onChangeActivity.bind(this);
    this.onChangeFromDate = this.onChangeFromDate.bind(this);
    this.onChangeToDate = this.onChangeToDate.bind(this);
    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.sortData = this.sortData.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.exportEmployeesTaskReportToExcel =
      this.exportEmployeesTaskReportToExcel.bind(this);

    //#region State Variables
    this.state = {
      customers: [],
      selectedCustomerCode: "(All)",
      projectCodes: [],
      selectedProjectCode: "(All)",
      batches: [],
      selectedBatchNo: "(All)",
      departments: [],
      selectedDepartment: "(All)",
      employees: [],
      selectedEmployee: "(All)",
      activities: [],
      selectedActivity: "(All)",
      fromDate: "",
      toDate: "",
      formErrors: "",
      employeesTaskReport: [],
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
    };
    //#endregion
  }

  //#region page load
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    var fromDate = new Date();
    var toDate = new Date();

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

    fromDate.setDate(firstDay.getDate());
    toDate.setDate(toDate.getDate());

    this.setState(
      {
        fromDate: Moment(fromDate).format("DD-MMM-yyyy"),
        toDate: Moment(toDate).format("DD-MMM-yyyy"),
      },
      () => this.fetchEmployeesTaskReport(true, false, false, true, true, true)
    );
  }
  //#endregion

  //#region fetching Employees Task Report from Web API
  fetchEmployeesTaskReport(
    isToLoadCustomerDropDown,
    isToLoadProjectDropDown,
    isToLoadBatchDropDown,
    isToLoadDepartmentDropDown,
    isToLoadEmployeeDropDown,
    isToLoadActivityDropDown
  ) {
    if (this.handleReportValidation()) {
      this.setState({
        spinnerMessage: "Please wait while loading Employees Task Report...",
        loading: true,
      });

      var data = {
        CustomerCode: this.state.selectedCustomerCode,
        ProjectCode: this.state.selectedProjectCode,
        BatchNo: this.state.selectedBatchNo,
        Department: this.state.selectedDepartment,
        EmployeeName: this.state.selectedEmployee,
        Activity: this.state.selectedActivity,
        FromDate: this.state.fromDate,
        ToDate: this.state.toDate,
      };

      let customerCodes = this.state.customers;
      let projectCodes = this.state.projectCodes;
      let batchNo = this.state.batches;
      let departments = this.state.departments;
      let employees = this.state.employees;
      let activities = this.state.activities;

      employeesTaskReportService
        .readEmployeesTaskDetailsReportData(data)
        .then((response) => {
          if (response.data.length === 0) {
            this.setState({
              loading: false,
            });
            toast.error("No Data Found!!");
            return;
          }

          let formattedArray = response.data.map((obj) => {
            delete obj.FromDate;
            delete obj.ToDate;

            return obj;
          });

          if (isToLoadCustomerDropDown) {
            customerCodes = [
              ...new Set(formattedArray.map((obj) => obj.CustomerCode)),
            ];

            customerCodes = customerCodes.filter(function (el) {
              return el !== "";
            });
          }

          if (isToLoadProjectDropDown) {
            projectCodes = [
              ...new Set(formattedArray.map((obj) => obj.ProjectCode)),
            ];

            projectCodes = projectCodes.filter(function (el) {
              return el !== "";
            });
          }

          if (isToLoadBatchDropDown) {
            batchNo = [...new Set(formattedArray.map((obj) => obj.BatchNo))];

            batchNo = batchNo.filter(function (el) {
              return el !== "";
            });
          }

          if (isToLoadDepartmentDropDown) {
            departments = [
              ...new Set(formattedArray.map((obj) => obj.Department)),
            ];

            departments = departments.filter(function (el) {
              return el !== "";
            });
          }

          if (isToLoadEmployeeDropDown) {
            employees = [
              ...new Set(formattedArray.map((obj) => obj.EmployeeName)),
            ];

            employees = employees.filter(function (el) {
              return el !== "";
            });
          }

          if (isToLoadActivityDropDown) {
            activities = [
              ...new Set(formattedArray.map((obj) => obj.Activity)),
            ];

            activities = activities.filter(function (el) {
              return el !== "";
            });
          }

          this.setState({
            customers: customerCodes,
            projectCodes: projectCodes,
            batches: batchNo,
            departments: departments,
            employees: employees,
            activities: activities,
            employeesTaskReport: formattedArray,
            loading: false,
          });
        })
        .catch((e) => {
          this.setState({
            loading: false,
          });
          toast.error(e.response.data.Message, { autoClose: false });
        });
    }
  }
  //#endregion

  //#region Get Selected Customer Code
  onChangeCustomerCode(e) {
    this.setState(
      {
        selectedCustomerCode: e.target.value,
        selectedProjectCode: "(All)",
        selectedBatchNo: "(All)",
        selectedDepartment: "(All)",
        selectedEmployee: "(All)",
        selectedActivity: "(All)",
      },
      () => this.fetchEmployeesTaskReport(false, true, true, true, true, true)
    );
  }
  //#endregion

  //#region Get Selected Project Code
  onChangeProjectCode(e) {
    this.setState(
      {
        selectedProjectCode: e.target.value,
        selectedBatchNo: "(All)",
        selectedDepartment: "(All)",
        selectedEmployee: "(All)",
        selectedActivity: "(All)",
      },
      () => this.fetchEmployeesTaskReport(false, false, true, true, true, true)
    );
  }
  //#endregion

  //#region Get Selected Batch No
  onChangeBatchNo(e) {
    this.setState(
      {
        selectedBatchNo: e.target.value,
        selectedDepartment: "(All)",
        selectedEmployee: "(All)",
        selectedActivity: "(All)",
      },
      () => this.fetchEmployeesTaskReport(false, false, false, true, true, true)
    );
  }
  //#endregion

  //#region Get Selected Department
  onChangeDepartment(e) {
    this.setState(
      {
        selectedDepartment: e.target.value,
        selectedEmployee: "(All)",
        selectedActivity: "(All)",
      },
      () =>
        this.fetchEmployeesTaskReport(false, false, false, false, true, true)
    );
  }
  //#endregion

  //#region Get Selected Employee
  onChangeEmployee(e) {
    this.setState(
      {
        selectedEmployee: e.target.value,
        selectedActivity: "(All)",
      },
      () =>
        this.fetchEmployeesTaskReport(false, false, false, false, false, true)
    );
  }
  //#endregion

  //#region Get Selected Activity
  onChangeActivity(e) {
    this.setState(
      {
        selectedActivity: e.target.value,
      },
      () =>
        this.fetchEmployeesTaskReport(false, false, false, false, false, false)
    );
  }
  //#endregion

  //#region Get Selected From Date
  onChangeFromDate(date) {
    this.setState(
      {
        fromDate: date,
        employeesTaskReport: [],
        isToShowFilteringField: false,
        isToShowSortingFields: false,
      },
      () => this.fetchEmployeesTaskReport(true, true, true, true, true, true)
    );

    if (date !== "" && date !== null) {
      const formErrors = { ...this.state.formErrors, fromDateError: "" };
      this.setState({ formErrors: formErrors });
    }
  }
  //#endregion

  //#region Get Selected To Date
  onChangeToDate(date) {
    this.setState(
      {
        toDate: date,
        employeesTaskReport: [],
        isToShowFilteringField: false,
        isToShowSortingFields: false,
      },
      () => this.fetchEmployeesTaskReport(true, true, true, true, true, true)
    );

    if (date !== "" && date !== null) {
      const formErrors = { ...this.state.formErrors, toDateError: "" };
      this.setState({ formErrors: formErrors });
    }
  }

  //#endregion

  //#region  Validating the data
  handleReportValidation() {
    const fromDate = this.state.fromDate.trim();
    const toDate = this.state.toDate.trim();

    var fromDateValue = new Date(this.state.fromDate);
    var toDateValue = new Date(this.state.toDate);

    let formErrors = {};
    let isValidForm = true;

    //From Date
    if (!fromDate) {
      isValidForm = false;
      formErrors["fromDateError"] = "From Date is required";
    } else if (fromDateValue > toDateValue) {
      isValidForm = false;
      formErrors["fromDateError"] = "From Date can't be later than To Date";
    }

    //To Date
    if (!toDate) {
      isValidForm = false;
      formErrors["toDateError"] = "To Date is required";
    }

    this.setState({ formErrors: formErrors });
    return isValidForm;
  }
  //#endregion

  //#region Export Employees Task Report to Excel
  exportEmployeesTaskReportToExcel() {
    this.setState({
      spinnerMessage:
        "Please wait while exporting Employees Task Report to Excel...",
      loading: true,
    });

    var data = {
      CustomerCode: this.state.selectedCustomerCode,
      ProjectCode: this.state.selectedProjectCode,
      BatchNo: this.state.selectedBatchNo,
      Department: this.state.selectedDepartment,
      EmployeeName: this.state.selectedEmployee,
      Activity: this.state.selectedActivity,
      FromDate: this.state.fromDate,
      ToDate: this.state.toDate,
    };

    employeesTaskReportService
      .exportEmployeesTaskReportToExcel(data)
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", "Employees Task Details Report.xlsx");
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.employeesTaskReport,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.employeesTaskReport[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = [
      "SlNo",
      "ProductionAllocatedCount",
      "ProductionCompletedCount",
      "QCAllocatedCount",
      "QCCompletedCount",
      "HoursWorked",
    ];

    sortedArray = tableFunctions.sortData(
      this.state.employeesTaskReport,
      column,
      selectedSort,
      numberColumns,
      []
    );

    this.setState({ employeesTaskReport: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  render() {
    const data = this.state.employeesTaskReport.slice(0, this.state.index);
    const filteredData = this.state.filteredArray.slice(0, this.state.index);
    const employeesTaskReportLength = this.state.employeesTaskReport.length;

    const employeesTaskReportColumns = [
      {
        dataField: "SlNo",
        align: "center",
        style: {
          width: "3.4%",
        },
      },
      {
        dataField: "CustomerCode",
        align: "center",
        style: {
          width: "6.5%",
        },
      },
      {
        dataField: "ProjectCode",
        align: "center",
        style: {
          width: "6.8%",
        },
      },
      {
        dataField: "BatchNo",
        align: "center",
        style: {
          width: "6.35%",
        },
      },
      {
        dataField: "EmployeeCode",
        align: "center",
        style: {
          width: "6.6%",
        },
      },
      {
        dataField: "EmployeeName",
        style: {
          width: "15.55%",
        },
      },
      {
        dataField: "Department",
        style: {
          width: "8.5%",
        },
        align: "center",
      },
      {
        dataField: "Manager",
        style: {
          width: "13.5%",
        },
      },
      {
        dataField: "Activity",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
          width: "14.3%",
        },
        title: true,
      },
      {
        dataField: "ProductionAllocatedCount",
        align: "center",
        style: {
          width: "10.8%",
        },
      },
      {
        dataField: "ProductionCompletedCount",
        align: "center",
        style: {
          width: "10.8%",
        },
      },
      {
        dataField: "QCAllocatedCount",
        align: "center",
        style: {
          width: "9.7%",
        },
      },
      {
        dataField: "QCCompletedCount",
        align: "center",
        style: {
          width: "9.6%",
        },
      },
      {
        dataField: "HoursWorked",
        align: "center",
        style: {
          width: "10%",
        },
      },
    ];

    //#region UI
    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div
            style={{ border: "1px solid #cdd4e0" }}
            className="mg-l-50 mg-r-25"
          >
            <div className="row row-sm mg-r-15 mg-l-5 mg-t-5">
              <div className="col-lg">
                <div className="row">
                  <div className="col-md-5 mg-t-5">
                    <b>Customer Code</b>
                  </div>
                  <div className="col-md-6">
                    <select
                      className="form-control"
                      tabIndex="1"
                      id="customerCode"
                      name="customerCode"
                      placeholder="--Select--"
                      value={this.state.selectedCustomerCode}
                      onChange={this.onChangeCustomerCode}
                      onSelect={this.onChangeCustomerCode}
                    >
                      <option value="(All)">(All)</option>
                      {this.state.customers.map((customer) => (
                        <option key={customer}>{customer}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0">
                <div className="row">
                  <div className="col-md-5 mg-t-5">
                    <b>Project Code</b>
                  </div>
                  <div className="col-md-6">
                    <select
                      className="form-control"
                      tabIndex="2"
                      id="projectCode"
                      name="projectCode"
                      placeholder="--Select--"
                      value={this.state.selectedProjectCode}
                      onChange={this.onChangeProjectCode}
                    >
                      <option value="(All)">(All)</option>
                      {this.state.projectCodes.map((projectCode) => (
                        <option key={projectCode}>{projectCode}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              {this.state.batches.length > 0 ? (
                <div className="col-lg mg-t-10 mg-lg-t-0">
                  <div className="row">
                    <div className="col-md-4 mg-t-5">
                      <b>Batch No.</b>
                    </div>
                    <div className="col-md-6">
                      <select
                        className="form-control"
                        tabIndex="3"
                        id="batchNo"
                        name="batchNo"
                        placeholder="--Select--"
                        value={this.state.selectedBatchNo}
                        onChange={this.onChangeBatchNo}
                      >
                        <option value="(All)">(All)</option>
                        {this.state.batches.map((batchNo) => (
                          <option key={batchNo}>{batchNo}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="col-lg mg-t-10 mg-lg-t-0"></div>
              )}
            </div>
            <div className="row row-sm mg-r-15 mg-l-5 mg-t-5">
              <div className="col-lg">
                <div className="row">
                  <div className="col-md-5 mg-t-5">
                    <b>Department</b>
                  </div>
                  <div className="col-md-6">
                    <select
                      className="form-control"
                      id="department"
                      name="department"
                      placeholder="--Select--"
                      value={this.state.selectedDepartment}
                      onChange={this.onChangeDepartment}
                    >
                      <option value="(All)">(All)</option>
                      {this.state.departments.map((department) => (
                        <option key={department}>{department}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0">
                <div className="row">
                  <div className="col-md-5 mg-t-5">
                    <b>Employee Name</b>
                  </div>
                  <div className="col-md-6">
                    <select
                      className="form-control"
                      id="employeeName"
                      name="employeeName"
                      placeholder="--Select--"
                      value={this.state.selectedEmployee}
                      onChange={this.onChangeEmployee}
                    >
                      <option value="(All)">(All)</option>
                      {this.state.employees.map((employee) => (
                        <option key={employee}>{employee}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0">
                <div className="row">
                  <div className="col-md-4 mg-t-5">
                    <b>Activity</b>
                  </div>
                  <div className="col-md-6">
                    <select
                      className="form-control"
                      id="activity"
                      name="activity"
                      value={this.state.selectedActivity}
                      onChange={this.onChangeActivity}
                    >
                      <option value="(All)">(All)</option>
                      {this.state.activities.map((activity) => (
                        <option key={activity}>{activity}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="row row-sm mg-r-15 mg-l-5 mg-t-5">
              <div className="col-lg">
                <div className="row row-sm">
                  <div className="col-md-5">
                    <b>From Date</b>{" "}
                    <span className="text-danger asterisk-size">*</span>
                  </div>
                  <div className="col-md-6">
                    <div className="form-control">
                      <ModernDatepicker
                        date={this.state.fromDate}
                        format={"DD-MMM-YYYY"}
                        onChange={(date) => this.onChangeFromDate(date)}
                        placeholder={"Select a date"}
                        className="color"
                        minDate={new Date(1900, 1, 1)}
                      />
                    </div>
                    <div className="error-message">
                      {this.state.formErrors["fromDateError"]}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0">
                <div className="row row-sm">
                  <div className="col-md-5">
                    <b>To Date</b>{" "}
                    <span className="text-danger asterisk-size">*</span>
                  </div>
                  <div className="col-md-6">
                    <div className="form-control">
                      <ModernDatepicker
                        date={this.state.toDate}
                        format={"DD-MMM-YYYY"}
                        onChange={(date) => this.onChangeToDate(date)}
                        placeholder={"Select a date"}
                        className="color"
                        minDate={new Date(1900, 1, 1)}
                      />
                    </div>
                    <div className="error-message">
                      {this.state.formErrors["toDateError"]}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg mg-t-10 mg-lg-t-0"></div>
            </div>
          </div>
          <div className="mg-l-50">
            <ToolkitProvider
              keyField="SlNo"
              data={this.state.filterValue === "" ? data : filteredData}
              columns={employeesTaskReportColumns}
            >
              {(props) => (
                <div className="mg-t-10">
                  <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                    <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                      <div className="row">
                        {this.state.isToShowSortingFields && (
                          <>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortColumn">Column:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedColumn}
                                    onChange={this.onChangeColumn}
                                  >
                                    <option value="">--Select--</option>
                                    {this.state.columns.map((col) => (
                                      <option key={col}>{col}</option>
                                    ))}
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortOrder">Order:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedSort}
                                    onChange={this.onChangeSortOrder}
                                  >
                                    <option value="">--Select--</option>
                                    <option value="ascending">Ascending</option>
                                    <option value="descending">
                                      Descending
                                    </option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-2">
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSort}
                                  title="Clear Sort Fields"
                                >
                                  <i className="far fa-window-close"></i>
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {this.state.isToShowFilteringField && (
                          <>
                            <div className="col-md-12">
                              <div
                                className="row"
                                style={{ flexWrap: "nowrap" }}
                              >
                                <div className="col-md-1 mg-t-7">
                                  <label htmlFor="search">Search:</label>
                                </div>
                                <div className="col-lg pd-r-10">
                                  <input
                                    type="text"
                                    className="form-control mg-l-10"
                                    maxLength="20"
                                    value={this.state.filterValue}
                                    onChange={this.onChangefilterValue}
                                  />
                                </div>
                                <div>
                                  <span
                                    className="btn btn-primary pd-b-5"
                                    onClick={this.clearSearch}
                                  >
                                    <i
                                      className="far fa-window-close"
                                      title="Clear Filter"
                                    ></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    {employeesTaskReportLength > 0 && (
                      <div
                        className="col-md-2"
                        style={{ textAlign: "end", marginTop: "10px" }}
                      >
                        <i
                          className="fas fa-exchange-alt fa-rotate-90 pointer"
                          title={
                            this.state.isToShowSortingFields
                              ? "Hide Sort"
                              : "Show Sort"
                          }
                          onClick={this.displaySortingFields}
                        ></i>
                        {!this.state.isToShowFilteringField ? (
                          <i
                            className="fas fa-filter pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Show Filter"
                          ></i>
                        ) : (
                          <i
                            className="fas fa-funnel-dollar pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Hide Filter"
                          ></i>
                        )}
                        <i
                          className="fas fa-file-excel mg-l-10 pointer"
                          style={{ color: "green" }}
                          onClick={this.exportEmployeesTaskReportToExcel}
                          title="Export to Excel"
                        ></i>
                      </div>
                    )}
                  </div>
                  <div
                    className="borderTable"
                    style={{ overflowX: "auto", width: "98%" }}
                  >
                    <div>
                      <table
                        style={{
                          width:
                            (this.state.filteredArray.length < 10 &&
                              this.state.filterValue !== "") ||
                            this.state.employeesTaskReport.length < 10
                              ? "133%"
                              : "132.1%",
                        }}
                      >
                        <thead>
                          <tr>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "2.1%"
                                    : "2.05%",
                              }}
                            >
                              Sl No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "3.9%"
                                    : "3.95%",
                              }}
                            >
                              Cus Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "4.1%"
                                    : "4.1%",
                              }}
                            >
                              Proj Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "3.8%"
                                    : " 3.85%",
                              }}
                            >
                              Batch No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 12
                                    ? "4.05%"
                                    : "4.05%",
                              }}
                            >
                              Emp Code
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 12
                                    ? "9.4%"
                                    : "9.35%",
                              }}
                            >
                              Emp Name
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 12
                                    ? "5.15%"
                                    : "5.2%",
                              }}
                            >
                              Department
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 12
                                    ? "8.2%"
                                    : "8.2%",
                              }}
                            >
                              Manager
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "8.65%"
                                    : "8.6%",
                              }}
                            >
                              Activity
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "6.55%"
                                    : "6.6%",
                              }}
                            >
                              Prod. Allocated
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "6.55%"
                                    : "6.5%",
                              }}
                            >
                              Prod. Completed
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "5.9%"
                                    : "5.95%",
                              }}
                            >
                              QC Allocated
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "5.8%"
                                    : "5.8%",
                              }}
                            >
                              QC Completed
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 10 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeesTaskReport.length < 10
                                    ? "6.05%"
                                    : "6.05%",
                              }}
                            >
                              Productivity (Hrs)
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div
                      style={
                        (this.state.filteredArray.length > 10 &&
                          this.state.filterValue !== "") ||
                        (this.state.employeesTaskReport.length > 10 &&
                          this.state.filterValue === "")
                          ? {
                              width: "133%",
                              overflowY: "scroll",
                              borderBottom: "1px solid #cdd4e0",
                            }
                          : { width: "133%" }
                      }
                      ref={this.divScrollRef}
                      className="employees-task-report-table-height"
                      onScroll={this.handleScroll}
                    >
                      <BootstrapTable
                        bootstrap4
                        {...props.baseProps}
                        striped
                        hover
                        condensed
                        headerClasses="header-class"
                      />
                      <div className="col-md-10">
                        {/* {((this.state.index <=
                          this.state.employeesTaskReport.length &&
                          this.state.filterValue === "") ||
                          this.state.index <=
                            this.state.filteredArray.length) && (
                          <p>loading more rows, please scroll...</p>
                        )} */}
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
            {this.state.position > 600 && this.state.filterValue === "" && (
              <div style={{ textAlign: "end" }}>
                <button className="scroll-top" onClick={this.scrollToTop}>
                  <div className="arrow up"></div>
                </button>
              </div>
            )}
          </div>
        </LoadingOverlay>
      </div>
    );
    //#endregion
  }
}

export default employeesTaskReportReport;
