import React, { Component } from "react";
import employeeSpecificReportService from "../../services/employeeSpecificReport.service";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import helper from "../../helpers/helpers";
import tableFunctions from "../../helpers/tableFunctions";
import Moment from "moment";
import BarLoader from "react-spinners/BarLoader";
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

class employeeSpecificDatewiseSummaryReport extends Component {
  constructor(props) {
    super(props);

    this.divScrollRef = React.createRef();

    this.displaySortingFields = this.displaySortingFields.bind(this);
    this.displayFilteringField = this.displayFilteringField.bind(this);
    this.onChangefilterValue = this.onChangefilterValue.bind(this);
    this.onChangeColumn = this.onChangeColumn.bind(this);
    this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
    this.sortData = this.sortData.bind(this);
    this.clearSort = this.clearSort.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.exportEmployeeSpecificSummaryReportToExcel =
      this.exportEmployeeSpecificSummaryReportToExcel.bind(this);

    //#region State Variables
    this.state = {
      employeeSpecificSummaryReport: [],
      loading: false,
      spinnerMessage: "",
      index: 20,
      position: 0,
      columns: [],
      isToShowSortingFields: false,
      isToShowFilteringField: false,
      selectedColumn: "",
      selectedSort: "",
      filteredArray: [],
      filterValue: "",
      employeeSpecificDetailsReport: [],
      employeeSpecificDetailsReportExpanded: [],
    };
    //#endregion
  }

  //#region page load
  componentDidMount() {
    if (!helper.getUser()) {
      this.props.history.push({
        pathname: "/",
      });
      return;
    }

    this.fetchEmployeeSpecificSummaryReport();
  }
  //#endregion

  //#region fetching Employee Specific Summary Report from Web API
  fetchEmployeeSpecificSummaryReport() {
    this.setState({
      spinnerMessage:
        "Please wait while fetching Employee Specific Summary Report...",
      loading: true,
    });

    employeeSpecificReportService
      .readEmployeeSpecificSummaryReport(
        this.props.employee,
        this.props.fromDate,
        this.props.toDate
      )
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          ProductionOrQCCompletedOn: Moment(
            obj.ProductionOrQCCompletedOn
          ).format("DD-MMM-yyyy"),
        }));

        if (response.data.length === 0) {
          toast.error("No Data Found!!");
        }
        this.setState({
          employeeSpecificSummaryReport: formattedArray,
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Export Employee Specific Report to Excel
  exportEmployeeSpecificSummaryReportToExcel() {
    this.setState({
      spinnerMessage:
        "Please wait while exporting Employee Specific Summary Report to Excel...",
      loading: true,
    });

    employeeSpecificReportService
      .exportEmployeeSpecificSummaryReportToExcel(
        this.props.employee,
        this.props.fromDate,
        this.props.toDate
      )
      .then((response) => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute(
          "download",
          "Employee Specific Summary Report.xlsx"
        );
        document.body.appendChild(fileLink);
        fileLink.click();

        this.setState({
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  }
  //#endregion

  //#region Filter Functions
  //#region Display Filtering Field
  displayFilteringField() {
    this.setState((previousState) => ({
      isToShowFilteringField: !previousState.isToShowFilteringField,
      filterValue: "",
      isToShowSortingFields: false,
    }));
  }
  //#endregion

  //#region on change filter value
  onChangefilterValue(e) {
    this.setState({ filterValue: e.target.value }, () =>
      this.getFilteredRows()
    );
  }
  //#endregion

  //#region get filtered rows
  getFilteredRows() {
    const filteredArray = tableFunctions.filterArray(
      this.state.employeeSpecificSummaryReport,
      this.state.filterValue
    );

    this.setState({ filteredArray: filteredArray });
  }
  //#endregion

  //#region Clear Search
  clearSearch() {
    this.setState({
      filterValue: "",
    });
  }
  //#endregion
  //#endregion

  //#region  Sort Functions
  //#region Display Sorting Fields
  displaySortingFields() {
    let columns = Object.keys(this.state.employeeSpecificSummaryReport[0]);

    this.setState((previousState) => ({
      isToShowSortingFields: !previousState.isToShowSortingFields,
      selectedColumn: "",
      selectedSort: "",
      columns: columns,
      filterValue: "",
      isToShowFilteringField: false,
    }));
  }
  //#endregion

  //#region Selecting the sort column
  onChangeColumn(e) {
    this.setState({
      selectedColumn: e.target.value,
      selectedSort: "",
    });
  }
  //#endregion

  //#region On Change Sort
  onChangeSortOrder(e) {
    this.setState(
      {
        selectedSort: e.target.value,
      },
      () => this.sortData()
    );
  }
  //#endregion

  //#region Sort Data based on column and order
  sortData() {
    let sortedArray = [];
    let column =
      this.state.selectedColumn !== "" ? this.state.selectedColumn : "SlNo";
    const selectedSort =
      this.state.selectedSort !== "" ? this.state.selectedSort : "ascending";
    let numberColumns = ["SlNo", "ProductiveHours"];

    let dateColumns = ["ProductionOrQCCompletedOn"];

    sortedArray = tableFunctions.sortData(
      this.state.employeeSpecificSummaryReport,
      column,
      selectedSort,
      numberColumns,
      dateColumns
    );

    this.setState({ employeeSpecificSummaryReport: sortedArray });
  }
  //#endregion

  //#region  Clear Sort
  clearSort() {
    this.setState(
      {
        selectedColumn: "",
        selectedSort: "",
      },
      () => this.sortData()
    );
  }
  //#endregion
  //#endregion

  //#region Handle Scroll
  handleScroll(e) {
    var currentHeight = e.currentTarget.scrollTop;
    var maxScrollPosition =
      e.currentTarget.scrollHeight - e.currentTarget.clientHeight;

    this.setState({ position: currentHeight });

    if ((currentHeight / maxScrollPosition) * 100 > 90) {
      let curIndex = this.state.index + 20;
      this.setState({ index: curIndex });
    }
  }
  //#endregion

  //#region Scroll to Top
  scrollToTop = () => {
    this.divScrollRef.current.scroll({
      top: 0,
      behavior: "smooth",
    });
  };
  //#endregion

  //#region fetch Employee Specific Details Report
  fetchEmployeeSpecificDetailsReport = (row, isExpand, rowIndex, e) => {
    if (!isExpand) {
      this.setState(() => ({
        employeeSpecificDetailsReportExpanded: [],
        loading: false,
      }));
      return;
    }

    this.setState({
      spinnerMessage: "Please wait while fetching Employee Specific Details...",
      loading: true,
    });

    employeeSpecificReportService
      .readEmployeeSpecificDetailsReport(
        this.props.employee,
        row.ProductionOrQCCompletedOn,
        row.ProductionOrQCCompletedOn
      )
      .then((response) => {
        let formattedArray = response.data.map((obj) => ({
          ...obj,
          ProductionOrQCCompletedOn: Moment(
            obj.ProductionOrQCCompletedOn
          ).format("DD-MMM-yyyy"),
        }));

        this.setState({
          employeeSpecificDetailsReport: formattedArray,
          employeeSpecificDetailsReportExpanded: [row.SlNo],
          loading: false,
        });
      })
      .catch((e) => {
        this.setState({
          loading: false,
        });
        toast.error(e.response.data.Message, { autoClose: false });
      });
  };
  //#endregion

  render() {
    const data = this.state.employeeSpecificSummaryReport.slice(
      0,
      this.state.index
    );
    const filteredData = this.state.filteredArray.slice(0, this.state.index);
    const employeeSpecificSummaryReportLength =
      this.state.employeeSpecificSummaryReport.length;

    //#region Employee Specific Summary Report Columns
    const employeeSpecificSummaryReportColumns = [
      {
        dataField: "SlNo",
        align: "center",
        text: "SL No",
      },
      {
        dataField: "ProductionOrQCCompletedOn",
        align: "center",
        text: "Production Or QC Completed On",
      },
      {
        dataField: "ProductiveHours",
        align: "center",
        text: "Productive Hours",
      },
    ];
    //#endregion

    //#region Employee Specifc Details Report Columns
    const employeeSpecificDetailsReportColumns = [
      {
        dataField: "SlNo",
        align: "center",
        text: "Sl No",
        style: {
          width: "5.5%",
        },
      },
      {
        dataField: "CustomerCode",
        align: "center",
        text: "Cus Code",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "ProjectCode",
        align: "center",
        text: "Proj Code",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "BatchNo",
        align: "center",
        text: "Batch No",
        style: {
          width: "8%",
        },
      },
      {
        dataField: "Activity",
        text: "Activity",
        style: {
          textOverflow: "ellipsis",
          overflow: "hidden",
          whiteSpace: "nowrap",
          width: "19%",
        },
        title: true,
      },
      {
        dataField: "ProductionOrQCCompletedOn",
        text: "Prod. or QC Completed On",
        align: "center",
        style: {
          width: "15.5%",
        },
      },
      {
        dataField: "ProductionCompletedCount",
        text: "Prod. Completed Count",
        align: "center",
        style: {
          width: "14%",
        },
      },
      {
        dataField: "ProductionTarget",
        text: "Prod. Target",
        align: "center",
        style: {
          width: "9%",
        },
      },
      {
        dataField: "ProductionProductiveHours",
        text: "Prod. Productive Hours",
        align: "center",
        style: {
          width: "14%",
        },
      },
      {
        dataField: "QCCompletedCount",
        text: "QC Completed Count",
        align: "center",
        style: {
          width: "13%",
        },
      },
      {
        dataField: "QCTarget",
        text: "QC Target",
        align: "center",
        style: {
          width: "11%",
        },
      },
      {
        dataField: "QCProductiveHours",
        text: "QC Productive Hours",
        align: "center",
        style: {
          width: "14%",
        },
      },
    ];
    //#endregion

    const rowStyle = (row, rowIndex) => {
      const style = {};
      if (row.ProductiveHours <= 4) {
        style.backgroundColor = "red";
        style.color = "white";
      }

      return style;
    };

    const expandEmployeeSpecificSummaryRow = {
      onlyOneExpanding: true,
      //parentClassName: "rowBackgroundColor",
      showExpandColumn: true,
      onExpand: this.fetchEmployeeSpecificDetailsReport,
      expanded: this.state.employeeSpecificDetailsReportExpanded,
      expandHeaderColumnRenderer: (isAnyExpands) => (
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
        ></div>
      ),
      renderer: (row) => (
        <div>
          <ToolkitProvider
            keyField="SlNo"
            data={this.state.employeeSpecificDetailsReport}
            columns={employeeSpecificDetailsReportColumns}
          >
            {(props) => (
              <>
                <div
                  className="borderTable"
                  style={{ overflowX: "auto", width: "98%" }}
                >
                  <div>
                    <table
                      style={{
                        width:
                          (this.state.filteredArray.length < 10 &&
                            this.state.filterValue !== "") ||
                          this.state.employeeSpecificDetailsReport.length < 10
                            ? "140%"
                            : "139.1%",
                      }}
                    >
                      <thead>
                        <tr>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "4%"
                                  : "4%",
                            }}
                          >
                            Sl No
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "5.85%"
                                  : "5.85%",
                            }}
                          >
                            Cus Code
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "5.9%"
                                  : "5.9%",
                            }}
                          >
                            Proj Code
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "5.9%"
                                  : "5.9%",
                            }}
                          >
                            Batch No
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "13.9%"
                                  : "13.9%",
                            }}
                          >
                            Activity
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "11.4%"
                                  : "11.4%",
                            }}
                          >
                            Prod. or QC Completed on
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "10.2%"
                                  : "10.2%",
                            }}
                          >
                            Prod. Completed Count
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "6.55%"
                                  : "6.55%",
                            }}
                          >
                            Prod. Target
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "10.25%"
                                  : "10.25%",
                            }}
                          >
                            Prod. Productive Hours
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "9.55%"
                                  : "9.55%",
                            }}
                          >
                            QC Completed Count
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "8.05%"
                                  : "8.1%",
                            }}
                          >
                            QC Target
                          </td>
                          <td
                            className="custom-table-header"
                            style={{
                              width:
                                (this.state.filteredArray.length < 10 &&
                                  this.state.filterValue !== "") ||
                                this.state.employeeSpecificDetailsReport
                                  .length < 10
                                  ? "10.25%"
                                  : "10.25%",
                            }}
                          >
                            QC Productive Hours
                          </td>
                        </tr>
                      </thead>
                    </table>
                  </div>
                  <div
                    style={
                      (this.state.filteredArray.length > 10 &&
                        this.state.filterValue !== "") ||
                      (this.state.employeeSpecificDetailsReport.length > 10 &&
                        this.state.filterValue === "")
                        ? {
                            width: "140%",
                            height: "300px",
                            overflowY: "scroll",
                            borderBottom: "1px solid #cdd4e0",
                          }
                        : { width: "140%" }
                    }
                    ref={this.divScrollRef}
                    className="scrollable-element"
                    onScroll={this.handleScroll}
                  >
                    <BootstrapTable
                      bootstrap4
                      {...props.baseProps}
                      headerClasses="header-class"
                    />
                  </div>
                </div>
              </>
            )}
          </ToolkitProvider>
        </div>
      ),
    };

    return (
      <div>
        <LoadingOverlay
          active={this.state.loading}
          spinner={
            <div className="spinner-background">
              <BarLoader
                css={helper.getcss()}
                color={"#38D643"}
                width={"350px"}
                height={"10px"}
                speedMultiplier={0.3}
              />
              <p style={{ color: "black", marginTop: "5px" }}>
                {this.state.spinnerMessage}
              </p>
            </div>
          }
        >
          <div className="mg-l-50">
            <ToolkitProvider
              keyField="SlNo"
              data={this.state.filterValue === "" ? data : filteredData}
              columns={employeeSpecificSummaryReportColumns}
            >
              {(props) => (
                <div className="mg-t-10">
                  <div className="row mg-b-10" style={{ marginRight: "15px" }}>
                    <div className="col-md-10" style={{ whiteSpace: "nowrap" }}>
                      <div className="row">
                        {this.state.isToShowSortingFields && (
                          <>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortColumn">Column:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedColumn}
                                    onChange={this.onChangeColumn}
                                  >
                                    <option value="">--Select--</option>
                                    {this.state.columns.map((col) => (
                                      <option key={col}>{col}</option>
                                    ))}
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="row">
                                <div className="col-md-3 mg-t-7">
                                  <label htmlFor="sortOrder">Order:</label>
                                </div>
                                <div className="col-lg">
                                  <select
                                    className="form-control mg-l-5"
                                    value={this.state.selectedSort}
                                    onChange={this.onChangeSortOrder}
                                  >
                                    <option value="">--Select--</option>
                                    <option value="ascending">Ascending</option>
                                    <option value="descending">
                                      Descending
                                    </option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-2">
                              <div>
                                <span
                                  className="btn btn-primary pd-b-5"
                                  onClick={this.clearSort}
                                  title="Clear Sort Fields"
                                >
                                  <i className="far fa-window-close"></i>
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {this.state.isToShowFilteringField && (
                          <>
                            <div className="col-md-12">
                              <div
                                className="row"
                                style={{ flexWrap: "nowrap" }}
                              >
                                <div className="col-md-1 mg-t-7">
                                  <label htmlFor="search">Search:</label>
                                </div>
                                <div className="col-lg pd-r-10">
                                  <input
                                    type="text"
                                    className="form-control mg-l-10"
                                    maxLength="20"
                                    value={this.state.filterValue}
                                    onChange={this.onChangefilterValue}
                                  />
                                </div>
                                <div>
                                  <span
                                    className="btn btn-primary pd-b-5"
                                    onClick={this.clearSearch}
                                  >
                                    <i
                                      className="far fa-window-close"
                                      title="Clear Filter"
                                    ></i>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    {employeeSpecificSummaryReportLength > 0 && (
                      <div
                        className="col-md-2"
                        style={{ textAlign: "end", marginTop: "10px" }}
                      >
                        <i
                          className="fas fa-exchange-alt fa-rotate-90 pointer"
                          title={
                            this.state.isToShowSortingFields
                              ? "Hide Sort"
                              : "Show Sort"
                          }
                          onClick={this.displaySortingFields}
                        ></i>
                        {!this.state.isToShowFilteringField ? (
                          <i
                            className="fas fa-filter pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Show Filter"
                          ></i>
                        ) : (
                          <i
                            className="fas fa-funnel-dollar pointer mg-l-10"
                            onClick={this.displayFilteringField}
                            title="Hide Filter"
                          ></i>
                        )}
                        <i
                          className="fas fa-file-excel mg-l-10 pointer"
                          style={{ color: "green" }}
                          onClick={
                            this.exportEmployeeSpecificSummaryReportToExcel
                          }
                          title="Export to Excel"
                        ></i>
                      </div>
                    )}
                  </div>
                  <div style={{ width: "98%" }}>
                    <div>
                      <table
                        style={{
                          width:
                            (this.state.filteredArray.length < 12 &&
                              this.state.filterValue !== "") ||
                            this.state.employeeSpecificSummaryReport.length < 12
                              ? "100%"
                              : "99.1%",
                        }}
                      >
                        <thead>
                          <tr>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificSummaryReport
                                    .length < 12
                                    ? "6.45%"
                                    : "6.45%",
                              }}
                            >
                              Sl No
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificSummaryReport
                                    .length < 12
                                    ? "5.9%"
                                    : "5.9%",
                              }}
                            >
                              Date
                            </td>
                            <td
                              className="custom-table-header"
                              style={{
                                width:
                                  (this.state.filteredArray.length < 12 &&
                                    this.state.filterValue !== "") ||
                                  this.state.employeeSpecificSummaryReport
                                    .length < 12
                                    ? "5.9%"
                                    : "5.9%",
                              }}
                            >
                              Productive Hours
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div
                      style={
                        (this.state.filteredArray.length > 9 &&
                          this.state.filterValue !== "") ||
                        (this.state.employeeSpecificSummaryReport.length > 9 &&
                          this.state.filterValue === "")
                          ? {
                              height: "280px",
                              overflowY: "scroll",
                              borderBottom: "1px solid #cdd4e0",
                            }
                          : {}
                      }
                      ref={this.divScrollRef}
                      className="scrollable-element"
                      onScroll={this.handleScroll}
                    >
                      <BootstrapTable
                        bootstrap4
                        {...props.baseProps}
                        striped
                        hover
                        condensed
                        headerClasses="header-class"
                        expandRow={expandEmployeeSpecificSummaryRow}
                        rowStyle={rowStyle}
                      />
                      <div className="col-md-10">
                        {((this.state.index <=
                          this.state.employeeSpecificSummaryReport.length &&
                          this.state.filterValue === "") ||
                          this.state.index <=
                            this.state.filteredArray.length) && (
                          <p>loading more rows, please scroll...</p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
            {this.state.position > 600 && this.state.filterValue === "" && (
              <div style={{ textAlign: "end" }}>
                <button className="scroll-top" onClick={this.scrollToTop}>
                  <div className="arrow up"></div>
                </button>
              </div>
            )}
          </div>
        </LoadingOverlay>
      </div>
    );
  }
}

export default employeeSpecificDatewiseSummaryReport;
