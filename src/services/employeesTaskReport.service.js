import http from "../http-common";

class EmployeesTaskReportService {
  //#region Read Employees Task Details Report Data
  readEmployeesTaskDetailsReportData(data) {
    return http.post(
      `/EmployeesTaskReport/ReadEmployeesTaskDetailsReportData`,
      data
    );
  }
  //#endregion

  //#region Download Employees Task Report to Excel
  exportEmployeesTaskReportToExcel(data) {
    return http.post(
      `/EmployeesTaskReport/ExportEmployeesTaskDetailsReportDataToExcel`,
      data,
      {
        responseType: "blob",
      }
    );
  }
  //#endregion
}

export default new EmployeesTaskReportService();
